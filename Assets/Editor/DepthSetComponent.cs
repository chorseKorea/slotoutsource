﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


public class DepthSetComponent : ScriptableWizard

{
    public GameObject rootObject = null;
    public int SetDepth;
    public int LabelDepth;
    public int SetPositoinZ;

    string temp_ParentName;

    [MenuItem("NGUI/Depth Set Component")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard("NGUI Set Component", typeof(DepthSetComponent));
    }


    void OnGUI()
    {
        //EditorGUIUtility.LookLikeControls(80f);
        EditorGUIUtility.fieldWidth = 80.0f;

        if (rootObject != null)
        {
            EditorGUILayout.LabelField("Parent : ", rootObject.transform.parent.name);
            //temp_ParentName = rootObject.transform.parent.name;
        }
        else
        {
            EditorGUILayout.LabelField("Parent : ", "NO TARGET");
        }

        rootObject = (GameObject)EditorGUILayout.ObjectField("Root Object", this.rootObject, typeof(GameObject), true, GUILayout.ExpandWidth(true));

        /*
        if (Event.current.type == EventType.repaint) {

            Debug.Log("repaint    " + rootObject.transform.parent.name + "  " + temp_ParentName);

            //DragPerformEvent(rootObject.transform.parent.name);
        }
        */
        

        SetDepth = EditorGUILayout.IntField("SetDepth : ", SetDepth, GUILayout.Width(300));
        LabelDepth = EditorGUILayout.IntField("LabelDepth : ", LabelDepth, GUILayout.Width(300));
        SetPositoinZ = EditorGUILayout.IntField("SetPositoinZ : ", SetPositoinZ, GUILayout.Width(300));

        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Set Depth"))
            {
                FindChildObj(rootObject);
                Repaint();
            }
            if (GUILayout.Button("Find Label Set Depth"))
            {
                FindLabelSetDepth(rootObject);
                Repaint();
            }
            if (GUILayout.Button("Label Set z position"))
            {
                LabelClearPositoinZ(rootObject);
                Repaint();
            }

            if (GUILayout.Button("Set Type Object Name"))
            {
                SetTypeObjName(rootObject);
                Repaint();
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Set Depth, Find Label, Set Z Positoin, Object Name On Click"))
            {
                FindChildObj(rootObject);
                FindLabelSetDepth(rootObject);
                LabelClearPositoinZ(rootObject);
                SetTypeObjName(rootObject);
                Repaint();
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("panel Depth Change"))
        {
            
        }
        GUILayout.EndHorizontal();
    }

    /*
    private void DragPerformEvent(string _name)
    {
        Event e = Event.current;

        if (e.type == EventType.DragPerform)
        {
            if (temp_ParentName == _name)
            {
                Debug.Log("== parent");
            }
            else
            {
                Debug.Log("!= parent");
            }

            temp_ParentName = _name;
            Debug.Log(temp_ParentName);
        }
    }
    */

    

    private void FindChildObj(GameObject _target)
    {
        if (rootObject == null)
        {
            NGUIDebug.Log("no target!!");
            return;
        }
        Transform[] tempTransforms = _target.GetComponentsInChildren<Transform>();

        for (int i = 0; i < tempTransforms.Length; i++)
        {
            if (i == 0) continue;
            UIWidget child = tempTransforms[i].GetComponent<UIWidget>();
            if (child != null)
            {
                child.depth = SetDepth;
                SetDepth++;
            }
        }
    }

    private void FindLableSetDepth(GameObject _target)
    {
        if (rootObject == null)
        {
            NGUIDebug.Log("no target!!");
            return;
        }
        
        Transform[] tempTransforms = _target.GetComponentsInChildren<Transform>();

        for (int i = 0; i < tempTransforms.Length; i++)
        {
            if (i == 0) continue;
            
        }
    }

    private void LabelClearPositoinZ(GameObject _target)
    {
        if (rootObject == null)
        {
            NGUIDebug.Log("no target!!");
            return;
        }
        
        Transform[] tempTransforms = _target.GetComponentsInChildren<Transform>();

        for (int i = 0; i < tempTransforms.Length; i++)
        {
            if (i == 0) continue;
            if (tempTransforms[i].GetComponent<UILabel>() != null)
            {
                tempTransforms[i].localPosition = new Vector3(tempTransforms[i].localPosition.x, tempTransforms[i].localPosition.y, SetPositoinZ);
            }
        }
    }

    private void FindLabelSetDepth(GameObject _target)
    {
        if (rootObject == null)
        {
            NGUIDebug.Log("no target!!");
            return;
        }
        
        Transform[] tempTransforms = _target.GetComponentsInChildren<Transform>();

        for (int i = 0; i < tempTransforms.Length; i++)
        {
            if (i == 0) continue;
            if (tempTransforms[i].GetComponent<UILabel>() != null)
            {
                tempTransforms[i].GetComponent<UIWidget>().depth = LabelDepth;
                //tempTransforms[i].GetComponent<UIWidget>().depth = SetDepth;
                //SetDepth++;
            }            
        }
    }

    private void SetTypeObjName(GameObject _target)
    {
        if (rootObject == null)
        {
            NGUIDebug.Log("no target!!");
            return;
        }
        
        Transform[] tempTransforms = _target.GetComponentsInChildren<Transform>();

        for (int i = 0; i < tempTransforms.Length; i++)
        {
            if (i == 0) continue;

            string searchWithinThis = tempTransforms[i].name;
            string searchForThis;
            int findString;

            if (tempTransforms[i].GetComponent<UILabel>() != null)
            {
                searchForThis = "Label";
                findString = searchWithinThis.IndexOf(searchForThis);

                if (findString == -1)
                {
                   tempTransforms[i].name = searchForThis + "(" + tempTransforms[i].name + ")";
                }
            }

            if (tempTransforms[i].GetComponent<UISprite>() != null)
            {
                searchForThis = "Sprite";
                findString = searchWithinThis.IndexOf(searchForThis);

                if(findString == -1)
                {
                    tempTransforms[i].name = searchForThis + "(" + tempTransforms[i].name + ")";
                }                
            }
        }
    }
    

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
    void Update() { }
}
