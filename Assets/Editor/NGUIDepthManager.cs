﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;



public class NGUIDepthManager : ScriptableWizard
{
    /// <summary>
    /// panel이 붙어있는 조사 대상 객체
    /// </summary>
    public GameObject rootObject = null;

    /// <summary>
    /// gameObect의 경로 , widget들
    /// </summary>
    private Dictionary<int, Dictionary<string, List<UIWidget>>> widgets = new Dictionary<int, Dictionary<string, List<UIWidget>>>();

    /// <summary>
    /// 패널들을 관리한다.
    /// </summary>
    private Dictionary<int, UIPanel> panels = new Dictionary<int, UIPanel>();

    /// <summary>
    /// widget이 없는 부모 객체들 관리
    /// </summary>
    private Dictionary<string, GameObject> parents = new Dictionary<string, GameObject>();
    
    /// <summary>
    /// 에디터 창 위치
    /// </summary>
    protected Vector2 scrollPosition = Vector2.zero;

    [MenuItem("NGUI/Depth Manager")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard("NGUI Depth Manager", typeof(NGUIDepthManager));
    }

    void OnGUI()
    {
        //EditorGUIUtility.LookLikeControls(80f);
        EditorGUIUtility.fieldWidth = 80.0f;

        rootObject = (GameObject)EditorGUILayout.ObjectField("Root Object", this.rootObject, typeof(GameObject), true, GUILayout.ExpandWidth(true));

        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Find Widgets Under Root"))
            {
                FindObjs();
                Repaint();
            }
            if (GUILayout.Button("Root Object Clear"))
            {
                rootObject = null;
                ClearList();
                Repaint();
            }
            if (GUILayout.Button("Clear z position"))
            {
                if (rootObject != null)
                {
                    ClearZPosition();
                    Repaint();
                }
            }
        }
        GUILayout.EndHorizontal();

        if (widgets.Count == 0)
            GUILayout.Label("Use 'Find Widgets Under Root' to get the widgets and their depths and Z Pos.");
        else
        {
            if (this.rootObject == null)
            {
                rootObject = null; 
                ClearList();
                Repaint();
            }
            else
                DrawWidgetsScrollView();
        }
    }

    /// <summary>
    /// 리스트들을 초기화
    /// </summary>
    private void ClearList()
    {
        widgets.Clear();
        panels.Clear();
        parents.Clear();
    }

    /// <summary>
    /// 패널의 정보를 출력한다.
    /// </summary>
    /// <param name="panelPath"></param>
    private Vector3 DrawPanelInfo(string panelPath)
    {
        //패널 이름 출력
        Vector3 tPanelPos;
        GUILayout.Label(string.Format("Panel: {0}", panelPath), EditorStyles.boldLabel);

        //패널 자체의 position
        GUILayout.BeginHorizontal();
        {
            EditorGUILayout.ObjectField(
                this.objectAtTransPathRoot(panelPath),
                typeof(GameObject), false);
            GameObject tPanelObj = this.objectAtTransPathRoot(panelPath);
            tPanelPos = tPanelObj.transform.localPosition;
            EditorGUILayout.LabelField("Z Pos:", GUILayout.Width(40), GUILayout.MaxWidth(40));
            tPanelPos.z = EditorGUILayout.FloatField(tPanelObj.transform.localPosition.z, GUILayout.MaxWidth(60));
            tPanelObj.transform.localPosition = tPanelPos;
        }
        GUILayout.EndHorizontal();

        return tPanelPos;
    }

    /// <summary>
    /// NGUI 객체들을 보여준다.
    /// </summary>
    protected void DrawWidgetsScrollView()
    {
        this.scrollPosition =
            EditorGUILayout.BeginScrollView(this.scrollPosition);

        if (widgets == null || widgets.Count <= 0)
            return;

        List<int> depthSort = new List<int>(widgets.Keys);
        depthSort.Sort();

        foreach( int depth in depthSort )
        {
            List<string> pathSort = new List<string>(widgets[depth].Keys);
            pathSort.Sort();

            GUI.color = Color.red;
            string text = string.Format( "{0} : {1}", depth, panels[depth]);
            GUILayout.Label( text , EditorStyles.boldLabel);
            GUI.color = Color.white;

            //panel밑에 아무것도 없으면 스킵
            if (widgets[depth].Count <= 0)
            {
                GUILayout.Space(20);
                continue;
            }

            EditorGUILayout.BeginVertical(EditorGUIUtility.GetBuiltinSkin(EditorSkin.Game).box);
            //widget을 출력한다.
            foreach (string fullPath in pathSort)
            {
                GUILayout.Label(string.Format("Path: {0}", fullPath), EditorStyles.boldLabel);

                for (int i = 0; i < widgets[depth][fullPath].Count; i++)
                    DrawDepthsWidget(widgets[depth][fullPath][i]);
            }
            EditorGUILayout.EndVertical();
            GUILayout.Space(20);
        }

        EditorGUILayout.EndScrollView();
    }

    /// <summary>
    /// widget을 그린다.
    /// </summary>
    /// <param name="depth"></param>
    /// <param name="widget"></param>
    protected void DrawDepthsWidget(UIWidget widget)
    {
        if (widgets == null)
            return;
        GUILayout.BeginHorizontal();
        {
            EditorGUILayout.TextField(widget.GetType().ToString(), GUILayout.MaxWidth(100));
            //GUILayout.Space(64.0f);
            EditorGUILayout.ObjectField(widget, typeof(UIWidget), true, GUILayout.ExpandWidth(true));

            if (GUILayout.Button("Select", GUILayout.Width(50)))
            {
                Selection.activeObject = widget.gameObject;
            }

            widget.depth = EditorGUILayout.IntField("Depth:", widget.depth);

        }
        GUILayout.EndHorizontal();
    }

    /// <summary>
    /// 최상의 uipanel밑의 객체들을 찾는다.
    /// </summary>
    protected void FindObjs()
    {
        if (rootObject == null)
            return;

        ClearList();

        UIPanel panel = rootObject.GetComponent<UIPanel>();
        if (panel == null)
        {
            //DebugUI.LogError("Not found a panel in rootObject");
            return;
        }

        parents.Add("", rootObject);
        panels.Add(panel.depth, panel);
        FindWidgetWithPathByParent(panel.depth, rootObject, "");
    }

    /// <summary>
    /// 하위 객체들을 찾을 시작 객체와 시작 경로
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="startPath"></param>
    protected void FindWidgetWithPathByParent(int panelDepth, GameObject parent, string startPath)
    {
        //패널 뎁스별 widgets 리스트를 가져온다.
        Dictionary<string, List<UIWidget>> panelList;
        if (widgets.ContainsKey(panelDepth))
            panelList = widgets[panelDepth];
        else
        {
            panelList = new Dictionary<string, List<UIWidget>>();
            widgets.Add(panelDepth, panelList);
        }

        //parent객체의 자식들에서 찾는다. 
        foreach (Transform child in parent.transform)
        {
            string path = string.Format("{0}/{1}", startPath, child.name);
            UIWidget widget = child.gameObject.GetComponent<UIWidget>();
            
            //widget일 때에는 dictionary에 추가
            if (widget != null)
            {                
                List<UIWidget> list;
                if (panelList.ContainsKey(startPath))
                    list = panelList[startPath];
                else
                {
                    list = new List<UIWidget>();
                    panelList.Add(startPath, list);
                }

                list.Add(widget);
            }
            else //일반 객체일 때에는 하위를 더 찾도록 한다.
            {
                //widget이 없는 부모 객체들을 모아둔다.
                if (!parents.ContainsKey(path))
                    parents.Add(path, child.gameObject);

                UIPanel childPanel = child.GetComponent<UIPanel>();

                //하위에 패널이 있을 때에는 새로운 패널 뎁스로 시작
                if (childPanel != null)
                {
                    if (panels.ContainsKey(childPanel.depth))
                    {
                        //DebugUI.LogError("Find a Same panel depth value. Change panel depth : " + path);
                        return;
                    }
                    panels.Add(childPanel.depth, childPanel);
                    FindWidgetWithPathByParent(childPanel.depth, child.gameObject, path);
                }
                else //패널이 없을 때에는 현재 패널 뎁스로 시작
                    FindWidgetWithPathByParent(panelDepth, child.gameObject, path);
            }
        }
    }

    public GameObject objectAtTransPathRoot(string path)
    {
        GameObject objectAtPath = null;
        string subPath = path.Substring(this.rootObject.name.Length);
        if (subPath.Length < 1)
        {
            objectAtPath = this.rootObject;
        }
        else
        {
            subPath = subPath.Substring(1);
            Transform transformAtPath = this.rootObject.transform.Find(subPath);
            if (transformAtPath != null)
            {
                objectAtPath = transformAtPath.gameObject;
            }
        }
        return objectAtPath;
    }

    /// <summary>
    /// 위치의 z값을 0으로 초기화 한다.
    /// </summary>
    private void ClearZPosition()
    {
        foreach (int depth in widgets.Keys)
        {
            foreach (string path in widgets[depth].Keys)
            {
                for (int i = 0; i < widgets[depth][path].Count; i++)
                    SetZPositionZero(widgets[depth][path][i].transform);
            }
        }

        foreach (string path in parents.Keys)
            SetZPositionZero(parents[path].transform);

        //DebugUI.Log("Complete Z position 0.0f");
    }

    /// <summary>
    /// 위치 설정
    /// </summary>
    /// <param name="tras"></param>
    private void SetZPositionZero(Transform tras)
    {
        Vector3 position = tras.localPosition;
        position.z = 0.0f;
        tras.localPosition = position;
    }
}