﻿using UnityEngine;
using System.Collections;

public class UT_Main : MonoBehaviour {

	// Use this for initialization
	void Start () {
        UI_Script_Mng._Inst.Create(gameObject);


        if (GameInfo.m_ePlatformName == ePLATFORM.WEB)
        {
            GameInfo.PlatFormName = "Web";
        }
        else
        {
            GameInfo.PlatFormName = "Mobile";
        }

        if (GameInfo.m_eCurrGameKind <= eGAMEKIND.CARD)
        {
            GameInfo.GameKindDirectory = "CardGame";
        }
        else
        {
            GameInfo.GameKindDirectory = "SlotGame";
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
