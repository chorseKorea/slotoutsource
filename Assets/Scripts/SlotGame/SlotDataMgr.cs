﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EDNParser;

[System.Serializable]
public class ReelElement
{
    public string id;
    public List<int> m_reelList;

    public ReelElement()
    {
        m_reelList = new List<int>();
    }
}

[System.Serializable]
public class ReelInfoMng
{
    public List<ReelElement> m_reels;
    List<int> ReelElementParsing(EdnNode en)
    {
        Debug.Log("ReelElementParsing " + en.values.Count);

        List<int> reellist = new List<int>();
        for (int i = 0; i < en.values.Count; i++)
        {
            EdnNode mapnode = en.values[i];
            reellist.Add(int.Parse(mapnode.value));
        }

        return reellist;
    }
    public ReelInfoMng()
    {
        m_reels = new List<ReelElement>();
    }

    public void AddReelInfo(EdnNode reelInfo)
    {
        for (int j = 0; j < reelInfo.values.Count - 1; j += 2)
        {
            ReelElement reelelement = new ReelElement();

            EdnNode reelelementID = reelInfo.values[j];
            Debug.Log(reelelementID.value);
            reelelement.id = reelelementID.value;

            EdnNode reelelementlist = reelInfo.values[j + 1];
            List<int> templist = ReelElementParsing(reelelementlist);
            reelelement.m_reelList = templist;

            m_reels.Add(reelelement);
        }

        m_reels.Sort((reel1, reel2) => reel1.id.CompareTo(reel2.id));

    }
}
 

public class SlotDataMgr : MonoBehaviour {

    private SlotDataMgr() { }
    static private readonly SlotDataMgr m_Instance = new GameObject().AddComponent<SlotDataMgr>();
    static public SlotDataMgr _Inst 
    {
        get 
        {
            m_Instance.name = "SlotDataMgr";
            return m_Instance; 
        } 
    }

    #region fields

    public ReelInfoMng m_normalReelInfoMng = null;
    public ReelInfoMng m_bonusReelInfoMng = null;

    // slot related Data
    public int m_amountBalance = 0;
    public string m_slotID = string.Empty;
    public float m_rewardJackpot = 0;
    public int[] m_reelIndexes;
    public List<int> m_listBingoLineIndex = new List<int>();
    public List<int> m_listBingCount = new List<int>();
    public int m_numScatter = 0;
    public int m_amountWin = 0;
    public int m_amountWinFreenSpin = 0;
    public bool m_isRecReelData = false;
    public bool m_isRecievedSpinResult = false;
    public bool m_isFreeSpin = false;
    private SlotMachine_Base m_slotMachineUI;
    #endregion

    public ReelInfoMng GetReelInfoMng
    {
        get
        {
            if(m_isFreeSpin)
            {
                return m_bonusReelInfoMng;
            }
            else
            {
                return m_normalReelInfoMng;
            }
        }
    }

    private void Start()
    {
        m_slotMachineUI = UI_Script_Mng._Inst.GetUI(eUI_TYPE.SlotMachine_Base) as SlotMachine_Base;
    }

    public void ExitSlot()
    {
        PacketMng._Inst.SEND_EXITSLOT_REQ(GamePlayer._Inst._UserToken, m_slotID); // send exit slot macchine req to server
    }

    public void OnReceivedExitReq()
    {
        Main._Inst._CUR_SCENE = eSCENE.SLOT_LOBBY;
    }

    public void JoinSlot(string id, uint betamount, bool autosel)
    {
        m_isRecReelData = false;
        PacketMng._Inst.SEND_JOINSLOTMACHINE_RES(id, betamount, autosel);
    }

    public void JoinBonusSLot(string id, uint betamount, bool autosel)
    {
        m_isRecReelData = false;
        PacketMng._Inst.SEND_JOINBONUSSLOTMACHINE_RES(id, betamount, autosel);
    }

    public void SpinReel(int betPerLine, int betLine)
    {
        m_isRecievedSpinResult = false;
        PacketMng._Inst.SEND_SPIN_REQ(GamePlayer._Inst._UserToken, (uint)betLine, (uint)betPerLine);
    }

    public void SpinBonusReel(int betPerLine, int betLine)
    {
        m_isRecievedSpinResult = false;
        PacketMng._Inst.SEND_BONUS_SPIN_REQ(GamePlayer._Inst._UserToken, (uint)betLine, (uint)betPerLine);
    }

    public void SetReelIndexes(int[] reelIndexs)
    {
        m_reelIndexes = reelIndexs;
    }

    public void SetBingoValueWithEdnNode(EdnNode bingValueNode)
    {
        m_listBingoLineIndex = new List<int>();
        m_listBingCount = new List<int>();
        m_amountWin = 0;

        for (int i = 0; i < bingValueNode.values.Count; i++)
        {
            EdnNode bingoNode = bingValueNode.values[i];

            if (bingoNode.value != null)
            {
                int valueBingLineIndex = 0;

                if (int.TryParse(bingoNode.value.ToString(), out valueBingLineIndex))
                {
                    m_listBingoLineIndex.Add(valueBingLineIndex);
                }
            }
            else
            {
                for (int j = 0; j < bingoNode.values.Count; j += 2)
                {
                    EdnNode mapNode = bingoNode.values[j];

                    if (mapNode.value == "pay") // amount to Win
                    {
                        EdnNode valuePay = bingoNode.values[j + 1];
                        int valueAmountWin = 0;

                        if (int.TryParse(valuePay.value.ToString(), out valueAmountWin))
                        {
                            m_amountWin += valueAmountWin;
                        }

                    }

                    else if (mapNode.value == "count") // hit Count
                    {
                        EdnNode valueCount = bingoNode.values[j + 1];
                        int valueBingCount = 0;

                        if (int.TryParse(valueCount.value.ToString(), out valueBingCount))
                        {
                            m_listBingCount.Add(valueBingCount);
                        }
                    }
                }
            }
        }
    }

    public void RecievedSpinResult()
    {
        m_isRecievedSpinResult = true;
    }

    public void UpdateSlotMachine()
    {
        m_slotMachineUI.SetJackPot((int)m_rewardJackpot);
        m_slotMachineUI.SetBalance(m_amountBalance);
    }
}
