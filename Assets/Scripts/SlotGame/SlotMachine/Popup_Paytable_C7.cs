﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Popup_Paytable_C7 : UI_Script
{

    #region field
    [SerializeField]
    List<GameObject> m_listPageGo = new List<GameObject>();

    [SerializeField]
    int m_pageIndex = 0;

    #endregion

    #region override method

    public override void DepthProcess ()
    {
        m_currDepth = childPanels[0].depth;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.depth = ++m_currDepth;
        }
    }

    public override void DirectionStart ()
    {
        //Debug.Log("열기 연출 시작!");
    }

    public override void DirectionEnd ( eUI_TYPE eType )
    {
        //Debug.Log("닫기 연출 시작!");
        _UIClose ();
    }

    public override void _UIClose ()
    {
        gameObject.SetActive (false);
    }
    public override void SetRenderQ()
    {
        int RQ = UI_Script_Mng.s_iPOPUP_RQ;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.renderQueue = UIPanel.RenderQueue.Explicit;
            item.startingRenderQueue = RQ++;
        }
    }


    #endregion override method

    #region mono

    private void Awake ()
    {
        childPanels = gameObject.GetComponentsInChildren<UIPanel> ();
        UIType = eUI_TYPE.Popup_Paytable_C7;
    }
    private void OnEnable()
    {
        for (int i = 0; i < m_listPageGo.Count; i++)
        {
            m_listPageGo[i].SetActive(false);
        }

        m_pageIndex = 0;

        m_listPageGo[m_pageIndex].SetActive(true);
    }

    private void OnDisable()
    {
        for(int i = 0 ; i < m_listPageGo.Count; i++)
        {
            m_listPageGo[i].SetActive(false);
        }

        m_pageIndex = 0;

        m_listPageGo[m_pageIndex].SetActive(true);
    }

    #endregion

    #region public interface

    public void DoToNextPage()
    {

        m_listPageGo[m_pageIndex].SetActive(false);
        m_pageIndex = (m_pageIndex - 1 + m_listPageGo.Count) % m_listPageGo.Count;
        m_listPageGo[m_pageIndex].SetActive(true);
    }

    public void DoToPrevPage()
    {
        m_listPageGo[m_pageIndex].SetActive(false);
        m_pageIndex = (m_pageIndex + 1 + m_listPageGo.Count) % m_listPageGo.Count;
        m_listPageGo[m_pageIndex].SetActive(true);
    }

    #endregion
}
