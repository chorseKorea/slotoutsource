﻿using UnityEngine;
using System.Collections;

public class LineIndicatorBox : MonoBehaviour
{
    #region fiedls

    [SerializeField]
    GameObject m_goBgOn;

    [SerializeField]
    string m_name_goBgOn = string.Empty;

    [SerializeField]
    int m_order = 0;

    [SerializeField]
    int m_lineIndex = 0;

    #endregion

    #region public getter

    public int GetOrder
    {
        get
        {
            return m_order;
        }
    }

    public int GetLineIndex
    {
        get
        {
            return m_lineIndex;
        }
    }

    #endregion

    #region mono

    private void OnEnable()
    {

        if(m_name_goBgOn.Equals(string.Empty))
        {
            return;
        }

        GameObject goBgOn = null;

        foreach(Transform childTr in this.gameObject.GetComponentsInChildren<Transform>())
        {
            if(childTr.gameObject.name == m_name_goBgOn)
            {
                goBgOn = childTr.gameObject;
                break;
            }
        }

        m_goBgOn = goBgOn;
        m_goBgOn.SetActive (false);
    }

    private void OnDisable()
    {
        m_goBgOn.SetActive (true);
    }

    #endregion 

    #region public interface

    public void DoTurnBg(bool isOn)
    {
        m_goBgOn.SetActive (isOn);
    }

    #endregion 
}
