﻿using ChorseBase;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Tile Line effect.
/// 
/// Define: Term "Spin" is total movement of reel 
/// Define: Term "Roll" is movement of each tile
/// 
/// </summary>
public class Reel : MonoBehaviour
{
    #region field

    [SerializeField]
    private Scene_Slot m_sceneSlot;

    // line order
    [SerializeField]
    private int m_index = 0;

    // tile list in line
    [SerializeField]
    private TileSet[] m_tiles;

    [SerializeField]
    private int[] m_tilesTypes;

    private int m_numberOfRoll = 0;
    private float m_timePerRoll = 0;
    [SerializeField]
    private iTween.EaseType m_easeTypeCurrent = iTween.EaseType.linear;

    private Task m_taskSpinReel = null; // task for reel relling;

	[SerializeField]
	private float m_durSpring = 0.8f; // duration for spring effect 
	[SerializeField]
	private float m_offsetSpring = 0.3f; // offset amount for spring 


    #endregion field

    #region public getter and setter

    public int Index
    {
        get
        {
            return m_index;
        }
        set
        {
            m_index = value;
        }
    }

    public TileSet[] Tiles
    {
        get
        {
            return m_tiles;
        }
        set
        {
            m_tiles = value;
        }
    }

    public int NumOfRoll
    {
        get
        {
            return m_numberOfRoll;
        }
        set
        {
            m_numberOfRoll = value;
        }
    }

    public float TimePerRoll
    {
        get
        {
            return m_timePerRoll;
        }
        set
        {
            m_timePerRoll = value;
        }
    }

    public iTween.EaseType EaseType
    {
        get
        {
            return m_easeTypeCurrent;
        }
        set
        {
            m_easeTypeCurrent = value;
        }
    }

    #endregion public getter and setter

    #region mono

    private void OnEnable ()
    {
        
    }

    private void OnDisable ()
    {
    }

    #endregion mono

    #region public method

    public void CreateTiles(GameObject targetTileSet , Vector2 tileSize  , Vector2 gapSize, int numTiles , int reelIndex)
    {
        Reel ScriptReel = this;

        ScriptReel.Index = reelIndex;
        int offsetAmount = 4;
        int actualNumInVerticalLine = numTiles + offsetAmount;

        int centerIndexHor = numTiles / 2;
        int centerIndexVer = actualNumInVerticalLine / 2;

        ScriptReel.Tiles = new TileSet[actualNumInVerticalLine];

        for (int i = 0; i < actualNumInVerticalLine; i++)
        {
            //create tile object from tile set
            GameObject tile = null; //new GameObject();
            tile = Instantiate (targetTileSet) as GameObject;

            //set layer
            tile.transform.parent = this.gameObject.transform;
            tile.layer = this.gameObject.layer;
            tile.transform.localScale = Vector3.one;
            this.transform.localScale = Vector3.one;

            //set location
            tile.transform.localPosition = Vector3.zero;
            Vector3 posTile = tile.transform.localPosition;

            int centerIndex = actualNumInVerticalLine / 2;
            int positionOffset = i - centerIndex;

            posTile.y += positionOffset * (tileSize.y + gapSize.y);

            tile.gameObject.name = string.Format ("tile_{000:0}" , i);
            tile.transform.localPosition = posTile;

            TileSet tileSet = tile.GetComponent<TileSet> ();
            tileSet.Index = i;
            tileSet.Height = tileSize.y;
            tileSet.Width = tileSize.x;
            tileSet.Gap = gapSize;

            int tileType = Random.Range (0 , tileSet.Slots.Length);
            tileSet.SetTileType (tileType);

            ScriptReel.Tiles[i] = tileSet;
        }
    }

    public Task SpinReel ( float timePerRoll , int NumOfRoll )
    {
        m_taskSpinReel = Task.Create (ReelSpinCoroutine (timePerRoll , NumOfRoll));

        return m_taskSpinReel;
    }

    public void StopSpin()
    {

    }


    #endregion public coroutine

    #region private method

    private void ShowSpinResult ()
    {
        int[] selectedTypes = new int[m_tiles.Length];
        int reelIndexCenterSlot = SlotDataMgr._Inst.m_reelIndexes[m_index];
        int indexCenterSlot = selectedTypes.Length / 2;

        List<int> listTypeForSelectedReel = SlotDataMgr._Inst.GetReelInfoMng.m_reels[m_index].m_reelList;

        for (int i = 0; i < selectedTypes.Length; i++)
        {
            int offsetIndex = (reelIndexCenterSlot + indexCenterSlot - i);

            if (offsetIndex < 0)
            {
                offsetIndex = listTypeForSelectedReel.Count + offsetIndex;
            }
            else if (offsetIndex > (listTypeForSelectedReel.Count - 1))
            {
                offsetIndex = offsetIndex - listTypeForSelectedReel.Count;
            }

            selectedTypes[i] = listTypeForSelectedReel[offsetIndex];
        }

        SetTilesTypes (selectedTypes);
    }

    /// <summary>
    /// set tiles types specifically with given types 
    /// </summary>
    /// <param name="types"></param>
    private void SetTilesTypes ( int[] types )
    {
        m_tilesTypes = types;

        for (int i = 0; i < m_tiles.Length; i++)
        {
            m_tiles[i].SetTileType (m_tilesTypes[i]);
        }
    }

    /// <summary>
    /// randomly set tile types
    /// </summary>
    private void SetRandomTilesTypes ()
    {
        int totalSymbols = PayData.totalSymbols;

        for (int i = 0; i < m_tiles.Length; i++)
        {
            m_tiles[i].SetTileType (Random.Range (0 , totalSymbols) % totalSymbols);
        }
    }

    private IEnumerator StopRollCoroutine ( float time )
    {
        TileSet tileTarget = null;
        int indexCenter = m_tiles.Length / 2;
        float heightPerTile = m_tiles[0].Height;
        int indexTarget = 0;

        Vector3 posCurrent = Vector3.zero;
        Vector3 posTarget = Vector3.zero;

        for (int i = 0; i < m_tiles.Length; i++)
        {
            tileTarget = m_tiles[i];
            iTween.Stop (tileTarget.gameObject);

            Hashtable hash = new Hashtable ();
            posCurrent = tileTarget.transform.localPosition;
            posTarget = posCurrent;
            indexTarget = tileTarget.Index;
            posTarget.y = 1 * heightPerTile * (indexTarget - indexCenter);

            hash.Add ("time" , time);
            hash.Add ("position" , posTarget);
            hash.Add ("islocal" , true);
            hash.Add ("easetype" , m_easeTypeCurrent);

            //tileTarget.transform.localPosition = posTarget;

            iTween.MoveTo (tileTarget.gameObject , hash);
        }

        yield return new WaitForSeconds (time);
       
    }

    private IEnumerator SpringCoroutine ( float time , float springOffset , bool isToUp )
    {
        yield return new WaitForFixedUpdate ();
        Hashtable hash = new Hashtable ();

        TileSet tileTarget = null;
        float amountToMove = m_tiles[0].Height * springOffset;

        Vector3 posCurrent = Vector3.zero;
        Vector3 posTarget = Vector3.zero;

        for (int i = 0; i < m_tiles.Length; i++)
        {
            tileTarget = m_tiles[i];
            posCurrent = tileTarget.transform.localPosition;
            posTarget = posCurrent;

            if (isToUp)
                posTarget.y += amountToMove;
            else
                posTarget.y -= amountToMove;

            //Debug.Log (string.Format ("time:{0}, posTarget {1}" , time , posTarget));
            hash.Clear ();
            hash.Add ("time" , time);
            hash.Add ("position" , posTarget);
            hash.Add ("islocal" , true);
            hash.Add ("easetype" , m_easeTypeCurrent);

            iTween.MoveTo (tileTarget.gameObject , hash);
        }

        yield return new WaitForSeconds (time);
    }

    private IEnumerator RollTileCoroutine ( float timePerRoll )
    {
        yield return new WaitForFixedUpdate ();

        TileSet tileTarget = null;
        int indexCenter = m_tiles.Length / 2;
        float heightPerTile = m_tiles[0].Height;
        int indexTarget = 0;

        Vector3 posCurrent = Vector3.zero;
        Vector3 posTarget = Vector3.zero;

        for (int i = 0; i < m_tiles.Length; i++)
        {
            tileTarget = m_tiles[i];
            tileTarget.Index -= 1;

            iTween.Stop (tileTarget.gameObject);

            if (tileTarget.Index < 0) // 0 번 인덱스 아래일때
            {
                indexTarget = m_tiles.Length - 1; // 제일 상단 인덱스로 보냄

                tileTarget.Index = indexTarget;
                posCurrent = tileTarget.transform.localPosition;
                posTarget = posCurrent;
                posTarget.y = 1 * heightPerTile * Mathf.Abs (indexTarget - indexCenter);

                tileTarget.transform.localPosition = posTarget;
            }
            else
            {
                Hashtable hash = new Hashtable ();
                posCurrent = tileTarget.transform.localPosition;
                posTarget = posCurrent;
                indexTarget = tileTarget.Index;
                posTarget.y = 1 * heightPerTile * (indexTarget - indexCenter);

                hash.Add ("time" , timePerRoll);
                hash.Add ("position" , posTarget);
                hash.Add ("islocal" , true);
                hash.Add ("easetype" , m_easeTypeCurrent);

                iTween.MoveTo (tileTarget.gameObject , hash);
            }
        }

        m_tiles = m_tiles.ToList ().OrderBy (x => x.Index).ToArray ();
    }

    private IEnumerator ReelSpinCoroutine ( float timePerRoll , int numberOfRoll )
    {
        m_timePerRoll = timePerRoll;
        m_numberOfRoll = numberOfRoll;

        float timeSpan = 0;
		float durInertia = m_durSpring;
		float springOffset = m_offsetSpring;

        for (int i = 0; i < m_numberOfRoll; i++)
        {
            if (i == 0) // first reel
            {
                // pull up
                m_easeTypeCurrent = iTween.EaseType.easeOutSine;
				yield return this.StartCoroutine (SpringCoroutine (durInertia , springOffset , true));

                // drop
                m_easeTypeCurrent = iTween.EaseType.easeInSine;
                this.StartCoroutine (this.RollTileCoroutine (durInertia));
                yield return new WaitForSeconds (durInertia);
                timeSpan += durInertia;
            }
            else
            {
                // actual spin
                m_easeTypeCurrent = iTween.EaseType.linear;

                this.StartCoroutine (this.RollTileCoroutine (m_timePerRoll));
                yield return new WaitForSeconds (m_timePerRoll);
            }
        }
        ShowSpinResult ();

        m_easeTypeCurrent = iTween.EaseType.easeOutSine;
        this.StartCoroutine (SpringCoroutine (m_timePerRoll , 0.3f , false));
        yield return new WaitForSeconds (m_timePerRoll);
        // pull up
        GameSoundMgr._Inst.PlayOneSound("Slots_Classic7_Reel_Stop");

        m_easeTypeCurrent = iTween.EaseType.spring;
        this.StartCoroutine (StopRollCoroutine (0.5f));
        yield return new WaitForSeconds (0.5f);

        m_tiles = m_tiles.ToList().OrderBy(x => x.Index).ToArray();

        SlotMachine_Base slotmachine = UI_Script_Mng._Inst.GetUI(eUI_TYPE.SlotMachine_Base) as SlotMachine_Base;
        slotmachine.FindScatter(m_index, 2);
    }

    #endregion private method
}