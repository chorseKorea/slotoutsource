﻿using UnityEngine;
using System.Collections;

public class BtnSlotLobby : MonoBehaviour
{
    #region fields

    public GameObject m_goHoverEffect;
    private UITweener m_hoverEffectUITweener;

    public GameObject m_goBgHover;
    private UITweener m_bgUITweenr;

    #endregion

    #region mono

    private void OnEnable()
    {
        OnNormal ();
    }

    #endregion

    #region public Method

    public void OnNormal()
    {
        m_goHoverEffect.SetActive (false);
        m_goBgHover.SetActive (false);
        
    }

    public void OnHoverON()
    {
        m_goHoverEffect.SetActive (true);
        m_goBgHover.SetActive (true);

        m_bgUITweenr = m_goBgHover.GetComponent<UITweener> ();
        m_hoverEffectUITweener = m_goHoverEffect.GetComponent<UITweener> ();
    }

    public void OnHoverOut()
    {
        m_goHoverEffect.SetActive (false);
        m_goBgHover.SetActive (false);
    }

    public void OnPress()
    {
        m_goHoverEffect.SetActive (false);
        
        Color colorBgSprite = m_goBgHover.GetComponent<UISprite> ().color;
        colorBgSprite.a = 1f;
        m_bgUITweenr.enabled = false;
        m_goBgHover.GetComponent<UISprite> ().color = colorBgSprite;

    }
    public void OnClick()
    {
        m_goHoverEffect.SetActive (true);
        m_goBgHover.SetActive (true);
        m_bgUITweenr.Play (true);
    }

    #endregion

}
