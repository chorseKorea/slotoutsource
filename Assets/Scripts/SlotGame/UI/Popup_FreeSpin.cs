﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ChorseBase;

public class Popup_FreeSpin : UI_Script 
{
    #region fields

    private int m_numFreeSpin = 0;
    
    [SerializeField]
    private int m_currentPageIndex = 0;
    public List<GameObject> m_goPages = new List<GameObject>();
    public SpriteNumberLabel m_labelFreespinNumber;
    public SpriteNumberLabel m_labelFreespinResult;

    public GameObject m_goResultPage;

    #endregion

    #region override method

    public override void DepthProcess ()
    {
        m_currDepth = childPanels[0].depth;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.depth = ++m_currDepth;
        }
    }

    public override void DirectionStart ()
    {
        //Debug.Log("열기 연출 시작!");
    }

    public override void DirectionEnd ( eUI_TYPE eType )
    {
        //Debug.Log("닫기 연출 시작!");
        _UIClose ();
    }

    public override void _UIClose ()
    {
        gameObject.SetActive (false);
    }
    public override void SetRenderQ()
    {
        int RQ = UI_Script_Mng.s_iPOPUP_RQ;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.renderQueue = UIPanel.RenderQueue.Explicit;
            item.startingRenderQueue = RQ++;
        }
    }

    #endregion override method

    #region mono
    private void Awake ()
    {
        childPanels = gameObject.GetComponentsInChildren<UIPanel> ();
        UIType = eUI_TYPE.Popup_FreeSpin;
    }

    private void OnEnable()
    {
        for(int i = 0 ; i < m_goPages.Count ; i++)
        {
            m_goPages[i].SetActive (false);
        }

        m_goResultPage.SetActive (false);
    }

    private void OnDisable()
    {

    }

    #endregion

    #region public getter 

    public int GetCurrentPageIndex
    {
        get
        {
            return m_currentPageIndex;
        }
    }

    #endregion

    #region public interface
    public void init ()
    {
        m_numFreeSpin = 0;
        m_currentPageIndex = 0;
    }
    public void EnableCurrentPage ()
    {
        for (int pageIdx = 0; pageIdx < m_goPages.Count; pageIdx++)
        {
            GameObject goPage = m_goPages[pageIdx];

            if (pageIdx == m_currentPageIndex)
            {
                goPage.SetActive (true);

                UITweener[] arrUiTweener = goPage.GetComponentsInChildren<UITweener> ();

                for (int itemIdx = 0; itemIdx < arrUiTweener.Length; itemIdx++)
                {
                    D.Log ("play ui tween ");
                    arrUiTweener[itemIdx].Play ();
                }
            }
            else
            {
                goPage.SetActive (false);
            }
        }
    }

    public void ToNextPage()
    {
        m_currentPageIndex++;
    }

    public void SetFreeSpinNumber(int numFreeSpin)
    {
        m_numFreeSpin = numFreeSpin;
        m_labelFreespinNumber.SetValue (m_numFreeSpin);
    }

    public IEnumerator ShowFreeSpinResult(int amountWin , float time)
    {
        m_goResultPage.SetActive (true);

        yield return m_labelFreespinResult.TweenLabelNumberTask (amountWin , time).UntilDone;
    }

    #endregion

    #region private method
    #endregion
}
