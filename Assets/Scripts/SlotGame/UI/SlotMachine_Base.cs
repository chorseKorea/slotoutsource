﻿using ChorseBase;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public struct SlotBtnPair
{
    public string name;
    public GameObject btnGo;
}

public class SlotMachine_Base : UI_Script
{
    #region fields

    private Scene_Slot m_sceneSlot = null;

    // tile prefab
    [SerializeField]
    private GameObject m_PrefabTileSet;

    [SerializeField]
    private Vector2 m_tileSize;

    // gap between tiles
    [SerializeField]
    private Vector2 m_gapSizeTile;

    [SerializeField]
    private List<GameObject> m_listReels = new List<GameObject>(); // created lines

    [SerializeField]
    private GameObject m_pivotLine; // pivot point for line;

    [SerializeField]
    private int m_NumInHorizontalLine = 5; // horizontal line

    [SerializeField]
    private int m_NumInVerticalLine = 3; // vertical line number

    private int m_balance;
    private int m_numOfBettingLine = 25;

    private int m_betIndex = 4; // default bet amount is 50
    private int m_betLineIndex = 4; // default line index is 20
    private int m_bet = 1;
    private int m_totalBet = 25;
    private int m_numFreespin = 0;

    [SerializeField]
    private int m_numScatter = 0;

    [SerializeField]
    private GameObject m_goFreeSpinIndicator;

    [SerializeField]
    private TweenPosition m_tweenPosTitle;

    [SerializeField]
    private TweenAlpha m_tweenAlphaTitle;

    private bool m_isSpin = false;
    private bool m_isStopable = false;

    [SerializeField]
    private bool m_isAuto = false;

    [SerializeField]
    private bool m_isFreeSpin = false;

    private bool m_isFreeSpinLineAnimOn = false;
    private bool m_isHighlight = false;

    //number reel spining action and time
    [SerializeField]
    private int m_numSpinPerReel = 10;

    private int m_deltaNumSpinPerReel = 0;

    [SerializeField]
    private float m_timePerSpin = 0.1f;

    private float m_deltaTimePerSpin = 0.0f;

    [SerializeField]
    private float m_timeSpinGap = 0.2f;

    // repeat number of highlight line display per each line
    [SerializeField]
    private int m_numRepeatHighlight = 2;

    //labels
    [SerializeField]
    private SpriteNumberLabel m_labelWin;

    [SerializeField]
    private SpriteNumberLabel m_labelBet;

    [SerializeField]
    private SpriteNumberLabel m_labelTotalBet;

    [SerializeField]
    private SpriteNumberLabel m_labelLine;

    [SerializeField]
    private SpriteNumberLabel m_labelBalance;

    [SerializeField]
    private SpriteNumberLabel m_labelFreeSpin;

    [SerializeField]
    private SpriteNumberLabel m_labelJackpot;

    [SerializeField]
    private SpriteNumberLabel m_labelFreespinWin;

    private List<int> m_listBingoLineIndex;
    private List<int> m_listBingoCount;

    private Dictionary<int, List<LineIndicatorBox>> m_mapLineBoxSortByOrder = new Dictionary<int, List<LineIndicatorBox>>();

    [SerializeField]
    private List<LineIndicatorBox> m_mapLineBoxSortByLineIndex = new List<LineIndicatorBox>(0);

    [SerializeField]
    private HighlightLine m_highlightLine;

    [SerializeField]
    private GameObject m_btnSpin;

    [SerializeField]
    private GameObject m_btnStop;

    [SerializeField]
    private GameObject m_btnAuto;

    [SerializeField]
    private List<SlotBtnPair> m_listBtn = new List<SlotBtnPair>();

    /// Reel model
    [SerializeField]
    private GameObject m_goReelBase;

    private Task m_taskLineBoxFreeSpinAnim = null;
    private Task m_taskHighlitLine = null;
    private Task m_taskSpin = null;
    private Task m_taskSpinStop = null;

    #endregion fields

    #region mono

    private void Awake()
    {
        childPanels = gameObject.GetComponentsInChildren<UIPanel>();
        UIType = eUI_TYPE.SlotMachine_Base;
    }

    private void OnEnable()
    {
        if (Main._Inst == null)
            return;

        if (Main._Inst._CUR_SCENE == eSCENE.SLOT)
        {
            Scene_Slot scene_Slot = Main._Inst._CUR_GAME_SCENE as Scene_Slot;
            m_sceneSlot = scene_Slot;
        }

        InitSlot();
    }

    private void OnDisable()
    {
        ClearHighlight();

        if (m_listReels != null && m_listReels.Count > 0)
        {
            for (int i = m_listReels.Count - 1; i >= 0; i--)
            {
                GameObject.Destroy(m_listReels[i]);
            }

            m_listReels = new List<GameObject>();
        }
    }

#if UNITY_EDITOR

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            D.Log("show free spin indicate popup");
            Task.Create(DoIndicateFreeSpinPopup(10, 5));
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            D.Log("Show free spin result popup");
            Task.Create(DoShowFreeSpinResultPopup(100000000, 5));
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            D.Log("show jackpot popup");
            Task.Create(DoIndicateJackpotPopup(1000000, 2));
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            Task.Create(DoShowWinPopup(false, 1000000, 2));
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            Task.Create(DoShowWinPopup(true, 1000000, 2));
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
        }
    }

#endif

    #endregion mono

    #region override method

    public override void DepthProcess()
    {
        m_currDepth = childPanels[0].depth;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.depth = ++m_currDepth;
        }
    }

    public override void DirectionStart()
    {
        //Debug.Log("열기 연출 시작!");
    }

    public override void DirectionEnd(eUI_TYPE eType)
    {
        //Debug.Log("닫기 연출 시작!");
        _UIClose();
    }

    public override void _UIClose()
    {
        gameObject.SetActive(false);
    }

    #endregion override method

    #region private method

    private void InitLabels()
    {
        SetWin(0);
        SetLine();
        SetBet();
    }

    private void RefreshData()
    {
        if (!m_isFreeSpin)
        {
            if (!SlotDataMgr._Inst.m_isRecievedSpinResult)
            {
                SlotDataMgr._Inst.JoinSlot(GamePlayer._Inst._UserToken, (uint)GamePlayer._Inst._AccountMoney, true);

            }
            else
            {
                SlotDataMgr._Inst.JoinSlot(GamePlayer._Inst._UserToken, (uint)SlotDataMgr._Inst.m_amountBalance, true);
            }
        }
        else
        {
            if (!SlotDataMgr._Inst.m_isRecievedSpinResult)
            {
                SlotDataMgr._Inst.JoinBonusSLot(GamePlayer._Inst._UserToken, (uint)SlotDataMgr._Inst.m_amountBalance, true);
            }
            else
            {
                SlotDataMgr._Inst.JoinBonusSLot(GamePlayer._Inst._UserToken, (uint)SlotDataMgr._Inst.m_amountBalance, true);
            }

        }
    }

    private void InitSlot()
    {
        //m_betIndex = 0;
        //m_betLineIndex = 0;
        m_isAuto = false;
        m_isFreeSpin = false;
        m_numFreespin = 0;
        m_isSpin = false;

        SortLineBox();
        CreateReels();

        RefreshData();

        InitLabels();
        DisplayFreeSpins(false);

        GameSoundMgr._Inst.PlayLoopSound("Slots_Classic7_Entry_Bgm", true);
        Task.Create(this, PlayTitleAlphaAnimRandomly());
    }

    private void SortLineBox()
    {
        LineIndicatorBox[] allLineBox = this.GetComponentsInChildren<LineIndicatorBox>(); // get all linebox attached in this object
        m_mapLineBoxSortByLineIndex = allLineBox.ToList();
        m_mapLineBoxSortByLineIndex.Sort((listBox_x, listBox_y) => listBox_x.GetLineIndex.CompareTo(listBox_y.GetLineIndex));

        for (int i = 0; i < allLineBox.Length; i++)
        {
            LineIndicatorBox lineBox = allLineBox[i];

            if (!m_mapLineBoxSortByOrder.ContainsKey(lineBox.GetOrder))
            {
                List<LineIndicatorBox> listLineBox = new List<LineIndicatorBox>();
                listLineBox.Add(lineBox);

                m_mapLineBoxSortByOrder.Add(lineBox.GetOrder, listLineBox);
            }
            else
            {
                m_mapLineBoxSortByOrder[lineBox.GetOrder].Add(lineBox);
            }
        }
    }

    /// <summary>
    /// generate reels
    /// </summary>
    private void CreateReels()
    {
        m_goReelBase.SetActive(false); // turn off reel ase objejct

        for (int idxReel = 0; idxReel < m_NumInHorizontalLine; idxReel++)
        {
            //creation of line
            GameObject goReel = GameObject.Instantiate(m_goReelBase); // instantiate reel object with given base reel object
            goReel.SetActive(true);
            goReel.name = string.Format("Reel_{00:0}", idxReel);
            goReel.layer = m_pivotLine.gameObject.layer;
            goReel.transform.localScale = Vector3.one;

            // set pivot point as parent object
            goReel.transform.parent = m_pivotLine.transform;

            // set position
            goReel.transform.localPosition = Vector2.zero;
            Vector3 posLine = goReel.transform.localPosition;

            int centerIndex = m_NumInHorizontalLine / 2;
            int positionOffset = idxReel - centerIndex;

            posLine.x += positionOffset * (m_tileSize.x + m_gapSizeTile.x);

            goReel.transform.localPosition = posLine;

            Reel scriptReel = goReel.GetComponent<Reel>();

            scriptReel.CreateTiles(m_PrefabTileSet, m_tileSize, m_gapSizeTile, m_NumInVerticalLine, idxReel);
            m_listReels.Add(goReel);
        }
    }

    private void UpdateTargetLabel(SpriteNumberLabel label, int tweenNumber)
    {
        label.SetValue(tweenNumber);
    }

    private void AddBalance(int val)
    {
        int sum = m_balance + val;
        m_balance = sum;
        m_labelBalance.SetValue(m_balance);
    }

    private void SetLine()
    {
        m_numOfBettingLine = PayData.bettingLine[m_betLineIndex];

        if (m_taskHighlitLine != null)
            m_taskHighlitLine.Kill();

        m_labelLine.SetValue(m_numOfBettingLine);
    }

    private void SetBet()
    {
        m_bet = PayData.betting[m_betIndex];
        m_totalBet = m_bet * m_numOfBettingLine;

        m_labelBet.SetValue(m_bet);
        m_labelTotalBet.SetValue(m_totalBet);
    }

    private void DisplayFreeSpins(bool isON)
    {
        m_goFreeSpinIndicator.SetActive(isON);

        if (m_isFreeSpin == isON)
            return;

        D.Log("free spin is started");

        m_isFreeSpin = isON;
        SlotDataMgr._Inst.m_isFreeSpin = isON;

        if (m_isFreeSpin)
        {
            m_tweenPosTitle.PlayForward();

        }
        else
        {
            m_tweenPosTitle.PlayReverse();
        }

        RefreshData();
    }

    private void SetFreeSpins(int val)
    {
        m_labelFreeSpin.SetValue(val);
    }

    #endregion private method

    #region public API

    public void FindScatter(int targetX, int baseY)
    {
        Reel lineScript = m_listReels[targetX].GetComponent<Reel>();
        for (int idxVer = 0; idxVer < m_NumInVerticalLine; idxVer++)
        {
            TileSet tile = lineScript.Tiles[idxVer + baseY];

            if (tile.GetTileType() == PayData.scatterTypeIndex)
            {
                tile.EnableShowMatch();
                m_numScatter++;

                if (m_numScatter == (PayData.minReqNumScatter - 1) && targetX < (m_NumInVerticalLine / 2))
                {
                    if (!GameSoundMgr._Inst.IsSoundPlaying("Slots_Classic7_Reel_Event"))
                    {
                        GameSoundMgr._Inst.PlayOneSound("Slots_Classic7_Reel_Event");
                    }

                    m_deltaNumSpinPerReel = (int)(m_deltaNumSpinPerReel * 1.5f);
                }
            }
        }

        if (m_numScatter < PayData.minReqNumScatter)
        {
            return;
        }
        else
        {
            m_numFreespin += Mathf.Abs(m_numScatter - (PayData.minReqNumScatter - 1)) * PayData.FreeSpinNumPerScatter;
            SetFreeSpins(m_numFreespin);
        }
    }

    public void ShowPayTable()
    {
        UI_Script_Mng._Inst.GetUI(eUI_TYPE.Popup_Paytable_C7);
    }

    public void MinusBet()
    {
        if (m_isSpin)
            return;

        m_betIndex = (m_betIndex - 1 + PayData.betting.Length) % PayData.betting.Length;
        SetBet();
    }

    public void PlusBet()
    {
        if (m_isSpin)
            return;

        m_betIndex = (m_betIndex + 1) % PayData.betting.Length;
        SetBet();
    }

    public void OnMinusLinePressed()
    {
        if (m_isSpin)
            return;

        m_betLineIndex = (m_betLineIndex - 1 + PayData.bettingLine.Length) % PayData.bettingLine.Length;
        SetLine();
        SetBet();

        m_taskHighlitLine = Task.Create(DrawBetLine(0, 2));
    }

    public void OnPluseLinePressed()
    {
        if (m_isSpin)
            return;

        m_betLineIndex = (m_betLineIndex + 1) % PayData.bettingLine.Length;
        SetLine();
        SetBet();

        m_taskHighlitLine = Task.Create(DrawBetLine(0, 2));
    }

    public void SetJackPot(int val)
    {
        m_labelJackpot.SetValue(val);
    }

    public void SetBalance(int val)
    {
        int tweenBalance;
        tweenBalance = m_balance;
        m_balance = val;

        m_labelBalance.SetValue(m_balance);
        //TweenLabelNumber (m_labelBalance , tweenBalance , m_balance , iTween.EaseType.easeInOutQuad );
    }

    public void SetWin(int val)
    {
        m_labelWin.SetValue(val);
    }

    public void OnSpinPressed()
    {
        Spin();
    }

    public void OnAutoPressed()
    {
        m_isAuto = !m_isAuto;

        if (m_isAuto)
        {
            Spin();
        }
    }

    public void OnStopPressed()
    {
        if (m_taskSpinStop != null && m_taskSpinStop.Running)
        {
            return;
        }

        m_isStopable = false;
        m_taskSpinStop = Task.Create(this, StopSpinCoroutine());
    }

    public void OnLobbyPressed()
    {
        SlotDataMgr._Inst.ExitSlot();
    }

    #endregion public API

    #region private method

    /// <summary>
    ///  set target btn enable or disable
    /// </summary>
    /// <param name="btn"></param>
    /// <param name="isEnable"></param>
    private void SetTargetBtnEnable(UIButton btn, bool isEnable)
    {
        if (isEnable)
        {
            btn.disabledColor = btn.disabledColor;
            btn.SetState(UIButtonColor.State.Normal, true);
            btn.enabled = true;
        }
        else
        {
            btn.disabledColor = new Color(170, 170, 170, 1);
            btn.SetState(UIButtonColor.State.Disabled, true);
            btn.enabled = false;
        }
    }

    /// <summary>
    /// get gameobject of button with name from button list
    /// </summary>
    /// <param name="nameBtn"></param>
    /// <returns></returns>
    private GameObject GetBtnGoWithNameFromList(string nameBtn)
    {
        GameObject goBtn = null;

        if (m_listBtn != null && m_listBtn.Count > 0)
        {
            goBtn = m_listBtn.Find(item => item.name == nameBtn).btnGo;
        }

        return goBtn;
    }

    /// <summary>
    /// Get Uibutton with name from button list
    /// </summary>
    /// <param name="nameBtn"></param>
    /// <returns></returns>
    private UIButton GetUIButtonWithNameFromList(string nameBtn)
    {
        GameObject goBtn = null;
        UIButton btn = null;

        goBtn = GetBtnGoWithNameFromList(nameBtn);

        if (goBtn != null)
        {
            btn = goBtn.GetComponent<UIButton>();
        }

        return btn;
    }

    private void TurnOnLineBoxes(List<int> listLineIndex)
    {
        for (int i = 0; i < m_mapLineBoxSortByLineIndex.Count; i++)
        {
            LineIndicatorBox linebox = m_mapLineBoxSortByLineIndex[i];
            linebox.DoTurnBg(false);

            if (listLineIndex != null && listLineIndex.Count > 0)
            {
                if (listLineIndex.Contains(linebox.GetLineIndex))
                {
                    linebox.DoTurnBg(true);
                }
            }
        }
    }

    private void TurnOnLineBox(int lineIndex)
    {
        for (int i = 0; i < m_mapLineBoxSortByLineIndex.Count; i++)
        {
            LineIndicatorBox linebox = m_mapLineBoxSortByLineIndex[i];
            linebox.DoTurnBg(false);

            if (linebox.GetLineIndex == lineIndex)
                linebox.DoTurnBg(true);
        }
    }

    private void Spin()
    {
        if (m_isSpin)
        {
            return;
        }

        ClearHighlight();
        SetWin(0);
        TurnOnLineBoxes(null);
        m_numScatter = 0;

        m_deltaNumSpinPerReel = m_numSpinPerReel;
        m_deltaTimePerSpin = m_timePerSpin;
        m_taskSpin = Task.Create(this, SpinCoroutine(), true);
    }

    private IEnumerator ShowResultCoroutine()
    {
        m_isSpin = false;

        FindMatch(0, 2);

        if (SlotDataMgr._Inst.m_amountWin > 0 && !m_isFreeSpin)
        {
            if (SlotDataMgr._Inst.m_amountWin >= m_totalBet * 5)
            {
                string nameWinSound = string.Empty;
           
                for(int i = 0; i < SlotDataMgr._Inst.m_listBingCount.Count ; i++)
                {
                    if(SlotDataMgr._Inst.m_listBingCount[i] >= 5)
                    {
                        nameWinSound = "5ofakind";
                    }
                    else
                    {
                        if (SlotDataMgr._Inst.m_amountWin >= m_totalBet * 50)
                        {
                            nameWinSound = "Amazing";
                        }
                        else if (SlotDataMgr._Inst.m_amountWin >= m_totalBet * 10)
                        {
                            nameWinSound = "Bravo";
                        }
                        else
                        {
                            nameWinSound = "Super";
                        }
                    }
                }

                GameSoundMgr._Inst.PlayOneSound(nameWinSound);

                yield return new WaitForSeconds(2f);

                yield return Task.Create(DoShowWinPopup(true, SlotDataMgr._Inst.m_amountWin, 2));
            }
            else
            {
                yield return Task.Create(DoShowWinPopup(false, SlotDataMgr._Inst.m_amountWin, 2));
            }

            SetWin(SlotDataMgr._Inst.m_amountWin);
            SetJackPot((int)SlotDataMgr._Inst.m_rewardJackpot);

            m_labelWin.TweenLabelNumberTask(0, 2.0f);
            GameSoundMgr._Inst.PlayLoopSound("CoinChange");
            yield return m_labelBalance.TweenLabelNumberTask(m_balance + SlotDataMgr._Inst.m_amountWin, 2.0f).UntilDone;
            GameSoundMgr._Inst.StopPlaySound("CoinChange");
            AddBalance(SlotDataMgr._Inst.m_amountWin);
        }

        if (m_numFreespin > 0)
        {
            if (!m_isFreeSpin)
            {
                yield return Task.Create(DoIndicateFreeSpinPopup(m_numFreespin, 5)).UntilDone;
                DisplayFreeSpins(true);
                SlotDataMgr._Inst.m_amountWinFreenSpin = 0;
                m_labelFreespinWin.SetValue(0);
            }

            if (m_taskLineBoxFreeSpinAnim != null && !m_taskLineBoxFreeSpinAnim.Running)
            {
                m_taskLineBoxFreeSpinAnim = Task.Create(this, PlayLineBoxAnimationOnFreespin(0.1f));
            }

            SlotDataMgr._Inst.m_amountWinFreenSpin += SlotDataMgr._Inst.m_amountWin;
            m_labelFreespinWin.SetValue(SlotDataMgr._Inst.m_amountWinFreenSpin);
            Spin();
        }
        else if (m_isAuto)
        {
            Spin();
        }
    }

    private void FindMatch(int baseX, int baseY)
    {
        m_listBingoLineIndex = SlotDataMgr._Inst.m_listBingoLineIndex;
        m_listBingoCount = SlotDataMgr._Inst.m_listBingCount;

        DisplayHighlight(baseX, baseY);
    }

    private void ClearHighlight()
    {
        if (m_taskHighlitLine != null)
            m_taskHighlitLine.Kill();

        m_isHighlight = false;
        m_highlightLine.ClearLines();
        m_highlightLine.gameObject.SetActive(false);
        DisableAllSlotAnimation();
    }

    private void DisplayHighlight(int baseX, int baseY)
    {
        if (m_isHighlight)
            return;

        if (m_listBingoLineIndex.Count < 1)
            return;

        m_isHighlight = true;
        m_highlightLine.gameObject.SetActive(true);

        if (m_taskHighlitLine != null)
            m_taskHighlitLine.Kill();

        if (m_taskLineBoxFreeSpinAnim != null)
        {
            m_taskLineBoxFreeSpinAnim.Kill();
        }

        m_taskHighlitLine = Task.Create(PlayLineHighlightAnimation(baseX, baseY));
    }

    private void DisableAllSlotAnimation()
    {
        foreach (GameObject goLine in m_listReels)
        {
            Reel lineScript = goLine.GetComponent<Reel>();

            foreach (TileSet tScript in lineScript.Tiles)
            {
                tScript.DisableShowMatch();
            }
        }
    }

    private void EnableMatchSlotAnimation(int idxLine, int count, int baseX, int baseY)
    {
        int bingLineIdx = idxLine;
        List<Vector3> path = new List<Vector3>();

        for (int idxHor = 0; idxHor < m_NumInHorizontalLine; idxHor++)
        {
            int pos = PayData.line[bingLineIdx, idxHor];

            Reel lineScript = m_listReels[idxHor + baseX].GetComponent<Reel>();
            TileSet choiceTile = lineScript.Tiles[pos + baseY];
            Vector3 choiceTilePos = choiceTile.transform.localPosition + lineScript.transform.localPosition;
            float sizePadding = 10;
            Vector2 rectSize = new Vector2(m_tileSize.x - sizePadding, m_tileSize.y - sizePadding) + m_gapSizeTile;

            if (idxHor < count)
            {
                choiceTile.EnableShowMatch();
            }
        }
    }

    private void DisableMatchSlotAnimation(int idxLine, int count, int baseX, int baseY)
    {
        int bingLineIdx = idxLine;

        for (int idxHor = 0; idxHor < m_NumInHorizontalLine; idxHor++)
        {
            int pos = PayData.line[bingLineIdx, idxHor];

            Reel lineScript = m_listReels[idxHor + baseX].GetComponent<Reel>();
            TileSet choiceTile = lineScript.Tiles[pos + baseY];

            choiceTile.DisableShowMatch();
        }
    }

    private void SetLinePath(int idxLine, int count, int baseX, int baseY)
    {
        int bingLineIdx = idxLine;
        List<Vector3> path = new List<Vector3>();

        for (int idxHor = 0; idxHor < m_NumInHorizontalLine; idxHor++)
        {
            int pos = PayData.line[bingLineIdx, idxHor];

            Reel lineScript = m_listReels[idxHor + baseX].GetComponent<Reel>();
            TileSet choiceTile = lineScript.Tiles[pos + baseY];
            Vector3 choiceTilePos = choiceTile.transform.localPosition + lineScript.transform.localPosition;
            float sizePadding = 10;
            Vector2 rectSize = new Vector2(m_tileSize.x - sizePadding, m_tileSize.y - sizePadding) + m_gapSizeTile;

            if (idxHor < count)
            {
                if (idxHor > 0 && idxHor < (m_NumInHorizontalLine))
                {
                    int preIdxVerticalPos = PayData.line[bingLineIdx, idxHor - 1];

                    Reel preReel = m_listReels[(idxHor - 1) + baseX].GetComponent<Reel>();

                    TileSet preTile = lineScript.Tiles[preIdxVerticalPos + baseY];
                    Vector3 preTilePos = preTile.transform.localPosition + preReel.transform.localPosition;

                    if (preIdxVerticalPos == pos)
                    {
                        path.Add(new Vector3(preTilePos.x + (rectSize.x / 2), preTilePos.y));
                        path.Add(new Vector3(choiceTilePos.x - (rectSize.x / 2), choiceTilePos.y));
                    }
                    else if (preIdxVerticalPos > pos)
                    {
                        path.Add(new Vector3(preTilePos.x + (rectSize.x / 2), preTilePos.y - (rectSize.y / 2)));
                        path.Add(new Vector3(choiceTilePos.x - (rectSize.x / 2), choiceTilePos.y + (rectSize.y / 2)));
                    }
                    else
                    {
                        path.Add(new Vector3(preTilePos.x + (rectSize.x / 2), preTilePos.y + (rectSize.y / 2)));
                        path.Add(new Vector3(choiceTilePos.x - (rectSize.x / 2), choiceTilePos.y - (rectSize.y / 2)));
                    }
                    m_highlightLine.SetPath(bingLineIdx, path.ToArray());
                }
                Vector3[] rect = new Vector3[6];

                rect[0] = new Vector3(-rectSize.x / 2, rectSize.y / 2) + choiceTilePos;
                rect[1] = new Vector3(rectSize.x / 2, rectSize.y / 2) + choiceTilePos;
                rect[2] = new Vector3(rectSize.x / 2, -rectSize.y / 2) + choiceTilePos;
                rect[3] = new Vector3(-rectSize.x / 2, -rectSize.y / 2) + choiceTilePos;
                rect[4] = new Vector3(-rectSize.x / 2, rectSize.y / 2) + choiceTilePos;
                rect[5] = new Vector3(rectSize.x / 2, rectSize.y / 2) + choiceTilePos;

                m_highlightLine.SetPath(bingLineIdx, rect, e_lineLayerType.Rect);
                path.Clear();
            }
            else
            {
                if (idxHor == 0)
                {
                    path.Insert(0, new Vector3(choiceTilePos.x - rectSize.x / 2, choiceTilePos.y));
                }
                else if (count > 0 && idxHor < (m_NumInHorizontalLine))
                {
                    int preIdxVerticalPos = PayData.line[bingLineIdx, idxHor - 1];

                    Reel preReel = m_listReels[(idxHor - 1) + baseX].GetComponent<Reel>();

                    TileSet preTile = lineScript.Tiles[preIdxVerticalPos + baseY];
                    Vector3 preTilePos = preTile.transform.localPosition + preReel.transform.localPosition;

                    if (preIdxVerticalPos == pos)
                    {
                        path.Add(new Vector3(preTilePos.x + (rectSize.x / 2), preTilePos.y));
                    }
                    else if (preIdxVerticalPos > pos)
                    {
                        path.Add(new Vector3(preTilePos.x + (rectSize.x / 2), preTilePos.y - (rectSize.y / 2)));
                    }
                    else
                    {
                        path.Add(new Vector3(preTilePos.x + (rectSize.x / 2), preTilePos.y + (rectSize.y / 2)));
                    }
                }

                if (idxHor == (m_NumInHorizontalLine - 1))
                {
                    path.Add(choiceTilePos);
                    path.Add(new Vector3(choiceTilePos.x + m_tileSize.x / 2, choiceTilePos.y));
                }
                else
                {
                    path.Add(choiceTilePos);
                }

                m_highlightLine.SetPath(bingLineIdx, path.ToArray());
            }
        }
    }

    #endregion private method

    #region private coroutine

    private IEnumerator PlayTitleAlphaAnimRandomly()
    {
        while (m_tweenAlphaTitle.enabled && m_tweenAlphaTitle != null)
        {
            m_tweenAlphaTitle.delay = Random.Range(4.0f, 5.0f);
            m_tweenAlphaTitle.PlayForward();

            yield return new WaitForSeconds(m_tweenAlphaTitle.duration + m_tweenAlphaTitle.delay);
        }
    }

    private IEnumerator DrawBetLine(int baseX, int baseY)
    {
        List<int> listLineIndex = new List<int>();
        TurnOnLineBoxes(null);
        m_highlightLine.ClearLines();

        for (int i = 0; i < m_numOfBettingLine; i++)
        {
            listLineIndex.Add(i);
        }

        for (int idx = 0; idx < listLineIndex.Count; idx++)
        {
            SetLinePath(listLineIndex[idx], 0, baseX, baseY);
        }
        TurnOnLineBoxes(listLineIndex);
        m_highlightLine.DrawAllLine();

        yield return new WaitForSeconds(1f);
    }

    private IEnumerator PlayLineHighlightAnimation(int baseX, int baseY)
    {
        while (m_isHighlight)
        {
            if (m_listBingoLineIndex.Count > 1)
            {
                int repeated = 0;

                while (repeated < m_numRepeatHighlight)
                {
                    for (int i = 0; i < m_listBingoLineIndex.Count; i++)
                    {
                        SetLinePath(m_listBingoLineIndex[i], 0, baseX, baseY);
                    }
                    m_highlightLine.SetRenderOrder(UI_Script_Mng.s_Normal_RQ + UI_Script_Mng.s_Highlight_Line_RQ_Offset);
                    m_highlightLine.DrawAllLine();
                    TurnOnLineBoxes(m_listBingoLineIndex);
                    yield return new WaitForSeconds(0.5f);
                    m_highlightLine.ClearLines();
                    yield return new WaitForSeconds(0.5f);
                    repeated++;
                }

                GameSoundMgr._Inst.PlayOneSound("Slots_Classic7_Pluralwin");
            }

            for (int i = 0; i < m_listBingoLineIndex.Count; i++)
            {
                int repeated = 0;

                EnableMatchSlotAnimation(m_listBingoLineIndex[i], m_listBingoCount[i], baseX, baseY);

                while (repeated < m_numRepeatHighlight)
                {
                    SetLinePath(m_listBingoLineIndex[i], m_listBingoCount[i], baseX, baseY);
                    m_highlightLine.SetRenderOrder(UI_Script_Mng.s_Normal_RQ + UI_Script_Mng.s_Highlight_Line_RQ_Offset);
                    m_highlightLine.DrawSelectedLineIndex(m_listBingoLineIndex[i]);
                    TurnOnLineBox(m_listBingoLineIndex[i]);

                    yield return new WaitForSeconds(0.5f);

                    m_highlightLine.ClearLines();

                    yield return new WaitForSeconds(0.5f);

                    repeated++;
                }
                GameSoundMgr._Inst.PlayOneSound("Slots_Classic7_Win");
                DisableMatchSlotAnimation(m_listBingoLineIndex[i], m_listBingoCount[i], baseX, baseY);
            }
        }

        yield return null;
    }

    private IEnumerator PlayLineBoxAnimationOnFreespin(float interval = 0.1f)
    {
        List<LineIndicatorBox> listLineBoxInSameOrder = null;

        while (m_numFreespin > 0)
        {
            for (int i = 0; i < m_mapLineBoxSortByOrder.Count; i++)
            {
                LineIndicatorBox linebox = null;

                if (listLineBoxInSameOrder != null)
                {
                    for (int tempIterator = 0; tempIterator < listLineBoxInSameOrder.Count; tempIterator++)
                    {
                        linebox = listLineBoxInSameOrder[tempIterator];
                        linebox.DoTurnBg(false);
                    }

                    if (m_numFreespin <= 0)
                        break;
                }

                listLineBoxInSameOrder = m_mapLineBoxSortByOrder[i];

                for (int j = 0; j < listLineBoxInSameOrder.Count; j++)
                {
                    linebox = listLineBoxInSameOrder[j];

                    linebox.DoTurnBg(true);
                }

                yield return new WaitForSeconds(interval);
            }
        }
    }

    // popup related free spin
    private IEnumerator DoIndicateFreeSpinPopup(int numFreespin, float timeInterval)
    {
        GameSoundMgr._Inst.PlayOneSound("Freespin_Start");
        Popup_FreeSpin popupFreeSpin = UI_Script_Mng._Inst.GetUI(eUI_TYPE.Popup_FreeSpin) as Popup_FreeSpin;
        popupFreeSpin.init();
        popupFreeSpin.SetFreeSpinNumber(numFreespin);

        while (popupFreeSpin.GetCurrentPageIndex < popupFreeSpin.m_goPages.Count)
        {
            popupFreeSpin.EnableCurrentPage();
            yield return new WaitForSeconds(timeInterval);
            popupFreeSpin.ToNextPage();
        }

        UI_Script_Mng._Inst.HideUI(eUI_TYPE.Popup_FreeSpin);
    }

    private IEnumerator DoShowWinPopup(bool isBigWin, int amountWin, float timeDelay)
    {
        eUI_TYPE popupType = eUI_TYPE.Popup_Win;

        if (isBigWin)
        {
            popupType = eUI_TYPE.Popup_BigWin;
            Popup_BigWin popup = UI_Script_Mng._Inst.GetUI(eUI_TYPE.Popup_BigWin) as Popup_BigWin;
            yield return popup.StartCoroutine(popup.ShowWinAmount(amountWin, 1f));
        }
        else
        {
            popupType = eUI_TYPE.Popup_Win;
            Popup_Win popup = UI_Script_Mng._Inst.GetUI(eUI_TYPE.Popup_Win) as Popup_Win;
            yield return popup.StartCoroutine(popup.ShowWinAmount(amountWin, 1f));
        }

        yield return new WaitForSeconds(timeDelay);
        UI_Script_Mng._Inst.HideUI(popupType);
    }

    private IEnumerator DoShowFreeSpinResultPopup(int amountFreespinWin, float timeDelay)
    {
        GameSoundMgr._Inst.PlayOneSound("Freespin_End");
        Popup_FreeSpin popupFreeSpin = UI_Script_Mng._Inst.GetUI(eUI_TYPE.Popup_FreeSpin) as Popup_FreeSpin;

        popupFreeSpin.m_labelFreespinResult.SetValue(0);
        yield return popupFreeSpin.StartCoroutine(popupFreeSpin.ShowFreeSpinResult(amountFreespinWin, 1f));
        
        yield return new WaitForSeconds(timeDelay);

        popupFreeSpin.m_labelFreespinResult.SetValue(amountFreespinWin);
        popupFreeSpin.StartCoroutine(popupFreeSpin.ShowFreeSpinResult(0, 1f));
        yield return m_labelBalance.TweenLabelNumberTask(m_balance + amountFreespinWin, 1.0f).UntilDone;
        UI_Script_Mng._Inst.HideUI(eUI_TYPE.Popup_FreeSpin);
    }

    private IEnumerator DoIndicateJackpotPopup(int amountJackpot, float timeInterval)
    {
        Popup_Jackpot popupJackpot = UI_Script_Mng._Inst.GetUI(eUI_TYPE.Popup_Jackpot) as Popup_Jackpot;
        popupJackpot.init();

        while (popupJackpot.GetCurrentPageIndex < popupJackpot.m_goPages.Count)
        {
            popupJackpot.EnableCurrentPage();
            popupJackpot.m_coinSplahHandler.gameObject.SetActive(true);
            popupJackpot.m_coinSplahHandler.StartPlayCoinSplah(Mathf.Clamp(amountJackpot, 10, 100), 0.001f);
            popupJackpot.m_labelJackpotWin.SetValue(0);
            yield return popupJackpot.m_labelJackpotWin.TweenLabelNumberTask(amountJackpot, 5).UntilDone;
            yield return new WaitForSeconds(timeInterval);
            popupJackpot.m_coinSplahHandler.gameObject.SetActive(false);
            popupJackpot.ToNextPage();
        }

        popupJackpot.EnableCurrentPage();

        yield return popupJackpot.StartCoroutine(popupJackpot.ShowJackPotResult(amountJackpot, 3f));
    }

    private IEnumerator StopSpinCoroutine()
    {
        if (m_taskSpin != null && m_taskSpin.Running)
        {
            m_deltaNumSpinPerReel = m_listReels[0].GetComponent<Reel>().Tiles.Length;

            for (int i = 0; i < m_NumInHorizontalLine; i++)
            {
                GameObject line = m_listReels[i];
                Reel lineScript = line.GetComponent<Reel>();

                lineScript.NumOfRoll = m_deltaNumSpinPerReel; // change
            }

            //Stop button is disable while spin is stills running
            SetTargetBtnEnable(m_btnStop.GetComponent<UIButton>(), false);

            //wait untill task spin is done
            yield return m_taskSpin.UntilDone;
        }
        else
        {
            yield return null;
        }

        //stop button is enable after spining is done
        SetTargetBtnEnable(m_btnStop.GetComponent<UIButton>(), true);
    }

    private IEnumerator SpinCoroutine()
    {
        if (m_numFreespin < 1)
        {
            int balance = m_balance - m_totalBet;

            if (balance < 0)
            {
                yield break;
            }
            else
            {
                SetBalance(m_balance - m_totalBet);
            }
        }
        else
        {
            m_numFreespin--;

            if (m_numFreespin <= 0)
            {
                DisplayFreeSpins(false);
            }
            else
            {
                SetFreeSpins(m_numFreespin);
            }
        }

        while (!SlotDataMgr._Inst.m_isRecReelData) // wait until spin result is received by packeg mnger
        {
            yield return null;
        }

        m_isSpin = true;

        yield return null;

        if (!m_isFreeSpin) // if this is not freeSpin
        {
            SlotDataMgr._Inst.SpinReel(m_bet, m_numOfBettingLine);
        }
        else
        {
            SlotDataMgr._Inst.SpinBonusReel(m_bet, m_numOfBettingLine);
        }

        //disable spin button
        SetTargetBtnEnable(m_btnSpin.GetComponent<UIButton>(), false);

        //disable plus minus btns
        SetTargetBtnEnable(GetUIButtonWithNameFromList("BetPlus"), false);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("BetMinus"), false);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("LinePlus"), false);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("LineMinus"), false);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("Lobby"), false);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("PayTable"), false);


        while (!SlotDataMgr._Inst.m_isRecievedSpinResult) // wait until spin result is received by packeg mnger
        {
            yield return null;
        }

        // where actual spin is started
        m_isStopable = true;
        m_btnSpin.SetActive(false); // turn off spin button when stop button is eanbeld
        m_btnStop.SetActive(true); // active stop button;

        GameSoundMgr._Inst.StopPlaySound("Slots_Classic7_Entry_Bgm");
        GameSoundMgr._Inst.PlayLoopSound("Slots_Classic7_Reel_Bgm", true);

        for (int i = 0; i < m_NumInHorizontalLine; i++)
        {
            GameObject line = m_listReels[i];
            Reel scriptReel = line.GetComponent<Reel>();

            if (i != (m_NumInHorizontalLine - 1))
            {
                scriptReel.SpinReel(m_deltaTimePerSpin, m_deltaNumSpinPerReel);
                yield return new WaitForSeconds(m_timeSpinGap);
            }
            else // last reel
            {
                // wait until last reel is done
                yield return scriptReel.SpinReel(m_deltaTimePerSpin, m_deltaNumSpinPerReel).UntilDone;
            }
        }

        m_btnSpin.SetActive(true); // turn on spin button when stop button is off
        m_btnStop.SetActive(false); // turn off stop button

        // enable spin
        SetTargetBtnEnable(m_btnSpin.GetComponent<UIButton>(), true);

        // enable plus minus btns
        SetTargetBtnEnable(GetUIButtonWithNameFromList("BetPlus"), true);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("BetMinus"), true);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("LinePlus"), true);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("LineMinus"), true);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("Lobby"), true);
        SetTargetBtnEnable(GetUIButtonWithNameFromList("PayTable"), true);

        this.StartCoroutine(ShowResultCoroutine()); // start to show result of spin ( checking )
    }

    #endregion private coroutine
}