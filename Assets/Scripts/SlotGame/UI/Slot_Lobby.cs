﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Slot_Lobby : UI_Script
{
    #region fields

    public GameObject m_goGridItem;

    private List<GameObject> m_listGridItem = new List<GameObject>();
    public UIGrid m_Grid;

    public int m_selectedTabIndex = 0;
    public List<UIButton> m_listTabBtn;

    #endregion

    #region override method

    public override void DepthProcess ()
    {
        m_currDepth = childPanels[0].depth;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.depth = ++m_currDepth;
        }
    }

    public override void DirectionStart ()
    {
        //Debug.Log("열기 연출 시작!");
    }

    public override void DirectionEnd ( eUI_TYPE eType )
    {
        //Debug.Log("닫기 연출 시작!");
        _UIClose ();
    }

    public override void _UIClose ()
    {
        gameObject.SetActive (false);
    }

    #endregion override method

    #region mono
    private void Awake ()
    {
        childPanels = gameObject.GetComponentsInChildren<UIPanel> ();
        UIType = eUI_TYPE.Slot_Lobby;
    }

    private void OnEnable()
    {
        InitSlotLobby ();
    }

    #endregion

    #region private method

    private void InitSlotLobby()
    {
        InitTabBtns ();

        PacketMng._Inst.SEND_USERINFO_REQ (GamePlayer._Inst._ID , GamePlayer._Inst._PW);
        //this.StartCoroutine (InitScrollView ());
    }

    private void InitTabBtns()
    {
        for(int i = 0 ; i < m_listTabBtn.Count ; i++)
        {
            UIButton tabBtn = m_listTabBtn[i];

            if( i == m_selectedTabIndex )
            {
                tabBtn.SetState(UIButtonColor.State.Disabled,true);
            }
            else
            {
                tabBtn.SetState (UIButtonColor.State.Normal , true);
            }
        }
    }

    private IEnumerator InitScrollView ()
    {
        yield return null;

        int listCount = 3; 

        for (int i = 0; i < listCount; i++)
        {
            GameObject item = Instantiate (m_goGridItem) as GameObject;

            item.gameObject.transform.parent = m_Grid.gameObject.transform;
            item.gameObject.transform.localPosition = Vector3.zero;
            item.gameObject.transform.localScale = Vector3.one;
            m_listGridItem.Add (item);
        }

        yield return null;
        m_Grid.Reposition ();

    }

    #endregion

    #region public interface

    public void OnC7Clicked()
    {
        Main._Inst._CUR_SCENE = eSCENE.SLOT;
    }

    #endregion
}
