﻿using UnityEngine;
using System.Collections;

public class Popup_BigWin : UI_Script {

    #region fields
    [SerializeField]
    public SpriteNumberLabel m_labelWin;
    public GameObject m_goIndicator;
    public CoinSplashHandler m_coinSplash;

    #endregion

    #region override method

    public override void DepthProcess()
    {
        m_currDepth = childPanels[0].depth;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.depth = ++m_currDepth;
        }
    }

    public override void DirectionStart()
    {
        //Debug.Log("열기 연출 시작!");
    }

    public override void DirectionEnd(eUI_TYPE eType)
    {
        //Debug.Log("닫기 연출 시작!");
        _UIClose();
    }

    public override void _UIClose()
    {
        gameObject.SetActive(false);
    }
    public override void SetRenderQ()
    {
        int RQ = UI_Script_Mng.s_iPOPUP_RQ;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.renderQueue = UIPanel.RenderQueue.Explicit;
            item.startingRenderQueue = RQ++;
        }
    }

    #endregion override method

    #region mono
    private void Awake()
    {
        childPanels = gameObject.GetComponentsInChildren<UIPanel>();
        UIType = eUI_TYPE.Popup_BigWin;
    }

    private void OnEnable()
    {
        m_goIndicator.SetActive(false);
    }

    private void OnDisable()
    {

    }

    #endregion

    #region public interface

    public IEnumerator ShowWinAmount(int amountWin, float time)
    {
        m_goIndicator.SetActive(true);

        yield return null;
        m_labelWin.SetValue(0);
        m_coinSplash.gameObject.SetActive(true);
        m_coinSplash.StartPlayCoinSplah(Mathf.Clamp(amountWin, 5, 30), 0.01f);
        yield return m_labelWin.TweenLabelNumberTask(amountWin, time).UntilDone;
    }

    #endregion

}
