﻿using UnityEngine;
using System.Collections;

public class GamePlayer : MonoBehaviour
{
    #region singleton Instance

    private static GamePlayer m_instance;
    public static GamePlayer _Inst
    {
        get
        {
            return m_instance;
        }
    }
    #endregion

    #region userInfo

    [SerializeField]
    private string m_id = string.Empty; // user id
    [SerializeField]
    private string m_pw = string.Empty; // user password


    [SerializeField]
    private long m_moneyAccount = 0; // total money for user
    [SerializeField]
    private string m_tokenUser = string.Empty; // user token 
    [SerializeField]
    private long m_timeServer = 0; // server time

    #endregion

    #region public getter and setter

    public string _ID
    {
        get
        {
            return m_id;
        }
    }

    public string _PW
    {
        get
        {
            return m_pw;
        }
    }

    public long _AccountMoney
    {
        get
        {
            return m_moneyAccount;
        }
        set
        {
            m_moneyAccount = value;
        }
    }

    public string _UserToken
    {
        get
        {
            return m_tokenUser;
        }
        set
        {
            m_tokenUser = value;
        }
    }

    public long _ServerTime
    {
        get
        {
            return m_timeServer;
        }
        set
        {
            m_timeServer = value;
        }
    }
    #endregion

    #region mono

    private void Awake()
    {
        m_instance = this;
    }

    #endregion

}
