﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileSet : MonoBehaviour 
{
	#region fields

	[SerializeField]
	private int m_index;

	[SerializeField]
	private float m_height = 1f;
    private float m_width = 0;

    private Vector2 m_gap;

	// tile type
    [SerializeField]
	int m_type = 0;
	
	Transform m_transform;

	[SerializeField]
	private GameObject[] m_slots;
	
	// Tile Line Component
	[SerializeField]
	private Reel m_lineScript;
	
	// Condition when tile is moving
	[SerializeField]
	private bool m_isMove = false;

    [SerializeField]
    GameObject m_slot;
	
    Dictionary<int,GameObject> m_DictInstSlot = new Dictionary<int,GameObject>(); // slot prefab instance
    [SerializeField] GameObject m_prefabParticle; // partile prefab
	
    GameObject m_instanceParticle; // particle instance

    [SerializeField]
    iTween.EaseType m_easeType = iTween.EaseType.linear;

  
	#endregion

	#region public getter

	public int Index
	{
		get
		{
			return m_index;
		}
		set
		{
			m_index = value;
		}
	}

	public float Height
	{
		get
		{
			return m_height;
		}
		set
		{
			m_height = value;
		}

	}

    public float Width
    {
        get
        {
            return m_width;
        }
        set
        {
            m_width = value;
        }

    }

    public Vector2 Gap
    {
        get
        {
            return m_gap;
        }
        set
        {
            m_gap = value;
        }
    }
	public Reel LineScript
	{
		get
		{
			return m_lineScript;
		}
		set
		{
			m_lineScript = value;
		}
	}

	public bool IsMove
	{
		get
		{
			return m_isMove;
		}
	}

    public GameObject[] Slots
    {
        get
        {
            return m_slots;
        }
    }

    public iTween.EaseType GetEasetype
    {
        get
        {
            return m_easeType;
        }
    }


	#endregion

	void Awake()
	{
		m_transform = transform;

        InitSlots();

	}
    private void OnDrawGizmos ()
    {
        Vector3[] rect = new Vector3[6];

        rect[0] = new Vector3 (-m_width / 2 , m_height / 2) + this.transform.localPosition;
        rect[1] = new Vector3 (m_width / 2 , m_height / 2) + this.transform.localPosition;
        rect[2] = new Vector3 (m_width / 2 , -m_height / 2) + this.transform.localPosition;
        rect[3] = new Vector3 (-m_width / 2 , -m_height / 2) + this.transform.localPosition;
        rect[4] = new Vector3 (-m_width / 2 , m_height / 2) + this.transform.localPosition;
        rect[5] = new Vector3 (m_width / 2 , m_height / 2) + this.transform.localPosition;

        Gizmos.color = Color.green;
        for (int i = 0; i < rect.Length - 1; ++i)
        {
            Gizmos.DrawLine (gameObject.transform.TransformPoint (rect[i]) , gameObject.transform.TransformPoint (rect[i + 1]));
        }
    }

    void InitSlots()
    {
        if (m_slots.Length > 0)
        {
            for (int i = 0; i < m_slots.Length; i++)
            {
                if (m_DictInstSlot.ContainsKey(i))
                    continue;

                GameObject goSlot;
                goSlot = GameObject.Instantiate(m_slots[i]) as GameObject;

                goSlot.transform.parent = this.transform;
                goSlot.transform.localPosition = Vector3.zero;
                goSlot.transform.localScale = new Vector3(1f, 1f);
                goSlot.transform.localEulerAngles = new Vector3(0, 0, 0);

                m_DictInstSlot.Add(i, goSlot);

                goSlot.SetActive(false);
            }
        }
    }
	
	// Setup Tile Type.
	public void SetTileType(int type)
	{
		m_type = type;

        if (m_slot != null)
            m_slot.SetActive(false);

        GameObject goSlot;
        goSlot = m_DictInstSlot[type];
        goSlot.SetActive(true);

        goSlot.transform.parent = this.transform;
        goSlot.transform.localPosition = Vector3.zero;
        goSlot.transform.localScale = new Vector3(1f, 1f);
        goSlot.transform.localEulerAngles = new Vector3(0, 0, 0);

        goSlot.layer = this.gameObject.layer;

        foreach (Transform child in goSlot.GetComponentsInChildren<Transform>())
        {
            child.gameObject.layer = this.gameObject.layer;
        }

        m_slot = goSlot;
        

        DisableShowMatch ();
	}
	
	public void EnableShowMatch()
	{
        foreach (Transform child in m_slot.GetComponentsInChildren<Transform> ())
        {
            child.gameObject.layer = this.gameObject.layer;
        }

        SlotController slotController = m_slot.GetComponent<SlotController> ();

        if(slotController != null)
        {
            slotController.PlaySlotItween (true);
        }

        if (m_prefabParticle != null && m_instanceParticle == null)
		{
            GameObject instanceParticle = NsEffectManager.CreateReplayEffect(m_prefabParticle);
			NsEffectManager.PreloadResource(m_instanceParticle);

            instanceParticle.transform.parent = this.transform;
            instanceParticle.SetActive(true);
            instanceParticle.transform.localEulerAngles = new Vector3 (-90 , 0 , 0);
            instanceParticle.transform.localPosition = Vector3.zero;

            ParticleRenderer[] particles = instanceParticle.GetComponentsInChildren<ParticleRenderer> ();

            for (int i = 0; i < particles.Length; i++)
            {
                particles[i].sortingOrder = UI_Script_Mng.s_Normal_RQ + UI_Script_Mng.s_Particle_RQ_Offset++;
            }
    
            NsEffectManager.RunReplayEffect (instanceParticle , true);

            m_instanceParticle = instanceParticle;
		}

	}
	
	public void DisableShowMatch()
	{
        foreach (Transform child in m_slot.GetComponentsInChildren<Transform> ())
        {
            child.gameObject.layer = this.gameObject.layer;
        }

        SlotController slotController = m_slot.GetComponent<SlotController> ();

        if (slotController != null)
        {
            slotController.StopSlotItween();
        }

		if(m_instanceParticle != null)
		{
            m_instanceParticle.SetActive(false);
            GameObject.Destroy(m_instanceParticle);

            if (m_slot.GetComponentInChildren<UISpriteAnimation> () != null)
            {
                m_slot.GetComponentInChildren<UISpriteAnimation> ().enabled = false;
            }
		}
	}
	
	// Get Tile Type.
	public int GetTileType()
	{
		return m_type;
	}
	
	public void ShowMatch(float t)
	{
		EnableShowMatch();
	}
	
}
