﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ChorseBase;

public class SlotController : MonoBehaviour
{
    #region fields

    //private UISpriteAnimation m_spriteAnim;
    //private UISprite m_sprite;

    private Animator m_animator;

    private string m_initSpriteName = string.Empty;

    private Dictionary<int , List<SlotItween>> m_dictSlotItweens = new Dictionary<int , List<SlotItween>> ();

    private bool m_debug = false;

    [SerializeField]
    private bool m_isLoop = false;

    private Task m_taskSlotItweenPlaying = null;

    #endregion fields

    #region mono

    private void Awake ()
    {
        //m_spriteAnim = this.GetComponentInChildren<UISpriteAnimation> ();
        //m_sprite = this.GetComponentInChildren<UISprite>();
        //m_initSpriteName = m_sprite.spriteName;

        m_animator = this.GetComponentInChildren<Animator>();

        if(m_animator != null)
        {
            m_animator.enabled = true;
            m_animator.StartPlayback();
            m_animator.SetBool("isPlay", false);
        }

        List<SlotItween> list_AllSlotItween = new List<SlotItween> ();
        list_AllSlotItween = this.GetComponents<SlotItween> ().ToList ();

        for (int i = 0; i < list_AllSlotItween.Count; i++)
        {
            SlotItween itemSlotItween = list_AllSlotItween[i];

            if (!m_dictSlotItweens.ContainsKey (itemSlotItween.GetPlayOrder))
            {
                List<SlotItween> listSlotItween = new List<SlotItween> ();
                listSlotItween.Add (itemSlotItween);
                m_dictSlotItweens.Add (itemSlotItween.GetPlayOrder , listSlotItween);
            }
            else
            {
                m_dictSlotItweens[itemSlotItween.GetPlayOrder].Add (itemSlotItween);
            }
        }
    }

    private void OnEnable ()
    {
        if (m_debug)
        {
            PlaySlotItween (true);
        }
    }

    private void OnDisable ()
    {
    }

    #endregion mono

    #region public methods


    public void PlaySlotItween ( bool isLoop = false )
    {
        m_isLoop = isLoop;

        if (m_dictSlotItweens.Count > 0 && (m_taskSlotItweenPlaying == null || !m_taskSlotItweenPlaying.Running))
        {
			m_taskSlotItweenPlaying = Task.Create(this,PlaySlotItweenCoroutine(),true);
        }

        if (m_animator != null)
        {
            m_animator.SetBool("isPlay", true);
        }
    }

    public void StopSlotItween ()
    {
		if(m_taskSlotItweenPlaying != null && m_taskSlotItweenPlaying.Running)
        {
            m_isLoop = false;
			m_taskSlotItweenPlaying.Kill();
        }

        if (m_animator != null)
        {
            m_animator.SetBool("isPlay", false);
        }

        iTween.Stop(this.gameObject , true);

        SetToinit();
    }

    #endregion public methods

    private void SetToinit ()
    {
        for (int i = 0; i < m_dictSlotItweens.Count; i++)
        {
            var itemValue = m_dictSlotItweens[i];

            for (int j = 0; j < itemValue.Count; j++)
            {
                SlotItween slotItween = itemValue[j];
                slotItween.SetToInitValue ();
            }
        }
    }

    private IEnumerator PlaySlotItweenCoroutine ()
    {
        while (m_isLoop)
        {
            D.Log ("this slot itween is played" + this.gameObject.name);

            for (int i = 0; i < m_dictSlotItweens.Count; i++)
            {
                var itemValue = m_dictSlotItweens[i];
                float duration = 0;

                for (int j = 0; j < itemValue.Count; j++)
                {
                    SlotItween slotItween = itemValue[j];
                    slotItween.PlayItween ();

                    if (slotItween.GetTime >= duration)
                    {
                        duration = slotItween.GetTime;
                    }
                }

                yield return new WaitForSeconds (duration);
            }
        }
    }
}