﻿using UnityEngine;
using System.Collections;

public class SlotItweenMoveTo : SlotItween 
{
    #region public override method
    public override void PlayItween ()
    {
        //SetInitValue ();
        MakeHashtable ();
        iTween.MoveTo (m_goTarget , m_hash);
    }

    #endregion
}
