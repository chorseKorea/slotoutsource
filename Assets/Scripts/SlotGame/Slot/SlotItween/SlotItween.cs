﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum e_ItweenParamKey
{
    x ,
    y ,
    z ,
    time ,
    speed ,
    delay ,
}

[System.Serializable]
public sealed class ItweenParamter
{
    public e_ItweenParamKey m_key;
    public string m_value;

    public ItweenParamter ( e_ItweenParamKey key , string value )
    {
        m_key = key;
        m_value = value;
    }
}

public abstract class SlotItween : MonoBehaviour
{
    #region fiedls

    [SerializeField]
    private Vector3 m_InitLocalScale;
    [SerializeField]
    private Vector3 m_InitLocalRot;
    [SerializeField]
    private Vector3 m_InitLocalPos;

    [SerializeField]
    protected int m_playOrder = 0;

    [SerializeField]
    protected GameObject m_goTarget;

    [SerializeField]
    protected List<ItweenParamter> m_listItweenParams = new List<ItweenParamter> ();

    [SerializeField]
    protected iTween.LoopType m_loopType = iTween.LoopType.none;

    [SerializeField]
    protected iTween.EaseType m_easeType = iTween.EaseType.linear;

    protected Hashtable m_hash = new Hashtable ();

    protected float m_time = 0;
    protected float m_delay = 0;

    #endregion fiedls

    #region public getter

    public float GetTime
    {
        get
        {
            return m_time;
        }
    }

    public float GetDelay
    {
        get
        {
            return m_delay;
        }
    }

    public int GetPlayOrder
    {
        get
        {
            return m_playOrder;
        }
    }

    #endregion 

    #region mono

    private void Awake()
    {
        SetInitValue();
    }

    #endregion

    #region protected method

    protected void SetInitValue()
    {
        m_InitLocalScale = m_goTarget.transform.localScale;
        m_InitLocalPos = m_goTarget.transform.localPosition;
        m_InitLocalRot = m_goTarget.transform.localEulerAngles;
    }

    protected void MakeHashtable ()
    {
        Hashtable hash = new Hashtable ();

        for (int i = 0; i < m_listItweenParams.Count; i++)
        {
            ItweenParamter param = m_listItweenParams[i];

            string key = param.m_key.ToString ();
            float floatValue = 0;

            if (float.TryParse (param.m_value , out floatValue))
            {
                hash.Add (key , floatValue);

                switch (param.m_key)
                {
                    case e_ItweenParamKey.time:

                        m_time = floatValue;

                        break;
                    case e_ItweenParamKey.delay:

                        m_delay = floatValue;

                        break;

                    default:
                        break;
                }
            }
            else
            {
                hash.Add (key , param.m_value);
            }
        }

        hash.Add ("looptype" , m_loopType);
        hash.Add ("islocal" , true);
        hash.Add ("easetype" , m_easeType);

        m_hash = hash;
    }

    #endregion protected method

    #region public method

    public void SetToInitValue()
    {
        m_goTarget.transform.localPosition = m_InitLocalPos;
        m_goTarget.transform.localEulerAngles = m_InitLocalRot;
        m_goTarget.transform.localScale = m_InitLocalScale;
    }
    #endregion 

    #region public abstract method

    public abstract void PlayItween ();

    #endregion public abstract method
}