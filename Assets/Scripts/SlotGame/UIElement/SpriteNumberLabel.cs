﻿using ChorseBase;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public enum e_SpriteNumbLabelAlign
{
    Left,
    Center,
    Right,
}

public class SpriteNumberLabel : MonoBehaviour
{
    #region fields

    private bool m_isDebug = false;

    [SerializeField]
    private string m_spritePrefix = string.Empty;

    [SerializeField]
    private string m_labelValue = string.Empty;

    [SerializeField]
    private int m_actualValue = 0;

    [SerializeField]
    private UISprite m_prefix;

    [SerializeField]
    private List<UISprite> m_listUiSprites = new List<UISprite>();

    [SerializeField]
    private e_SpriteNumbLabelAlign m_alignLabel = e_SpriteNumbLabelAlign.Left;

    [SerializeField]
    private float m_offsetHorGap = 0f; // offset for horizontal gap between label sprite

    private float m_horGap = 0;

    [SerializeField]
    private float m_offsetHorGapComma = 4f;

    [SerializeField]
    private Vector3 m_posInit = Vector3.zero;

    [SerializeField]
    private int m_idxAlign = 0;
    private int m_idxLabelEnd = 0;

    private Task m_taskTweenLabel = null;

    #endregion fields

    #region mono

    private void Awake()
    {
        if (m_isDebug)
        {
            ConvertNumberValueToStringValue();
            PlaceLetterToSpriteList();
        }
        
    }

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    #endregion mono

    #region public getter

    public int GetValue
    {
        get
        {
            return m_actualValue;
        }
    }

    #endregion public getter

    #region public methods

    public void SetValue(int value)
    {
        m_actualValue = value;
        ConvertNumberValueToStringValue();
        PlaceLetterToSpriteList();

        Vector3 posCurrent = this.gameObject.transform.localPosition;

        if(m_posInit == Vector3.zero)
        {
            if (posCurrent == Vector3.zero)
            {
                posCurrent.z = 0.1f;
            }

            m_posInit = posCurrent;
        }
    }

    public Task TweenLabelNumberTask(int targetNumber, float time)
    {
        if (m_taskTweenLabel != null && m_taskTweenLabel.Running)
        {
            m_taskTweenLabel.Kill();
        }

        m_taskTweenLabel = Task.Create(this, TweenLabelNumberCoroutine(targetNumber, time));

        return m_taskTweenLabel;
    }

    #endregion public methods

    #region private methods

    private IEnumerator TweenLabelNumberCoroutine(int targetNumber, float time)
    {
        float timeSpan = 0;
        int tweenNumber = this.GetValue;
        float timeDelta = 0;
        float step = 0;

        while (timeSpan <= time)
        {
            timeDelta = 1 * Time.deltaTime;
            step = timeSpan / time;
            tweenNumber = (int)(Mathf.SmoothStep(tweenNumber, targetNumber, step));
            this.SetValue(tweenNumber);
            yield return new WaitForSeconds(timeDelta);
            timeSpan += timeDelta;
        }
        this.SetValue(tweenNumber);
    }

    private void ConvertNumberValueToStringValue()
    {
        m_labelValue = m_actualValue.ToString("N", CultureInfo.InvariantCulture);
        m_labelValue = m_labelValue.Replace(".00", string.Empty);
    }

    private void PlaceLetterToSpriteList()
    {
        for (int i = 0; i < m_listUiSprites.Count; i++)
        {
            UISprite letterSprite = m_listUiSprites[i];
            string letter = string.Empty;
            int number = 0;

            int selectedIndex = (m_labelValue.Length - 1) - i;

            if (selectedIndex >= 0)
            {
                letterSprite.gameObject.SetActive(true);
                letter = m_labelValue[selectedIndex].ToString();

                if (int.TryParse(letter, out number))
                {
                    letterSprite.spriteName = string.Format("{0}{1:00}", m_spritePrefix, number);
                }
                else
                {
                    letterSprite.spriteName = string.Format("{0}comma", m_spritePrefix);
                }

                m_idxLabelEnd = i;
            }
            else
            {
                letterSprite.gameObject.SetActive(false);
            }
        }

        SetAlign();
    }

    private void SetAlign()
    {
        this.gameObject.transform.localPosition = m_posInit;
        int idxToAlign = 0;

        m_idxAlign = idxToAlign;

        for (int idx = 0; idx < m_listUiSprites.Count; idx++)
        {
            UISprite letterSprite = m_listUiSprites[idx];
            m_horGap = m_listUiSprites[idx].width;

            string letter = string.Empty;
            int number = 0;
            int idxPre = idx - 1;

            letterSprite.transform.name = idx.ToString();

            int idxLetter = (m_labelValue.Length - 1) - idx;
            int idxLetterPre = idxLetter + 1;

            if (idxLetter < m_labelValue.Length && idxLetter >= 0)
            {
                letter = m_labelValue[idxLetter].ToString();
                Vector3 posLetter = Vector3.zero;
                float xOffset = 0;

                if (idx == 0)
                {
                    posLetter = Vector3.zero;
                    posLetter.x += (idxToAlign * m_horGap);
                }
                else if ((idxPre) >= 0)
                {
                    xOffset = m_horGap;
                    posLetter = m_listUiSprites[idxPre].gameObject.transform.localPosition;

                    if (int.TryParse(letter, out number))
                    {
                        if ((idxLetterPre) < m_labelValue.Length)
                        {
                            letter = m_labelValue[idxLetterPre].ToString();

                            if (int.TryParse(letter, out number))
                            {
                                xOffset += m_offsetHorGap;
                            }
                            else
                            {
                                xOffset -= (m_offsetHorGapComma);
                            }
                        }
                    }
                    else
                    {
                        xOffset -= m_offsetHorGapComma;
                    }

                    posLetter.x -= xOffset;
                }

                letterSprite.transform.localPosition = posLetter;
            }
        }

        float lengthLabel = Mathf.Abs(m_listUiSprites[0].transform.localPosition.x - m_listUiSprites[m_idxLabelEnd].transform.localPosition.x);
        Vector3 posCurrent = Vector3.zero;

        switch (m_alignLabel)
        {
            case e_SpriteNumbLabelAlign.Center:

                posCurrent = this.gameObject.transform.localPosition;
                posCurrent.x += lengthLabel / 2;
                this.gameObject.transform.localPosition = posCurrent;
                break;

            case e_SpriteNumbLabelAlign.Left:
                posCurrent = this.gameObject.transform.localPosition;
                posCurrent.x += lengthLabel;
                this.gameObject.transform.localPosition = posCurrent;
                break;

            case e_SpriteNumbLabelAlign.Right:

                break;

            default:
                D.Error("wrong align value is assigned");
                break;
        }

        if(m_prefix != null)
        {
            Vector3 posPrefix = m_listUiSprites[m_idxLabelEnd].transform.localPosition;
            posPrefix.x -= (m_horGap + m_offsetHorGap);
            m_prefix.transform.localPosition = posPrefix;
        }
    }

    #endregion private methods
}