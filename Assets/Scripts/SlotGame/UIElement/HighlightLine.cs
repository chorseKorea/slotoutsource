﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum e_lineLayerType
{
    Rect = 220 ,
    Line = 200 ,
    NONE = 0 ,
}

public struct LinePath
{
    public Vector3[] m_pointsPath;
    public e_lineLayerType m_lineType;

    public LinePath(Vector3[] pointPath , e_lineLayerType lineType)
    {
        m_pointsPath = pointPath;
        m_lineType = lineType;
    }
}

public class HighlightLine : MonoBehaviour
{
    #region fields

    [SerializeField]
    private GameObject m_goLine;

    [SerializeField]
    private int m_numLineinstance = 0;

    [SerializeField]
    private List<List<GameObject>> m_dictGoLineInstance = new List<List<GameObject>> ();

    [SerializeField]
    private Dictionary<int, List<LinePath>> m_dictLinePathByLineIndex = new Dictionary<int, List<LinePath>>();


    [SerializeField]
    private List<Texture> m_listLineTextureByIndex = new List<Texture> ();

    private int m_renderOrder = 0;

    #endregion fields

    #region public

    public void SetPath ( int idxLine, Vector3[] path , e_lineLayerType lineType = e_lineLayerType.Line )
    {
        if (m_dictLinePathByLineIndex.ContainsKey (idxLine))
        {
            m_dictLinePathByLineIndex[idxLine].Add(new LinePath(path, lineType));
        }
        else
        {
            List<LinePath> listPath = new List<LinePath>();
            listPath.Add (new LinePath(path , lineType)); 
            m_dictLinePathByLineIndex.Add (idxLine , listPath);
    
        }
    }

    public Vector3[] GetPath ( int idxLine , int idxPath )
    {
        return m_dictLinePathByLineIndex[idxLine][idxPath].m_pointsPath;
    }

    public void SetRenderOrder ( int renderOrder = 0 )
    {
        m_renderOrder = renderOrder;

        m_goLine.SetActive (true);
        m_goLine.GetComponent<VecLine> ().GetMaterial.renderQueue = m_renderOrder;

        for (int i = 0; i < m_dictGoLineInstance.Count; i++)
        {
            List<GameObject> listGoLine = m_dictGoLineInstance[i];

            for (int j = 0; j < listGoLine.Count; j++)
            {
                GameObject goLine = listGoLine[j];
                goLine.SetActive (true);
                VecLine line = goLine.GetComponent<VecLine> ();
                line.GetMaterial.renderQueue = m_renderOrder;
            }
        }
    }

    public void ClearLines ()
    {
        UndrawAllLine ();
        m_dictLinePathByLineIndex.Clear ();
    }

    public void DrawSelectedLineIndex ( int idxLine )
    {
        m_dictGoLineInstance.Add (new List<GameObject> ());

        CreateLines (0 , m_dictLinePathByLineIndex[idxLine].Count);

        for (int j = 0; j < m_dictLinePathByLineIndex[idxLine].Count; j++)
        {
            GameObject goLine = m_dictGoLineInstance[0][j];
            goLine.transform.parent = this.transform;
            goLine.transform.localPosition = Vector3.zero;
            goLine.transform.localScale = Vector3.one;
            DrawLine (goLine , idxLine , j);
        }
    }

    public void DrawAllLine ()
    {
        UndrawAllLine ();

        for (int i = 0; i < m_dictLinePathByLineIndex.Count; i++)
        {
            int idxLine = m_dictLinePathByLineIndex.Keys.ToList ()[i];

            CreateLines (i , m_dictLinePathByLineIndex[idxLine].Count);

            for (int j = 0; j < m_dictLinePathByLineIndex[idxLine].Count; j++)
            {
                GameObject goLine = m_dictGoLineInstance[i][j];
                goLine.transform.parent = this.transform;
                goLine.transform.localPosition = Vector3.zero;
                goLine.transform.localScale = Vector3.one;
                DrawLine (goLine , idxLine , j);
            }
        }
    }

    #endregion public

    #region private methods

    private void DrawLine ( GameObject targetLine , int idxLine , int idxPath )
    {
        targetLine.SetActive (true);
        Vector3[] path = m_dictLinePathByLineIndex[idxLine][idxPath].m_pointsPath;
        VecLine vecLine = targetLine.GetComponent<VecLine> ();
        vecLine.UnDrawLine ();
        vecLine.GetMaterial.mainTexture = m_listLineTextureByIndex[idxLine];
        vecLine.GetMaterial.renderQueue += (int)(m_dictLinePathByLineIndex[idxLine][idxPath].m_lineType);
        vecLine.DrawLine (path);
    }

    private void CreateLines ( int idx , int count )
    {
        if(m_dictGoLineInstance.Count <= idx)
        {
            List<GameObject> listGoLine = new List<GameObject>();
            m_dictGoLineInstance.Add (listGoLine);
        }

        GameObject goLine = null;

        for (int j = (m_dictGoLineInstance[idx].Count -1 ); j < count; j++)
        {
            goLine = GameObject.Instantiate (m_goLine) as GameObject;
            goLine.transform.parent = this.transform;
            goLine.transform.localPosition = Vector3.zero;
            goLine.transform.localScale = Vector3.one;
            m_dictGoLineInstance[idx].Add (goLine);
        }

       m_numLineinstance += count;
    }


    private void UndrawAllLine ()
    {
        m_goLine.GetComponent<VecLine> ().UnDrawLine ();
        m_goLine.SetActive (false);
        m_numLineinstance = 0;

        for (int i = 0; i < m_dictGoLineInstance.Count; i++)
        {
            List<GameObject> listGoLine = m_dictGoLineInstance[i];

            for (int j = 0; j < listGoLine.Count; j++)
            {
                GameObject goLine = listGoLine[j];
                VecLine line = goLine.GetComponent<VecLine> ();
                line.UnDrawLine ();
                goLine.SetActive (false);
            }
        }

        //m_dictGoLineInstance.Clear ();
    }

    #endregion private methods
}