﻿using System;
using System.Collections;
using UnityEngine;
using ChorseBase;
using System.Collections.Generic;


public class CoinSplashHandler : MonoBehaviour
{
    #region fiedls
    [SerializeField]
    bool m_isPlay = true;

    [SerializeField]
    private GameObject m_prefabCoin;

    [SerializeField]
    Vector2 m_maxDirection = Vector2.zero; // possible direction 
    [SerializeField]
    Vector2 m_minDirection = Vector2.zero;

    [SerializeField]
    float m_amntForce = 40f; // force to apply on each of coin

    [SerializeField]
    float m_lifeTime = 0.5f;

    [SerializeField]
    private int m_numInstance = 0;

    [SerializeField]
    private float m_interval = 0.1f;

    [SerializeField]
    private List<GameObject> m_listCoinInst = new List<GameObject>();

    [SerializeField]
    private int m_sortingOrder = 0;
    
    #endregion fiedls

    #region mono

    private void OnDisable()
    {
        m_isPlay = false;

        for(int i = 0 ; i < m_listCoinInst.Count; i++)
        {
            m_listCoinInst[i].SetActive(false);
        }
    }

    #endregion

    #region public apid

    public void SetSortingOrder(int sortingOrder)
    {
         m_sortingOrder = sortingOrder;
    }

    public void StartPlayCoinSplah(int numInstance  = 0 , float interval = 0.1f)
    {
        m_isPlay = true;

        m_interval = interval;
        m_numInstance = numInstance;
       

        this.StartCoroutine(CoinSplahCoroutine());
    }

    #endregion

    #region private method

    private void CreateCoin()
    {
        for (int i = 0; i < m_numInstance; i++ )
        {
            GameObject goCoin = GameObject.Instantiate(m_prefabCoin);
            goCoin.SetActive(false);
            goCoin.transform.parent = this.transform;
            goCoin.transform.localPosition = Vector3.zero;
            goCoin.transform.localScale = Vector3.one * 0.01f;
            m_listCoinInst.Add(goCoin);
        }
    }

    private IEnumerator CoinSplahCoroutine()
    {
        WaitForSeconds interval = new WaitForSeconds(m_interval);
        while (m_isPlay)
        {
            if (m_listCoinInst.Count < m_numInstance)
            {
                CreateCoin();
            }

            yield return null;

            for (int i = 0; i < m_listCoinInst.Count; i++)
            {
                GameObject goCoin = null;
                CoinSplashItem coinItem = null;
                Vector2 direction = Vector2.zero;

                direction.x = UnityEngine.Random.Range(m_minDirection.x , m_maxDirection.x);
                direction.y = UnityEngine.Random.Range(m_minDirection.y , m_maxDirection.y);

                direction.Normalize();
                direction = direction * m_amntForce;

                goCoin = m_listCoinInst[i];

                if (goCoin.activeSelf)
                {
                    goCoin = null;
                }
                else
                {
                    goCoin.SetActive(true);
                }

                yield return null;

                if(goCoin != null)
                {
                    coinItem = goCoin.GetComponent<CoinSplashItem>();
                    coinItem.StartCoinSplash(m_lifeTime);

                    goCoin.transform.localPosition = Vector3.zero;
                    goCoin.transform.localScale = Vector3.one * 0.01f;
                    goCoin.GetComponent<Rigidbody2D>().AddForce(direction, ForceMode2D.Force);
                    goCoin.GetComponent<SpriteRenderer>().sortingOrder = m_sortingOrder + i;
                }

                yield return interval;
            }

            yield return null;
        }
    }

    #endregion
}