﻿using UnityEngine;
using System.Collections;

public class CoinSplashItem : MonoBehaviour
{
    #region fields

    [SerializeField]
    private Animator m_animator;

    [SerializeField]
    private float m_currTimeSpan;

    public float GetCurrentTimeSpan
    {
        get
        {
            return m_currTimeSpan;
        }
    }

    [SerializeField]
    private float m_targetTimeSpan;

    [SerializeField]
    private bool m_isAlive = false;

    public bool IsAlive
    {
        get
        {
            return m_isAlive;
        }
    }


    #endregion

    #region mono

    private void OnEnable()
    {
        m_animator.speed = 2.5f;
        
    }

    private void OnDisable()
    {
        this.StopAllCoroutines();
    }

    #endregion

    #region public api

    public void StartCoinSplash(float lifeTime)
    {
        m_targetTimeSpan = lifeTime;
        m_currTimeSpan = 0;
        m_isAlive = true;
        this.StartCoroutine(CheckTimeSpan());
    }

    #endregion

    #region Coroutine

    private IEnumerator CheckTimeSpan()
    {
        float timeDelta = 1 * Time.deltaTime;

        while(m_currTimeSpan <= m_targetTimeSpan)
        {
            yield return new WaitForSeconds(timeDelta);
            timeDelta = 1 * Time.deltaTime;
            m_currTimeSpan += timeDelta;

            this.gameObject.transform.localScale = Vector3.one * Mathf.Clamp01((m_currTimeSpan / m_targetTimeSpan) * 2f);
        }

        m_isAlive = false;

        this.gameObject.SetActive(false);
        yield return null;
    }

    #endregion


}
