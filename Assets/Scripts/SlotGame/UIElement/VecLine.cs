﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using Vectrosity;

public class VecLine : MonoBehaviour
{
    #region fields

    [SerializeField]
    private Material m_material;

    private Material m_matInst;

    [SerializeField]
    private float m_pixelWidth = 0;

    private float m_lineWidth = 0.005f;

    VectorLine m_lineInst;

    [SerializeField]
    Vector3[] m_linePoints;

    #endregion

    #region getter

    public Material GetMaterial
    {
        get
        {
            if(m_matInst == null)
            {
                m_matInst = new Material (m_material);
            }

            return m_matInst;
        }
    }

    #endregion

    #region Mono

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
        UnDrawLine ();
    }

    private void OnDrawGizmos ()
    {
        Gizmos.color = Color.green;
        for (int i = 0; i < m_linePoints.Length - 1; ++i)
        {
            Gizmos.DrawLine (gameObject.transform.TransformPoint (m_linePoints[i]) , gameObject.transform.TransformPoint (m_linePoints[i + 1]));
        }
    }

    #endregion

    #region public methods

    public void DrawLine(Vector3[] linePoints)
    {
        UnDrawLine();
        float screenWidth = Camera.main.pixelWidth;
        float actualWidth = m_lineWidth * screenWidth;
        m_pixelWidth = actualWidth;

        m_linePoints = linePoints;
        m_lineInst = new VectorLine ("line" , m_linePoints , m_matInst , actualWidth , LineType.Continuous , Joins.Weld);
        m_lineInst.continuousTexture = true;
        m_lineInst.drawTransform = this.transform;
        m_lineInst.Draw3DAuto();
    }

    public void UnDrawLine()
    {
        VectorLine.Destroy (ref m_lineInst);
    }

    #endregion

}
