﻿using UnityEngine;
using System.Collections;

public class BtnOverlay : MonoBehaviour
{
    #region fields

    [SerializeField]
    private UITexture m_textureOverlay;
    [SerializeField]
    private UIButton m_thisBtn;

    #endregion

    private void OnEnable()
    {
        if (m_textureOverlay == null)
        {
            m_textureOverlay = this.GetComponentInChildren<UITexture> ();
        }
        if (m_thisBtn == null)
        {
            m_thisBtn = this.GetComponentInChildren<UIButton>();
        }

        if (m_textureOverlay != null)
            m_textureOverlay.enabled = false;
    }

    private void OnDisable()
    {
        if (m_textureOverlay == null)
        {
            m_textureOverlay = this.GetComponentInChildren<UITexture> ();
        }
        if (m_thisBtn == null)
        {
            m_thisBtn = this.GetComponentInChildren<UIButton>();
        }

        if (m_textureOverlay != null)
            m_textureOverlay.enabled = true;
    }

    #region public API

    public void TurnOnOverlay()
    {
        if(m_thisBtn.isEnabled)
        {
            m_textureOverlay.enabled = true;
        }
        else
        {
            m_thisBtn.SetState(UIButtonColor.State.Disabled, true);
        }
    }
    public void TurnOffOveerlay()
    {
        if (m_thisBtn.isEnabled)
        {
            m_textureOverlay.enabled = false;
        }
        else
        {
            m_thisBtn.SetState(UIButtonColor.State.Normal, true);
        }
    }

    #endregion

}
