﻿using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour
{
    #region fields

    [SerializeField]
    private string m_nameSound = string.Empty;
    [SerializeField]
    private bool m_loop = false;
    [SerializeField]
    private bool m_isAutoPlay = false;

    #endregion fields

    #region mono

    private void OnEnable()
    {
        if(m_isAutoPlay)
        {
            Play();
        }
    }

    private void OnDisable()
    {
        //Stop();
    }

    #endregion mono

    #region public

    public void Play()
    {
        if (m_nameSound != string.Empty)
        {
            if (m_loop)
            {
                GameSoundMgr._Inst.PlayLoopSound(m_nameSound);
            }
            else
            {
                GameSoundMgr._Inst.PlayOneSound(m_nameSound);
            }
        }

    }

    public void Stop()
    {
        if (m_nameSound != string.Empty)
        {
            if (m_loop)
            {
                GameSoundMgr._Inst.StopPlaySound(m_nameSound);
            }
            else
            {
                GameSoundMgr._Inst.StopPlaySound(m_nameSound);
            }
        }
    }

    #endregion public
}