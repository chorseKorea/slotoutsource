﻿using UnityEngine;
using System.Collections;

// 게임 시작전까지의 절차
public enum eMAINFLOW : byte
{
    Init = 0,
    Check_Url,			// 서버 상태 & Url 확인 (성공/실패 받음)
    Check_GameVer,		// 게임버전 확인(네트워크 접속 주소/포트 받음)
    Connect_GW,			// 네트워크 생성 & 게이트웨이 접속 (번들 경로 받음)
    Receive_Bundle,		// 번들 다운로드
    Complete_Bundle,	// 번들을 다 받고 게임 진행이 가능한 상황
    END,				// 모든 사전 처리가 완료되고 대기중인 상태
}

// 게임 씬
public enum eSCENE : byte
{
    LOGIN = 0,
    LOBBY,
    GAME,
    SLOT,
    SLOT_LOBBY,
    END
}




public class Main : MonoBehaviour {

    static private Main m_Instance;
    static public Main _Inst { get { return m_Instance; } }

    private IScene[] m_Scene = null;			// 게임씬

    public bool m_bReleaseMode = false;			// true:배포버전 모드, false:테스트 모드
    public bool m_bDebugMode = false;			// true:디버그 On, false:디버그 Off
    public eSERVICE_TARGET m_eServiceTarget = eSERVICE_TARGET.GOOGLE;
    public uint m_uiVersionRelease = 1;			// 배포시 게임(서버) 버전
    
    //public eGAMEKIND m_eGameKind = eGAMEKIND.CARD;
    public eGAMESTATE m_eCurrState = eGAMESTATE.LOAD;

    string tempPath;


    // 현재씬 처리
    private IScene m_curGameScene;				// 현재 씬의 게임오브젝트
    public IScene _CUR_GAME_SCENE { get { return m_curGameScene; } }
    private eSCENE m_eCurScene = eSCENE.LOGIN;	// 현재 씬의 유형(enum 정의 값)
    public eSCENE _CUR_SCENE
    {
        get { return m_eCurScene; }
        set
        {
            // 씬 전환시 처리는 여기서 처리
            if (m_eCurScene != value)
            {
                DestroyScreen();
                m_eCurScene = value;
                Create_Scene();
            }
        }
    }


    // 현재 게임 처리 순서
    private eMAINFLOW m_eFlow = eMAINFLOW.Init;
    public eMAINFLOW _CUR_FLOW
    {
        get { return m_eFlow; }
        set
        {
            if (m_eFlow != value)
            {
                m_eFlow = value;
            }
        }
    }



    // 현재 매니저들이 Awake 에서 인스턴스가 생성되므로 Start에서 매니저관련 처리를 수행한다.
    void Awake() { m_Instance = this; }

	// Use this for initialization
	void Start () {
        // 공통 처리
        Init();

        m_Scene = new IScene[(int)eSCENE.END];
        Create_Scene();
	}

	
    // 초기화
    void Init()
    {
       
        SetDebugConfig();
       
        // 4.매니저 클래스 처리
        //iLog.m_bUseLog = true;
        UI_Script_Mng._Inst.Create(gameObject);

        
        if(GameInfo.m_ePlatformName == ePLATFORM.WEB)
        {
            GameInfo.PlatFormName = "Web";
        }
        else 
        {
            GameInfo.PlatFormName = "Mobile";
        }

        if (GameInfo.m_eCurrGameKind <= eGAMEKIND.CARD)
        {
            GameInfo.GameKindDirectory = "CardGame";
        }
        else
        {
            GameInfo.GameKindDirectory = "SlotGame";
        }
        
    }




    // 디버깅 On/Off 설정
    void SetDebugConfig()
    {
        PacketMng._Inst.m_bLog = m_bDebugMode;
        //NetworkMng._Inst.m_bLog = m_bDebugMode;
        //iLog.m_bUseLog = m_bDebugMode;

        //if (m_bDebugMode) gameObject.AddComponent<iDebug>();
    }

    

    void OnGUI()
    {
        

        if (_CUR_FLOW == eMAINFLOW.Receive_Bundle)
            GUI.Label(new Rect(10, 100, 800, 30), "AssetBundle Download Start... : " + tempPath);
        else
            GUI.Label(new Rect(10, 100, 800, 30), "Main");
    }

    

    void Create_Scene()
    {
        int iScene = (int)m_eCurScene;

        switch (m_eCurScene)
        {
            case eSCENE.LOGIN:
                m_Scene[iScene] = new GameObject().AddComponent<Scene_Login>();
                break;
            case eSCENE.LOBBY:
                m_Scene[iScene] = new GameObject ().AddComponent<Scene_Lobby> ();
                break;
            case eSCENE.GAME:
                //m_Scene[iScene] = new GameObject().AddComponent<Scene_Game>();
                break;
            case eSCENE.SLOT:
                m_Scene[iScene] = new GameObject ().AddComponent<Scene_Slot> ();
                break;
            case eSCENE.SLOT_LOBBY:
                m_Scene[iScene] = new GameObject ().AddComponent<Scene_SlotLobby> ();
                break;

            default:
                break;
        }

        m_Scene[iScene].SetParent(gameObject);
        m_curGameScene = m_Scene[iScene];
    }

    void DestroyScreen()
    {
        if (m_Scene == null) return;

        for (int i = 0; i < m_Scene.Length; ++i)
        {
            if (m_Scene[i] != null)
            {
                DestroyImmediate(m_Scene[i].gameObject);
                m_Scene[i] = null;
                m_curGameScene = null;
                System.GC.Collect();
            }
        }
    }

}
