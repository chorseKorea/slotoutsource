﻿using UnityEngine;
using System.Collections;

public class Scene_SlotLobby : IScene 
{
    void Start ()
    {
        CreateGame ();
    }

    void CreateGame ()
    {
        UI_Script_Mng._Inst.GetUI (eUI_TYPE.Lobby_UI);
        UI_Script_Mng._Inst.GetUI (eUI_TYPE.CommonPanel_UI);
        UI_Script_Mng._Inst.GetUI (eUI_TYPE.Slot_Lobby);
    }

    void Destroy ()
    {
        UI_Script_Mng._Inst.HideUI (eUI_TYPE.Slot_Lobby);
    }
    void OnDestroy ()
    {
        Destroy ();
    }
}
