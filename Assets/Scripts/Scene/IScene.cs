﻿using UnityEngine;
using System.Collections;

public enum eGAME_SCENE
{
    LOGIN = 0,
    GAME1,
    RECONNECT,
    END
}

public class IScene : MonoBehaviour {

    public void SetParent(GameObject parent)
    {
        gameObject.name = this.GetType().ToString();
        gameObject.transform.parent = parent.transform;
    }

    public void Reset() { }

    public void SetActive(bool act)
    {
        gameObject.SetActive(act);
    }

    public bool IsActive()
    {
        return gameObject.activeSelf;
    }

    virtual public void UpdateData() { }
    virtual public void UpdateOneSec() { }
    virtual public void MouseEvent() { }

    //virtual public void OnDestroy()
    //{
    //	Destroy(gameObject);
    //}


    // 개별씬관련 외부 참조 함수
    virtual public bool _BuyObj { get { return false; } set { } }
    virtual public void CBProgress(float CBProgress) { }
}
