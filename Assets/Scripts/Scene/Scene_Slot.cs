﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using EDNParser;
public class Scene_Slot : IScene
{
 
    #region private method
    void Start ()
    {
        Debug.Log ("Create Scene slot");
        CreateGame ();
    }

    void CreateGame ()
    {
        UI_Script_Mng._Inst.GetUI (eUI_TYPE.SlotMachine_Base);
    }

    void Destroy ()
    {
    }
    void OnDestroy ()
    {
        Destroy ();
    }

    #endregion
   
}
