﻿using UnityEngine;
using System.Collections;
using EDNParser;

public class Scene_Login : IScene
{
    UIRoot m_root;

    void Start()
    {
        //iLog.Log(eLOG_OUTPUT.iDEBUGnCON, "Create Scene_Login");
        Debug.Log("Create Scene_Login");
        CreateGame();
    }

    void CreateGame()
    {
        //UI 생성하고 
        //그거 플레이 한다음에
        m_root = new GameObject("Root_Login").AddComponent<UIRoot>();
        m_root.transform.parent = gameObject.transform;
        m_root.scalingStyle = UIRoot.Scaling.ConstrainedOnMobiles;
        m_root.minimumHeight = 480;
        m_root.manualHeight = 720;
        m_root.transform.localPosition = new Vector3(-1000f, 0f, 0f);
        m_root.transform.localScale = Vector3.one;

        NetworkMng._Inst.StartClient ();
    }

    void Destroy()
    {
        if (m_root != null)
        {
            Destroy(m_root.gameObject);
            m_root = null;
        }
    }
    void OnDestroy() { Destroy(); }


    void OnGUI()
    {
        //iLog.Log(eLOG_OUTPUT.iDEBUGnCON, "Platform : " + Application.platform + " m_strBasePath : " + m_strBasePath);
        GUI.Label(new Rect(10, 10, 500, 30), "Platform : " + Application.platform);
        //GUI.Label(new Rect(10, 40, 500, 30), "PATH : " + DownloadData_Mng._Inst.m_strBasePath);
    }



    UIPanel m_pnDebug;


    bool t_bConnectNet = false;
    public void ReceiveDebugEvent(object argv)
    {
        if (t_bConnectNet) return;

        Debug.Log(argv);
        //DatUser._Inst.m_strID = argv.ToString();
        //DatUser._Inst.m_nMyOffset = -1;
        //DatUser._Inst.m_nMySeatIdx = -1;
        //DatUser._Inst.m_uiTotalMoney = 0;

        t_bConnectNet = true; // 다른 씬 클릭 막는 처리

        if (Main._Inst.m_eCurrState == eGAMESTATE.CONNECT)
        {
            PacketMng._Inst.SEND_LOGIN_REQ("akoz1", "pw");
        }

        UI_Script_Mng._Inst.CloseAllUI();

        Main._Inst._CUR_SCENE = eSCENE.LOBBY; //eSCENE.GAME;


        // 임시 테스트
        //GameSoundMgr._Inst.PlayOneSound("Check");
    }

    #region public override method

    

    #endregion

    #region private methods

    

    #endregion

    #region public Interface

    public void LoginToServer()
    {
        if (Main._Inst.m_eCurrState == eGAMESTATE.CONNECT)
        {
            PacketMng._Inst.SEND_LOGIN_REQ (GamePlayer._Inst._ID , GamePlayer._Inst._PW);
        }

        UI_Script_Mng._Inst.CloseAllUI ();

    }
    public void EnterLobby ()
    {
        Main._Inst._CUR_SCENE = eSCENE.LOBBY; //eSCENE.GAME;
    }

    #endregion
}
