﻿using UnityEngine;
using System.Collections;

public class Scene_Lobby : IScene 
{
    UIRoot m_root;

    void Start ()
    {
        CreateGame ();
    }

    void CreateGame ()
    {
        UI_Script_Mng._Inst.GetUI (eUI_TYPE.Lobby_UI);
        UI_Script_Mng._Inst.GetUI (eUI_TYPE.CommonPanel_UI);
    }

    void Destroy ()
    {
        UI_Script_Mng._Inst.CloseAllUI ();

        if (m_root != null)
        {
            Destroy (m_root.gameObject);
            m_root = null;
        }
    }
    void OnDestroy ()
    {
        Destroy ();
    }

    UIPanel m_pnDebug;
    bool t_bConnectNet = false;
    public void ReceiveDebugEvent ( object argv )
    {
        if (t_bConnectNet)
            return;
    }
}
