﻿using UnityEngine;
using System.Collections;

#region Enum System

// 서비스 마켓
public enum eSERVICE_TARGET : byte
{
    SKT = 0,
    KT,
    LG,
    GOOGLE,
    IOS,
    WEBGL,
}

public enum eFLURRY_OS : byte
{
    DEFAULT = 0,
    ANDROID = 1,		// 구글 플레이
    ANDROID_SK = 2,
    ANDROID_LG = 3,
    ANDROID_KT = 4,
    IPHONE = 5,
    WEBGL = 6,
}

// 언어 - 국가별 약어로 변경 : Alpha-3 사용
public enum eLANGUAGE : byte
{
    usa = 0,		// 미국(다른 영어권 국가 포함)
    kor = 1,		// 한국
    jpn = 2,		// 일본
    chn = 3,		// 중국 - 간체 cn
    twn = 4,		// 타이완(대만) - 번체 tw
    esp = 5,		// 스페인
    deu = 6,		// 독일
    fra = 7,		// 프랑스
    rus = 8,		// 러시아
    prt = 9,		// 포르투갈
    vnm = 10,		// 베트남
    mys = 11,		// 말레이시아
    tha = 12,		// 타이(태국)
    idn = 13,		// 인도네시아
    END
}

public enum eGAMESTATE
{
    LOAD,
    CONNECT,
    LOGIN,
    TABLELIST_SCENE,
    GAME_SCENE,
};

public enum eGAMEKIND
{
    TEXAS_HOLDEM,
    SPEED_HOLDEM,
    OMAHA_HOLDEM,
    SHOOTOUT,
    SIT_N_GO, 
    CARD,

    SLOT,

    END
}

public enum ePLATFORM : byte
{
    WEB = 0,
    MOBILE,
    END
}
#endregion

