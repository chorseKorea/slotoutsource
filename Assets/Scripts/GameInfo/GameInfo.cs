﻿using UnityEngine;
//using System.Collections;



public class GameInfo
{
    // 필수 확인 변수(Main에서 수정)
    static public bool s_bReleaseMode = false;									// true:실버전 (수정 필수)
    static public eSERVICE_TARGET s_eServiceTarget = eSERVICE_TARGET.GOOGLE;	// 서비스 앱 (수정 필수)
    static public uint s_uiVersionRelease = 1;									// 게임 빌드 버전 (수정 필수)

    // 출시 마켓관련 변수
    // 0 = 디폴트, 1 = 안드로이드, 2 = 안드로이드 SK, 3 = 안드로이드 LG, 4 = 안드로이드 KT, 5 = 아이폰
    static public byte s_nDeviceKind = 1;							// 마켓 인덱스 (aMain 에서 m_eServiceTarget 을 참조해 자동 변경)
    static public eFLURRY_OS s_eFLURRY_OS = eFLURRY_OS.ANDROID;		// 통계 OS(aMain에서 m_eServiceTarget 을 참조해 자동 변경)

    // 환경설정 변수
    static public eLANGUAGE s_eLanguage = eLANGUAGE.kor;	// 사용중인 언어
    static public float s_fVolBGM = 1.0f;					// 배경음 볼륨(BGM On/Off로 사용중)
    static public float s_fVolEff = 1.0f;					// 효과음 볼륨(효과음 On/Off로 사용중)
    static public int s_iPushMsg = 1;						// Push 메시지 (0:Off, 1:On)

    // 번들 관련 변수
    //static public int[] s_iVersionBundle = new int[(int)eASSETBUNDLE_TYPE.END];		// 각 번들별 버전
    //static public int s_iVersionGame = 0;				// 게임 버전
    //static public int s_iVersionUI = 0;					// UI 버전
    //static public int s_iVersionXml = 0;				// Xml 버전
    //static public int s_iVersionSound = 0;				// 사운드 버전

    static public byte s_byTotalBundleCount = 3;		// 받아야 할 총 번들 수
    static public byte s_byReceivedBundleCount = 0;		// 받은 번들 수(받아야 할 번들보다 작다면 Cash Clear 처리)

    static public ePLATFORM m_ePlatformName = ePLATFORM.WEB;
    static private string m_strPlatformName = "";

    static public eGAMEKIND m_eCurrGameKind = eGAMEKIND.SLOT;
    static private string m_strGameKindDirectory = "";

    static public string GameKindDirectory
    {
        get { return m_strGameKindDirectory; }
        set { m_strGameKindDirectory = value; }
    }

    static public string PlatFormName
    {
        get { return m_strPlatformName; }
        set { m_strPlatformName = value; }
    }

    //배경 사운드
    static public bool isBgSound
    {
        get
        {
            if (_BgSound <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    static public bool isFxSound
    {
        get
        {
            if (_FxSound <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    //배경사운드
    static public float _BgSound
    {
        get
        {
            return GameInfo.s_fVolBGM;
        }
        set
        {
            PlayerPrefs.SetFloat("s_fVolBGM", value);
            GameInfo.s_fVolBGM = value;
        }

    }
    //효과 사운드
    static public float _FxSound
    {
        get
        {
            return GameInfo.s_fVolEff;
        }
        set
        {
            PlayerPrefs.SetFloat("s_fVolEff", value);
            GameInfo.s_fVolEff = value;
        }
    }

    //언어 변환
    static public eLANGUAGE _Language
    {
        get
        {
            return GameInfo.s_eLanguage;
        }
        set
        {
            PlayerPrefs.SetInt("s_eLanguage", (int)value);
            GameInfo.s_eLanguage = value;
        }
    }
}
