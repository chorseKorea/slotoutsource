﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ChorseBase
{
	public class Service : MonoBehaviour 
	{

		#region Singleton
		static Service 		s_instance 		= null;
		static GameObject	s_gameObject	= null;
		
		static void MakeAlive()
		{
			if(s_instance == null)
			{
				s_instance = FindObjectOfType(typeof(Service)) as Service;
				
				if(s_instance == null)
				{
					s_gameObject = new GameObject("Service");
					s_instance = s_gameObject.AddComponent<Service>();
				}
			}
		}
		#endregion
		
		#region Fields
		static Dictionary<System.Type, object>  s_serviceMap = new Dictionary<System.Type, object>();
		#endregion
		
		#region Mono Behaviour
		void Awake()
		{
			DontDestroyOnLoad(this);
		}
		
		void OnApplicationPause(bool paused)
		{
			if(paused)
			{
				D.SaveLog();
			}
		}
		
		void OnApplicationQuit()
		{
			D.SaveLog();
			s_serviceMap.Clear();
			Destroy(s_gameObject);
		}
		
		void Update()
		{
			if(RuntimePlatform.Android == Application.platform)
			{
				if(Input.GetKeyDown(KeyCode.Escape))
				{
					GameObject.Find("ADUtilServiceManager").SendMessage("OnAndroidBackClicked");
				}
				else if(Input.GetKeyDown(KeyCode.Menu))
				{
					GameObject.Find("ADUtilServiceManager").SendMessage("OnAndroidMenuClicked");
				}
			}
		}
		#endregion
		
		#region Public API
		public static void Init()
		{
			// 	Because singletons are all lazy created. if you need it to be alive the monment the game start.
			//  Please put it here.
			MakeAlive();

			Application.targetFrameRate = 30;
		}
		
		public static T Get<T>() where T : Singleton
		{
			MakeAlive();
			
			Add<T>();
			
			return s_serviceMap[typeof(T)] as T;
		}
		
		public static void Remove<T>() where T : Singleton
		{
			MakeAlive();
			
			if(s_serviceMap.ContainsKey(typeof(T)))
			{
				var singletonClass = s_serviceMap[typeof(T)];
				Destroy(singletonClass as T);
				s_serviceMap.Remove(typeof(T));
			}
		}
		
		public static void Add<T>() where T : Singleton
		{
			MakeAlive();
			
			if(!s_serviceMap.ContainsKey(typeof(T)))
			{
				T singletonClass = s_gameObject.AddComponent<T>();
				s_serviceMap.Add(typeof(T), singletonClass);
			}
		}
		#endregion
	}
}

