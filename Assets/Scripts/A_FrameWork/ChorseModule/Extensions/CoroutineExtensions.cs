using UnityEngine;
using System.Collections;
using ChorseBase;

namespace ChorseExtensions
{
	public static class CoroutineExtensions
	{
		// Start a task from this mono class.
		// The task will not be run anymore if the mono class is inactive/destroyed.
		public static Task StartTask(this MonoBehaviour monoClass, IEnumerator coroutine)
		{
			var t = Task.Create(monoClass, coroutine, true);	
			return t;
		}
		
		public static Task StartGlobalTask(this MonoBehaviour monoClass, IEnumerator coroutine)
		{
			var t= Task.Create(null, coroutine, true);
			return t;
		}
	}
}
