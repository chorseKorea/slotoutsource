using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChorseExtensions;

namespace ChorseBase
{
    // This is a proxy object so we can launch coroutines.
    public class TaskManager : Singleton
    {
    }

    public class Task : IDisposable
    {
        #region Event

        public event System.Action TaskStarted;

        public event System.Action<bool> TaskCompleted;

        #endregion Event

        #region Fields

        private Coroutine m_untilDone;
        private Stack<Task> m_subTaskStack;

        #endregion Fields

        #region Protected Fields

        protected bool m_running;
        protected bool m_paused;
        protected bool m_wasKilled;
        protected MonoBehaviour m_monoClass;
        protected IEnumerator m_coroutine;

        #endregion Protected Fields

        #region Properties

        public bool Running { get { return m_running; } }

        public bool Paused { get { return m_paused; } }

        public bool Killed { get { return m_wasKilled; } }

        public virtual Coroutine UntilDone
        {
            get
            {
                return m_monoClass.StartCoroutine(StillRunning());
            }
        }

        #endregion Properties

        #region Constructors

        public Task(IEnumerator coroutine)
            : this(null, coroutine, true)
        { }

        public Task(IEnumerator coroutine, bool immediatelyStart)
            : this(null, coroutine, immediatelyStart)
        {
        }

        public Task(MonoBehaviour monoClass)
        {
            if (monoClass == null)
            {
                m_monoClass = Service.Get<TaskManager>();
            }
            else
            {
                m_monoClass = monoClass;
            }
        }

        public Task(MonoBehaviour monoClass, IEnumerator coroutine, bool immediatelyStart)
        {
            if (monoClass == null)
            {
                m_monoClass = Service.Get<TaskManager>();
            }
            else
            {
                m_monoClass = monoClass;
            }
            m_coroutine = coroutine;
            if (immediatelyStart)
            {
                Start();
            }
        }

        #endregion Constructors

        #region dispose

        public void Dispose ()
        {
            GC.SuppressFinalize (this);
        }

        #endregion

        #region Statis Task Creators

        public static Task Create(IEnumerator coroutine)
        {
            return new Task(coroutine);
        }

        public static Task Create(IEnumerator coroutine, bool immediatelyStart)
        {
            return new Task(coroutine, immediatelyStart);
        }

        public static Task Create(MonoBehaviour monoClass, IEnumerator coroutine, bool immediatelyStart = true)
        {
            return new Task(monoClass, coroutine, immediatelyStart);
        }

        #endregion Statis Task Creators

        #region Interface

        public virtual void Start()
        {
            m_running = true;

            m_monoClass.StartCoroutine(DoTask());

            //D.Log("Task " + m_coroutine.ToString() +  " starts from " + m_monoClass.name);

            if (TaskStarted != null)
            {
                TaskStarted.InvokeEvent();
            }
        }

        public virtual void Pause()
        {
            m_paused = true;

			if (m_subTaskStack == null || m_subTaskStack.Count <= 0) 
			{
				return;
			}

            Queue<Task> tempQueSubTask = new Queue<Task>();

            while (m_subTaskStack != null)
            {
                Task subTask = m_subTaskStack.Pop();
                subTask.Pause();

                tempQueSubTask.Enqueue(subTask);
            }
            
            while(tempQueSubTask.Peek() != null)
            {
                m_subTaskStack.Push(tempQueSubTask.Dequeue());
            }
        }

        public virtual void UnPause()
        {
            m_paused = false;

			if (m_subTaskStack == null || m_subTaskStack.Count <= 0) 
			{
				return;
			}

            Queue<Task> tempQueSubTask = new Queue<Task>();

            while (m_subTaskStack.Peek() != null)
            {
                Task subTask = m_subTaskStack.Pop();
                subTask.UnPause();

                tempQueSubTask.Enqueue(subTask);
            }

            while (tempQueSubTask.Peek() != null)
            {
                m_subTaskStack.Push(tempQueSubTask.Dequeue());
            }
        }

        public virtual void Kill()
        {
            if (m_subTaskStack != null && m_subTaskStack.Count > 0) 
            {
                while (m_subTaskStack.Count > 0 && m_subTaskStack.Peek () != null)
                {
                    m_subTaskStack.Pop ().Kill ();
                }
            }

            m_wasKilled = true;
            m_running = false;
            m_paused = false;

            Dispose ();
        }

        public virtual void Kill(float delayInSeconds)
        {
            var delay = (int)(delayInSeconds * 1000);
            new System.Threading.Timer(obj =>
                                       {
                                           lock (this)
                                           {
                                               Kill();
                                           }
                                       }, null, delay, System.Threading.Timeout.Infinite);
        }

        protected virtual IEnumerator DoTask()
        {
            // null out the first run through is case we start paused
            yield return null;


            while (m_running)
            {
                if (m_paused)
                {
                    yield return null;
                }
                else
                {
                    bool haveNext = false;

                    if (m_coroutine != null)
                    {
                        // Add exception error handling here..
                        try
                        {
                            haveNext = m_coroutine.MoveNext();
                        }
                        catch (System.Exception e)
                        {
                            D.Exception(e);
                        }
                    }

                    if (haveNext)
                    {
                        yield return m_coroutine.Current;
                    }
                    else
                    {
                        if (m_subTaskStack != null)
                        {
                            yield return m_monoClass.StartCoroutine(RunSubTasks());
                        }
                        m_running = false;
                    }
                }
            }

            //D.Log("Task " + m_coroutine.ToString() + " stopped by " + m_monoClass.name);

            if (TaskCompleted != null)
            {
                TaskCompleted.InvokeEvent(m_wasKilled);
            }
            else
                Kill();
        }

        public virtual IEnumerator StillRunning()
        {
            while (m_running)
            {
                yield return null;
            }
        }

        #endregion Interface

        #region Public API

        public Task CreateAndAddSubTask(IEnumerator coroutine)
        {
            var t = new Task(coroutine, false);
            AddSubTask(t);
            return t;
        }

        public void AddSubTask(Task subTask)
        {
            if (m_subTaskStack == null)
            {
                m_subTaskStack = new Stack<Task>();
            }
            m_subTaskStack.Push(subTask);
        }

        public void RemoveSubTask(Task subTask)
        {
            if (m_subTaskStack.Contains(subTask))
            {
                var subTaskStack = new Stack<Task>(m_subTaskStack.Count - 1);
                var allCurrentTask = m_subTaskStack.ToArray();
                System.Array.Reverse(allCurrentTask);

                for (int i = 0; i < allCurrentTask.Length; ++i)
                {
                    var t = allCurrentTask[i];
                    if (t != subTask)
                    {
                        subTaskStack.Push(t);
                    }
                }

                m_subTaskStack = subTaskStack;
            }
        }

        public IEnumerator StartAsCoroutine()
        {
            m_running = true;

            yield return m_monoClass.StartCoroutine(DoTask());
        }

        #endregion Public API

        #region Internal Implementation

        private IEnumerator RunSubTasks()
        {
            if (m_subTaskStack != null && m_subTaskStack.Count > 0)
            {
                while (m_subTaskStack.Count > 0)
                {
                    Task subTask = m_subTaskStack.Pop();

                    if (subTask != null)
                        yield return m_monoClass.StartCoroutine (subTask.StartAsCoroutine ());
                    else
                        yield return null;
                }
            }
        }

        #endregion Internal Implementation
    }
}