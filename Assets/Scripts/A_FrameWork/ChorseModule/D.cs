﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public enum LogLevel
{
    Nothing = 0x0000,
    Error = 0x0001,
    Exception = 0x0011,
    Warning = 0x0111,
    Info = 0x1111,
}

public static class D
{
    private static bool s_enabled;
    private static LogLevel s_logLevel = LogLevel.Info;

    private static List<string> m_logsToFile = new List<string>();

    static D()
    {
#if UNITY_EDITOR
        s_enabled = true;
#else
		s_enabled = false;
#endif
    }

    public static void SaveLog()
    {
#if !UNITY_WEBPLAYER
        Directory.CreateDirectory(Application.dataPath);

        // We only cache 2 days log..

        string filePath = Application.dataPath + "Log.txt";

        FileInfo file = new FileInfo(filePath);
        var span = System.DateTime.UtcNow - file.CreationTimeUtc;

        // if the file is already created more than 2 days, clear it..
        if (span.Days > 2)
        {
            File.Delete(filePath);
            var writer = File.Create(filePath);
            writer.Close();
        }

        using (var writer = File.AppendText(filePath))
        {
            for (int i = 0; i < m_logsToFile.Count; ++i)
            {
                writer.WriteLine(m_logsToFile[i]);
            }
        }

        // After writing, we need to clear it....
        m_logsToFile.Clear();
#endif
    }

    public static void Log(object message, bool saveToFile = false)
    {
        if (s_enabled && (s_logLevel & LogLevel.Info) == LogLevel.Info)
        {
            Debug.Log(message);
        }

        if (saveToFile)
        {
            m_logsToFile.Add(message.ToString());
        }
    }

    public static void Warn(object message, bool saveToFile = false)
    {
        if (s_enabled && (s_logLevel & LogLevel.Warning) == LogLevel.Warning)
        {
            Debug.LogWarning(message);
        }

        if (saveToFile)
        {
            m_logsToFile.Add(message.ToString());
        }
    }

    public static void Error(object message, bool saveToFile = false)
    {
        if (s_enabled && (s_logLevel & LogLevel.Error) == LogLevel.Error)
        {
            Debug.LogError(message);
        }

        if (saveToFile)
        {
            m_logsToFile.Add(message.ToString());
        }
    }

    public static void Exception(System.Exception exception, bool saveToFile = false)
    {
        if (s_enabled && (s_logLevel & LogLevel.Exception) == LogLevel.Exception)
        {
            Debug.LogException(exception);
        }

        if (saveToFile)
        {
            m_logsToFile.Add(exception.Message);
        }
    }

    [System.Diagnostics.ConditionalAttribute("UNITY_EDITOR")]
    public static void Assert(bool condition)
    {
        Assert(condition, string.Empty, true);
    }

    [System.Diagnostics.ConditionalAttribute("UNITY_EDITOR")]
    public static void Assert(bool condition, string assertString)
    {
        Assert(condition, assertString, false);
    }

    [System.Diagnostics.ConditionalAttribute("UNITY_EDITOR")]
    public static void Assert(bool condition, string assertString, bool pauseOnFail)
    {
        if (!s_enabled)
        {
            return;
        }

        if (!condition)
        {
            Debug.LogError("Assert Failed! " + assertString);
            if (pauseOnFail)
            {
                Debug.Break();
            }
        }
    }
}