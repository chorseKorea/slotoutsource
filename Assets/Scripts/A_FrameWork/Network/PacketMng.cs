﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System;

using EDNParser;



public class PacketMng : MonoBehaviour {

    static private PacketMng m_Instance;
    static public PacketMng _Inst { get { return m_Instance; } }

    public bool m_bLog = true;

    public bool sendingMessage = false;
    public bool receivedMessage = false;

    private RECV_PACKET t_packet = new RECV_PACKET();

    void Awake() { m_Instance = this; }

    void Update()
    {
        string msg = "";

        if (NetworkMng._Inst == null)
            return;

        msg = NetworkMng._Inst.ProcessReceiveData();
        if (msg == null) return;

        ProcessPacket(msg);
    }

    byte t_byResult = 0; // 현재는 에러코드 처리 없음

    public byte ProcessPacket(string message)
    {
        if (message == "") return 0;

        //Debug.Log("ProcessPacket : " + message);

        EdnNode receiveEdn = EdnPar.read(message);
        if (receiveEdn == null)
        {
            Debug.Log("Edn파싱 null!");
            return 0;
        }

        t_byResult = 0;

        for (int i = 0; i < receiveEdn.values.Count; i++)
        {
            EdnNode node_key = receiveEdn.values[i];
            
            if (node_key.value == "cmd")
            {
                EdnNode node_val = receiveEdn.values[i + 1];
                t_packet.strCmd = node_val.value;
            }
            else if (node_key.value == "data")
            {
                t_packet.ednData = receiveEdn.values[i + 1];
            }
        }


        Debug.Log("JIS : " + t_packet.strCmd);

        switch (t_packet.strCmd)
        {
            case "SLogin": //로그인 창
                {
                    Main._Inst.m_eCurrState = eGAMESTATE.LOGIN;
                    t_byResult = RECV_LOGIN_RES(t_packet.ednData);
                }
                break;

            case "SUserInfo": // 유저 계정 정보 요청
                {
                    t_byResult = RECV_USERINFO_RES(t_packet.ednData);
                }
                break;

            case "SJoinSlotmachine": // 슬롯머신에 조인
                {
                    Main._Inst.m_eCurrState = eGAMESTATE.GAME_SCENE;
                    t_byResult = RECV_JOINSLOTMACHINE_RES(t_packet.ednData);
                }
                break;

            case "SJoinBonuSlotmachine": // 보너스릴 정보 받아오기
                {
                    Main._Inst.m_eCurrState = eGAMESTATE.GAME_SCENE;
                    t_byResult = RECV_JOINBONUSSLOTMACHINE_RES(t_packet.ednData);
                }
                break;

            case "SSpinSlotmachine": // 스핀결과 받기
                {
                    Main._Inst.m_eCurrState = eGAMESTATE.GAME_SCENE;
                    t_byResult = RECV_SPINSLOTMACHINE_RES(t_packet.ednData);
                }
                break;

            case "SExitslot":// 게임 나가기
                {
                    t_byResult = RECV_EXITSLOT_RES(t_packet.ednData);
                }
                break;
        }

        return t_byResult;

    }

    public void SendToServer(string sendmsg)
    {
        NetworkMng._Inst.SendToServer(sendmsg);
    }

    public string SEND_LOGIN_REQ(string id, string pw)
    {
        string strCmd = string.Format("\"{0}\"", "CLogin");
        string strID = string.Format("\"id\"\"{0}\"", id);
        string strPW = string.Format("\"pw\"\"{0}\"", pw);
        string strData = string.Format("\"data\"{{{0}{1}}}", strID, strPW);
        string strSendString = string.Format("{{\"cmd\"{0}{1}}}", strCmd, strData);
        
        SendToServer(strSendString);
        PacketMng._Inst.sendingMessage = true;

        return strSendString;
    }

    public byte RECV_LOGIN_RES(EdnNode ednResponse )
    {
        for (int i = 0; i < ednResponse.values.Count; i++)
        {
            EdnNode mapnode = ednResponse.values[i];
            Debug.Log ("RECV_LOGIN_RES [" + i + "]:" + mapnode.value);

            if (mapnode.value == "accountMoney")
            {
                EdnNode accountMoney = ednResponse.values[i + 1];

                long valueMoney = 0;

                if (long.TryParse (accountMoney.value.ToString () , out valueMoney))
                {
                    GamePlayer._Inst._AccountMoney = valueMoney;
                }
            }
            else if (mapnode.value == "userToken")
            {
                EdnNode userToken = ednResponse.values[i + 1];

                GamePlayer._Inst._UserToken = userToken.value.ToString ();
            }
            else if (mapnode.value == "svrDate")
            {
                EdnNode svrDate = ednResponse.values[i + 1];

                long valueServerDate = 0;

                if (long.TryParse (svrDate.value.ToString () , out valueServerDate))
                {
                    GamePlayer._Inst._ServerTime = valueServerDate;
                }
            }
        }

        if(Main._Inst._CUR_SCENE == eSCENE.LOGIN)
        {
            Scene_Login loginScene =  (Scene_Login)Main._Inst._CUR_GAME_SCENE;
            loginScene.EnterLobby ();
        }

        return 0;
    }

    public string SEND_USERINFO_REQ(string id, string pw)
    {
        string strCmd = string.Format("\"{0}\"", "CUserInfo");
        string strID = string.Format("\"id\"\"{0}\"", id);
        string strPW = string.Format("\"pw\"\"{0}\"", pw);
        string strData = string.Format("\"data\"{{{0}{1}}}", strID, strPW);
        string strSendString = string.Format("{{\"cmd\"{0}{1}}}", strCmd, strData);
        //Debug.Log(strTempString);

        SendToServer(strSendString);
        PacketMng._Inst.sendingMessage = true;

        return strSendString;
    }

    public byte RECV_USERINFO_RES(EdnNode en)
    {
        for (int i = 0; i < en.values.Count; i++)
        {
            EdnNode mapnode = en.values[i];
            //Debug.Log("RECV_USERINFO_RES [" + i + "]:" + mapnode.value);

            if (mapnode.value == "accountMoney")
            {
                EdnNode accountMoney = en.values[i + 1];

                long valueMoney = 0;

                if (long.TryParse (accountMoney.value.ToString () , out valueMoney))
                {
                    GamePlayer._Inst._AccountMoney = valueMoney;
                }
                
            }
            else if (mapnode.value == "userToken")
            {
                EdnNode userToken = en.values[i + 1];

                GamePlayer._Inst._UserToken = userToken.value.ToString ();
                
            }
            else if (mapnode.value == "svrDate")
            {
                EdnNode svrDate = en.values[i + 1];

                long valueServerDate = 0;

                if (long.TryParse (svrDate.value.ToString () , out valueServerDate))
                {
                    GamePlayer._Inst._ServerTime = valueServerDate;
                }
            }
        }

        return 0;
    }

    public string SEND_SPIN_REQ(string id , uint betLine , uint betPerLine)
    {
        string strCmd = string.Format ("\"{0}\"" , "CSpinSlotmachine");
        string strBetLineCount = string.Format ("\"betLineCount\"{0}" , betLine);
        string strUserToken = string.Format ("\"userToken\"\"{0}\"" , id);
        string strUserReel = string.Format ("\"userReel\"\"{0}\"" , "main"); 
        string strBetPerLine = string.Format("\"betPerLine\"{0}" ,betPerLine );

        string strData = string.Format ("\"data\"{{{0}{1}{2}{3}}}" , strBetLineCount , strUserToken , strUserReel , strBetPerLine);
        string strSendString = string.Format ("{{\"cmd\"{0}{1}}}" , strCmd , strData);

        SendToServer (strSendString);
        PacketMng._Inst.sendingMessage = true;

        return strSendString;
    }

    public string SEND_BONUS_SPIN_REQ ( string id , uint betLine , uint betPerLine )
    {
        string strCmd = string.Format ("\"{0}\"" , "CSpinSlotmachine");
        string strBetLineCount = string.Format ("\"betLineCount\"{0}" , betLine);
        string strUserToken = string.Format ("\"userToken\"\"{0}\"" , id);
        string strUserReel = string.Format ("\"userReel\"\"{0}\"" , "bonus");
        string strBetPerLine = string.Format ("\"betPerLine\"{0}" , betPerLine);

        string strData = string.Format ("\"data\"{{{0}{1}{2}{3}}}" , strBetLineCount , strUserToken , strUserReel , strBetPerLine);
        string strSendString = string.Format ("{{\"cmd\"{0}{1}}}" , strCmd , strData);

        SendToServer (strSendString);
        PacketMng._Inst.sendingMessage = true;

        return strSendString;
    }

    public string SEND_JOINSLOTMACHINE_RES(string id, uint betamount, bool autosel)
    {
        string strCmd = string.Format("\"{0}\"", "CJoinSlotmachine");
        string strBetAmount = string.Format("\"betAmount\"{0}", betamount);
        string strUserToken = string.Format("\"userToken\"\"{0}\"", id);
        string strAutoSelect = string.Empty;

        if(autosel)
            strAutoSelect = string.Format("\"autoSelect\"{0}", "true");   // true이면 자동입장(대소문자 조심)
        else
            strAutoSelect = string.Format ("\"autoSelect\"{0}" , "false");   

        string strData = string.Format("\"data\"{{{0}{1}{2}}}", strBetAmount, strUserToken, strAutoSelect);
        string strSendString = string.Format("{{\"cmd\"{0}{1}}}", strCmd, strData);

        SendToServer(strSendString);
        PacketMng._Inst.sendingMessage = true;

        return strSendString;
    }

    public byte RECV_JOINSLOTMACHINE_RES(EdnNode en)
    {

        for (int i = 0; i < en.values.Count; i++)
        {
            EdnNode mapnode = en.values[i];
            Debug.Log ("RECV_JOINSLOTMACHINE_RES [" + i + "]:" + mapnode.value);

            if (mapnode.value == "chips")
            {
                EdnNode chips = en.values[i + 1];

                int valueChip = 0;

                if(int.TryParse(chips.value.ToString() , out valueChip ))
                {
                    SlotDataMgr._Inst.m_amountBalance = valueChip;
                }
                
            }
            else if (mapnode.value == "slotId")
            {
                EdnNode slotId = en.values[i + 1];

                SlotDataMgr._Inst.m_slotID = slotId.value.ToString();
            }
            else if (mapnode.value == "jackpot")
            {
                EdnNode jackpot = en.values[i + 1];

                float valueJackpot = 0;

                if (float.TryParse (jackpot.value.ToString () , out valueJackpot))
                {
                    SlotDataMgr._Inst.m_rewardJackpot = valueJackpot;
                }
            }
            else if (mapnode.value == "reelInfo")
            {
                EdnNode reelInfo = en.values[i + 1];
                Debug.Log ("JIS reelInfo.values.Count:" + reelInfo.values.Count);

                SlotDataMgr._Inst.m_normalReelInfoMng= new ReelInfoMng();
                SlotDataMgr._Inst.m_normalReelInfoMng.AddReelInfo(reelInfo);
            }
        }

        SlotDataMgr._Inst.m_isRecReelData = true;
        SlotDataMgr._Inst.UpdateSlotMachine();

        return 0;
    }

    public string SEND_JOINBONUSSLOTMACHINE_RES(string id, uint betamount, bool autosel)
    {
        string strCmd = string.Format("\"{0}\"", "CJoinBonusSlotmachine");
        string strBetAmount = string.Format("\"betAmount\"{0}", betamount);
        string strUserToken = string.Format("\"userToken\"\"{0}\"", id);
        string strAutoSelect = string.Format("\"autoSelect\"{0}", autosel);   // true이면 자동입장

        string strData = string.Format("\"data\"{{{0}{1}{2}}}", strBetAmount, strUserToken, strAutoSelect);
        string strSendString = string.Format("{{\"cmd\"{0}{1}}}", strCmd, strData);

        SendToServer(strSendString);
        PacketMng._Inst.sendingMessage = true;

        return strSendString;
    }

    public byte RECV_JOINBONUSSLOTMACHINE_RES(EdnNode en)
    {
        for (int i = 0; i < en.values.Count; i++)
        {
            EdnNode mapnode = en.values[i];
            //Debug.Log("RECV_JOINBONUSSLOTMACHINE_RES [" + i + "]:" + mapnode.value);

            if (mapnode.value == "slotId")
            {
                EdnNode slotId = en.values[i + 1];

                SlotDataMgr._Inst.m_slotID = slotId.value.ToString();
            }
            else if (mapnode.value == "reelInfo")
            {
                EdnNode reelInfo = en.values[i + 1];

                SlotDataMgr._Inst.m_bonusReelInfoMng= new ReelInfoMng();
                SlotDataMgr._Inst.m_bonusReelInfoMng.AddReelInfo(reelInfo);

            }
        }
        SlotDataMgr._Inst.m_isRecReelData = true;
        SlotDataMgr._Inst.UpdateSlotMachine();

        return 0;
    }

    public byte RECV_SPINSLOTMACHINE_RES(EdnNode en)
    {
        for (int i = 0; i < (en.values.Count - 1); i++)
        {
            EdnNode mapnode = en.values[i];
            //Debug.Log ("RECV_SPINSLOTMACHINE_RES [" + i + "]:" + mapnode.value);

            if (mapnode.value == "scatter")
            {
                EdnNode scatter = en.values[i + 1];

                int numScatter = 0;

                if(int.TryParse(scatter.value.ToString(), out numScatter))
                {
                    SlotDataMgr._Inst.m_numScatter = numScatter;
                }
            }
            else if (mapnode.value == "reelIndex")
            {
                EdnNode reelIndex = en.values[i + 1];

                int[] valueReelIndexes = new int[reelIndex.values.Count];
                EdnNode reelIndexValue = null;
                int valueReelIndex = 0;

                for(int idx = 0 ; idx < valueReelIndexes.Length ; idx ++)
                {
                    reelIndexValue = reelIndex.values[idx];

                    if (int.TryParse (reelIndexValue.value.ToString () , out valueReelIndex))
                    {
                        valueReelIndexes[idx] = valueReelIndex;
                    }
                }

                SlotDataMgr._Inst.SetReelIndexes(valueReelIndexes);
            }
            else if (mapnode.value == "chips")
            {
                EdnNode chips = en.values[i + 1];

                int valueChips = 0;

                if(int.TryParse(chips.value.ToString() , out valueChips))
                {
                    SlotDataMgr._Inst.m_amountBalance = valueChips;
                }

            }
            else if (mapnode.value == "userToken")
            {
                EdnNode userToken = en.values[i + 1];

                GamePlayer._Inst._UserToken = userToken.value.ToString ();
            }
            else if (mapnode.value == "slotId")
            {
                EdnNode slotId = en.values[i + 1];

                SlotDataMgr._Inst.m_slotID = slotId.value.ToString();
            }
            else if (mapnode.value == "jackpot")
            {
                EdnNode jackpot = en.values[i + 1];

                float valueRewardJackpot = 0;

                if(float.TryParse(jackpot.value.ToString(),out valueRewardJackpot))
                {
                    SlotDataMgr._Inst.m_rewardJackpot = valueRewardJackpot;
                }

            }
            else if (mapnode.value == "bingo")
            {
                EdnNode bingo = en.values[i + 1];

                SlotDataMgr._Inst.SetBingoValueWithEdnNode(bingo);
            }
            else if (mapnode.value == "backs")
            {
                EdnNode backs = en.values[i + 1];
            }
        }

        SlotDataMgr._Inst.RecievedSpinResult();

        return 0;
    }

    // SEND_EXIT_REQ 함수 없음

    public string SEND_EXITSLOT_REQ(string userToken , string slotid)
    {
        string strCmd = string.Format ("\"{0}\"" , "CExitslot");
        string strUserToken = string.Format ("\"userToken\"\"{0}\"" , userToken);
        string strSlotID = string.Format ("\"slotId\"\"{0}\"" , slotid);

        string strData = string.Format ("\"data\"{{{0}{1}}}" , strUserToken , strSlotID);
        string strSendString = string.Format ("{{\"cmd\"{0}{1}}}" , strCmd , strData);

        SendToServer (strSendString);
        PacketMng._Inst.sendingMessage = true;

        string sendMsg = string.Empty;

        return sendMsg;
    }

    public byte RECV_EXITSLOT_RES(EdnNode en)
    {
        for (int i = 0; i < en.values.Count; i++)
        {
            EdnNode mapnode = en.values[i];
            Debug.Log ("RECV_EXITSLOT_RES [" + i + "]:" + mapnode.value);

            if (mapnode.value == "slotId")
            {
                EdnNode slotId = en.values[i + 1];
            }

            SlotDataMgr._Inst.OnReceivedExitReq();
        }

        return 0;
    }
}
