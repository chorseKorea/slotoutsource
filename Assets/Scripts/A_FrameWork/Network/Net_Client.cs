﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.IO;

public class Net_Client 
{
    Socket client = null;
    Thread th = null;

    public string m_strGW_Ip = "192.168.0.92";		// 게이트웨이 IP - 실서비스 버전은 웹서버에서 받아서 처리해야 함
    public int m_iGW_Port = 8200;					// 게이트웨이 Port
    short HEADSET = 0x73d1;							// prefix

    static public int MAX_RECV_BUF = 16384;//256;	// 버퍼 사이즈
    int MAX_RECV_BUF_HEAD = 6;						// 헤더 크기
    int m_cur_data_index = 0;						// 현재 처리중인 버퍼 인덱스 - 데이터 수신 관련 변수
    int m_end_data_index = 0;						// 데이터가 기록된 마지막 버퍼 인덱스
    int m_max_data_index = 100;						// 총 버퍼 수
    
    private bool m_bUseEncKey = false;				// 암호화 키 사용여부
    String tkey = "o6xNzq";
    private byte[] enkey;							// 암호화 키 Byte 배열

    private eSOCKET_STATE m_eSocketState = eSOCKET_STATE.Not_Connect;	// 접속 상태
    private SocketError m_errorCode;				// 소켓 에러 코드 - 에러 발생시 기록

    private bool m_bIsActGW = true;					// true:GW접속, fals:Lobby 접속
    public bool _IsActGW { get { return m_bIsActGW; } set { m_bIsActGW = value; } }





    private NetworkStream ns;
    private StreamWriter sw;
    private StreamReader sr;



    public Queue m_msgQ = new Queue();


    public Net_Client()
    {
        enkey = new byte[tkey.Length];
        enkey = Encoding.UTF8.GetBytes(tkey);
    }

    // 게이트웨이 접속
    public bool ConnectGW(GameObject target, string strCallBack)
    {
        try
        {
            m_bIsActGW = true;
            if (client != null) Disconnect();

            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            client.Connect(IPAddress.Parse(m_strGW_Ip), m_iGW_Port);
            
            
            Debug.Log("JIS GW Connecting!!");


            ns = new NetworkStream(client);
            //sr = new StreamReader(ns);
            sw = new StreamWriter(ns);


            return true;
        }
        catch (Exception ex)
        {
            NoticeDisConnect(eDISCONNECT_TYPE.Connect, ex.Message);
            if (target != null) target.SendMessage(strCallBack, false, SendMessageOptions.DontRequireReceiver);
            return false;
        }
    }

    // 데이터 수신 스레드 시작
    public bool StartThread(GameObject target, string strCallBack)
    {
        try
        {
            if (client == null) return false;
            if (th == null)
            {
                Debug.Log("JIS 수신 스레드 시작!");
                th = new Thread(new ThreadStart(Receive));
                th.Start();
                th.IsBackground = true;
            }
            //else
            //{
            //    th.Resume();
            //}

            target.SendMessage(strCallBack, true, SendMessageOptions.DontRequireReceiver);

            m_eSocketState = eSOCKET_STATE.Connected;

            return true;
        }
        catch (Exception ex)
        {
            NoticeDisConnect(eDISCONNECT_TYPE.Thread, ex.Message);
            if (target != null) target.SendMessage(strCallBack, false, SendMessageOptions.DontRequireReceiver);
            return false;
        }
    }

    // 네트워크 데이터 수신
    private void Receive()
    {
        try
        {
            while (client != null && client.Connected)
            {
                ReceiveData();
            }
        }
        catch (Exception ex)
        {
            NoticeDisConnect(eDISCONNECT_TYPE.Receive_Data, ex.Message);
        }
    }

    private byte[] buf_recv = new byte[MAX_RECV_BUF];		// 임시 수신버퍼
    //private byte[] buf_recv2 = new byte[MAX_RECV_BUF];		// 임시 수신버퍼(암호화 사용시 사용)
    // 수신 데이터 복호화 및 버퍼에 기록
    private void ReceiveData()
    {
        int total = 0;					// 총 수신된 데이터
        int size = MAX_RECV_BUF_HEAD;	// 수신할 데이터 크기
        int left_data = size;			// 남은 데이터 크기
        int recv_data = 0;				// 수신된 데이터 크기
        

        // Body 수신
        total = 0;
        size = MAX_RECV_BUF;
        left_data = size;

        Array.Clear(buf_recv, 0, buf_recv.Length);
        
        recv_data = client.Receive(buf_recv, SocketFlags.None);
        

        if (m_bUseEncKey)
        {
            
        }
        else
        {
            PushReceiveMsg(recv_data, buf_recv);
        }
    }

    // 수신 데이터 버퍼에 기록
    bool t_bLackBuffer = false;
    //public bool PushReceiveMsg(short opcode, short body_size, byte[] body_data)
    public bool PushReceiveMsg(int body_size, byte[] body_data)
    {
        try
        {
            string recvString = Encoding.UTF8.GetString(body_data, 0, body_size);
            Debug.Log("메세지 받음 크기:" + body_size + " " + recvString);

            string[] stringArray = recvString.Split('\n');
            for (int i = 0; i < stringArray.Length; i++)
            {
                string tempString = stringArray[i];

                if (tempString.Length > 0)
                {
                    //Debug.Log("큐에 삽입 " + tempString);
                    m_msgQ.Enqueue(tempString);
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            NoticeDisConnect(eDISCONNECT_TYPE.Record_Data, ex.Message);
            return false;
        }
    }

    public string ProcessReceiveData()
    {
        if (m_msgQ.Count > 0)
        {
            string returnMsg = (string)m_msgQ.Dequeue();
            //Debug.Log("ProcessReceiveData m_msgQ.Count : " + m_msgQ.Count);
            return returnMsg;
        }

        return null;
    }

    // 데이터 전송
    public void Send(string message)
    {
        try
        {
            if (client.Connected)
            {
                sw.WriteLine(message);
                sw.Flush();
                
                //this.SendData(message);
            }
        }
        catch (Exception ex)
        {
            NoticeDisConnect(eDISCONNECT_TYPE.Send_Data, ex.Message);
        }
    }

    private byte[] buf_send = new byte[MAX_RECV_BUF];
    private void SendData(string message)
    {
        short[] ssize = { (short)MAX_RECV_BUF };

        int total = 0;									// 전송한 데이터 사이즈
        int left_data = ssize[0] + MAX_RECV_BUF_HEAD;	// 남은 데이터 사이즈
        int send_data = 0;								// 전송한 데이터 사이즈
        int total_send_size = left_data;				// 전송할 총 데이터 사이즈

        Array.Clear(buf_send, 0, total_send_size);

        // body 복사
        Buffer.BlockCopy(message.ToCharArray(), 0, buf_send, MAX_RECV_BUF_HEAD, ssize[0]);


       

        
        while (total < total_send_size)
        {
            send_data = client.Send(buf_send, total, MAX_RECV_BUF_HEAD, SocketFlags.None);
            total += send_data;
            left_data -= send_data;
        }
         
    }

    // 네트워크 연결 종료(정상 종료)
    public void Disconnect()
    {
        try
        {
            //if (client != null)
            //{
            //    if (client.Connected)
            //    {
            //        client.Close();
            //    }
            //    client = null;
            //}

            //m_cur_data_index = m_end_data_index = 0;
            m_eSocketState = eSOCKET_STATE.DisConnected;
            NoticeDisConnect(eDISCONNECT_TYPE.Normal, "Normal Network Disconnect.");
        }
        catch (Exception ex)
        {
            //iLog.Log(eLOG_OUTPUT.iDEBUGnCON, "Disconnect : " + ex.Message);
        }
    }

    // 네트워크 연결 종료를 알림
    public void NoticeDisConnect(eDISCONNECT_TYPE eType, string strMsg)
    {
        Debug.Log(string.Format("Disconnect ({0}) : {1}  SocketError:{2}", eType.ToString(), (strMsg == null ? "" : strMsg), m_errorCode));

        if (eType != eDISCONNECT_TYPE.Normal)
        {
            if (m_eSocketState == eSOCKET_STATE.Connected)
            {
                byte[] tdata = new byte[1];
                //PushReceiveMsg(500, 1, tdata);
                m_eSocketState = eSOCKET_STATE.DisConnected;
                // 재접속 시도는 500 번 메시지에서 팝업을 띄우거나 아니면 자동 접속을 처리할 것.
            }
        }

        if (client != null)
        {
            if (client.Connected)
            {
                client.Close();
            }
            client = null;
        }

        try
        {
            if (th != null)
            {
                if (th.IsAlive)
                {
                    //th.Suspend();// .Interrupt();
                    th.Interrupt();
                    th.Join();
                    //Thread.Sleep(10);
                }
                th = null;
            }
        }
        catch (ThreadAbortException ex)
        {
            //iLog.Log(eLOG_OUTPUT.iDEBUGnCON, ex.Message);
        }
    }

    // 소켓 생성 확인
    public bool IsClient()
    {
        if (client != null) return true;
        return false;
    }


    // 네트워크 접속 확인
    public bool IsConnected()
    {
        if (client != null)
        {
            if (client.Connected)
                return true;
        }
        return false;
    }

    public void Interrupt()
    {
        if (th != null)
        {
            if (th.IsAlive)
            {
                th.Interrupt();
                th.Join();
            }
        }
    }

    public void Aboart()
    {
        if (th != null)
        {
            if (th.IsAlive)
            {
                th.Abort();
                th.Join();
            }
        }
    }

    //~Net_Client()
    //{
    //    Interrupt();
    //}

    // 기타
    // IP 주소 확인
    private string Get_MyIP()
    {
        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
        string myip = host.AddressList[0].ToString();
        return myip;
    }
}
