﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using EDNParser;

public enum eSOCKET_STATE : byte
{
    Not_Connect = 0,	// 미접속 상태
    Connected,			// 접속중
    DisConnected,		// 접속종료
    Connect_Retry,		// 접속 재시도
}

public enum eDISCONNECT_TYPE : byte
{
    Normal = 0,			// 일반 연결 종료
    Connect,			// 소켓 연결중 종료
    Thread,				// 스레드 생성중 종료
    Receive_Data,		// 데이터 받는중 종료
    Send_Data,			// 데이터 전송중 종료
    Record_Data,		// 버퍼에 기록중 종료
    Fetch_Msg			// 수신 데이터 확인중 종료
}

public struct RECV_PACKET
{
    public string strScene;
    public string strCmd;
    public string strErr;
    public EdnNode ednData;
};

public class NetworkMng : MonoBehaviour {

    private static NetworkMng m_Instance;
    static public NetworkMng _Inst { get { return m_Instance; } }

    private Net_Client m_netCli = new Net_Client();


    private const int MAXSIZE = 16384; //* 8192  */
    public string HOST = "192.168.0.92";
    public int PORT = 8100;



    void Awake() { m_Instance = this; }

    
    public void SetGWAddress(string ip, int port)
    {
        m_netCli.m_strGW_Ip = ip;
        m_netCli.m_iGW_Port = port;

        //iLog.Log(eLOG_OUTPUT.iDEBUGnCON, "GW Address : " + ip + "  " + port);
        Debug.Log("GW Address : " + ip + ", Port : " + port);
    }

    public void StartClient()
    {
        // Connect to a remote device.
        try
        {
            SetGWAddress(HOST, PORT);
            Main._Inst.m_eCurrState = eGAMESTATE.CONNECT;
            ConnectGW(gameObject, "CB_GWConnect");
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public bool ConnectGW(GameObject target, string strCallBack)
    {
        bool bResult = false;
        bResult = m_netCli.ConnectGW(target, strCallBack);
        if (bResult)
        {
            m_netCli.StartThread(target, strCallBack);
        }
        //else	// 굳이 필요없어서 막음(Net_Client 에서 처리)
        //{
        //    target.SendMessage(strCallBack, false, SendMessageOptions.DontRequireReceiver);
        //}

        return bResult;
    }

    void CB_GWConnect(bool bConnect)
    {
        //iLog.Log(eLOG_OUTPUT.iDEBUGnCON, "GW Connect Success ? " + bConnect);

        if (bConnect)
        {
            // 게임시간 체크를 한 후에 로비접속을 수행한다.
            //PacketMng._Inst.SEND_REFRESH_SERVER_TIME_REQ ();
            //PacketMng._Inst.SEND_LOGIN_REQ ("akoz1" , "pw");
            if(Main._Inst._CUR_SCENE != eSCENE.LOGIN)
            {
                Main._Inst._CUR_SCENE = eSCENE.LOGIN;
            }
            else
            {
                Scene_Login loginScene = Main._Inst._CUR_GAME_SCENE as Scene_Login;
                loginScene.LoginToServer ();
            }
        }
        else
        {
            Debug.Log ("connection is failed");
             //게이트웨이 접속 실패 팝업
        }
    }

    public void SendToServer(string message)
    {
        try
        {
            m_netCli.Send(message);
            Debug.Log("메세지 보냄: " + message);

        }
        catch (Exception ex)
        {
            Debug.Log("전송 중 오류발생" + ex.Message);
        }
    }

    public string ProcessReceiveData()
    {
        return m_netCli.ProcessReceiveData();
    }



    void OnDestroy()
    {
        Abort();
    }

    void OnApplicationQuit()
    {
        Abort();
    }

    void Abort()
    {
        if (m_netCli != null)
        {
            m_netCli.Disconnect();
            m_netCli.Interrupt();
            //m_netCli.Aboart();
            m_netCli = null;
        }
    }
}
