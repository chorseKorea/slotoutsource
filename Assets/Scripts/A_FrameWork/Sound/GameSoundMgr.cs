﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

// 사운드 사용 예제
// GameSoundMgr._Inst.PlayOneSound("Call");

public class GameSoundMgr : MonoBehaviour
{
    private GameSoundMgr() { }
    static private readonly GameSoundMgr m_Instance = new GameObject().AddComponent<GameSoundMgr>();
    static public GameSoundMgr _Inst 
    {
        get 
        {
            m_Instance.name = "GameSoundMgr";
            return m_Instance; 
        } 
    }

    private bool isTest = true;
    private bool isLoadComplete = false;

    //외부
    [SerializeField]
    private WWW www;

    [SerializeField]
    private const string cLocalPath = "file://Assets\\ResourcesBundle\\Web\\Sound\\";

    private string[] Files = GetFiles("Assets\\ResourcesBundle\\" + GameInfo.PlatFormName + "\\" + GameInfo.GameKindDirectory + "\\" + "Sound\\", "*.mp3|*.wav|*.ogg", SearchOption.AllDirectories);

    private static string[] sourceNames;

    //내부
    private AudioClip[] AudioClips;

    private Dictionary<string, AudioClip> D_AudioClips;
    private Dictionary<string, AssetBundle> mAssetBundles = new Dictionary<string, AssetBundle>();

    private string[] NoUnLoadSound = { "Coin" };

    private float WWWprogress = 0.0f;
    private GameObject m_GameSound;

    private void Awake()
    {
        //DontDestroyOnLoad(this);

        m_GameSound = new GameObject("GameSound");

        m_GameSound.transform.parent = this.transform;

        sourceNames = new string[Files.Length];

        //this.gameObject.AddComponent<AudioListener>();
        D_AudioClips = new Dictionary<string,AudioClip>();
    }

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void OnSoundLoadStart()
    {
        if (isTest)
        {
            StartCoroutine("SoundLoaderTest");
        }
        else
        {
            StartCoroutine("SoundLoader");
        }
    }

    private IEnumerator SoundLoaderTest()
    {
        for (int i = 0; i < Files.Length; i++)
        {
            string url = (string)("file://" + Files[i]).ToString();
            sourceNames[i] = sourceName(url);

            www = new WWW(url);
            if (www.error != null) Debug.Log(www.error);
            yield return www;

            AudioClip loadClip = www.GetAudioClip(true);

            //Debug.Log(sourceNames[i]);

            /*
            String[] _temp = CommonUtil.OnStringSplits(sourceNames[i]);

            if (GameObject.Find(_temp[0]) == null) {
                GameObject audioObject = new GameObject();
                audioObject.name = _temp[0];
			    audioObject.transform.parent = m_GameSound.transform;
            }
            */
            loadClip.name = sourceNames[i];

            D_AudioClips.Add(sourceNames[i], loadClip);
        }
    }

    /// <summary>
    /// 타켓 소스 이름 자르기.
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private string sourceName(string name)
    {
        string[] Names = name.Split(new Char[] { '/', '\\', '.' });
        int length = Names.Length;
        return Names[length - 2];
    }

    public static string[] GetFiles(string path, string searchPattern, SearchOption searchOption)
    {
        string[] dir = Directory.GetDirectories(path);

        for (int i = 0; i < dir.Length; i++)
        {
            Debug.Log(dir[i]);
        }

        string[] searchPatterns = searchPattern.Split('|');
        List<string> files = new List<string>();

        for (int i = 0; i < searchPatterns.Length; i++)
        {
            string sp = searchPatterns[i];

            // files.AddRange(Directory.GetFiles(path, sp, searchOption));

            // 임시 Web 번들 묶을때는 인자 3개는 에러!! 원인은 모르겠음
            files.AddRange(Directory.GetFiles(path, sp));
        }

        files.Sort();

        return files.ToArray();
    }

    private IEnumerator SoundLoader()
    {
        yield return StartCoroutine(LoadAssetBundle("GameSound"));
        int count = AssetCount("GameSound");

        AudioClips = new AudioClip[count];
        AudioClips = GetObjects<AudioClip>("GameSound");

        for (int i = 0; i < count; i++)
        {
            GameObject audioObject = new GameObject();
            audioObject.name = AudioClips[i].name;
            audioObject.transform.parent = this.transform;

            D_AudioClips.Add(AudioClips[i].name, AudioClips[i]);
        }

        yield return StartCoroutine(LoadAssetBundle("MusicSelectPrev"));
        AudioClip[] tempClip = GetObjects<AudioClip>("MusicSelectPrev");

        for (int i = 0; i < tempClip.Length; i++)
        {
            Debug.Log(tempClip[i]);
            //D_MusicSelectClips.Add(tempClip[i].name, tempClip[i]);
        }
    }

    private bool HasAssetBundle(string name)
    {
        if (mAssetBundles == null) return false;
        if (mAssetBundles.ContainsKey(name)) return true;

        return false;
    }

    private void AddAssetBundle(string name, AssetBundle value)
    {
        if (HasAssetBundle(name)) return;
        mAssetBundles.Add(name, value);
    }

    public IEnumerator LoadAssetBundle(string name)
    {
        if (HasAssetBundle(name) == true)
        {
            //Log.AddLog( string.Format( "[LoadAssetBundle:HasAssetBundle] {0}", name ) );
            yield break;
        }

        string _assetName = "";// = AssetBundleMgr.Instance.GetAssetBundleName(name);
        string path = "";// = GlobalFunc.StreamingAssetsPath(_assetName + ".bundle");

        WWWprogress = 0f;

        www = new WWW(path);
        while (!www.isDone)
        {
            WWWprogress = www.progress;
            yield return null;
        }

        if (string.IsNullOrEmpty(www.error))
        {
            //Log.AddLog( "[AssetBundle Load Complate] " + path );
            AddAssetBundle(name, www.assetBundle);
        }
        else
        {
            //Log.AddLog( "[AssetBundle Load Fail] " + path );
            //Log.AddLog( "[Error] " + www.error );
        }

        if (www != null)
        {
            www.Dispose();
            www = null;
        }
    }

    private T[] GetObjects<T>(string assetname) where T : UnityEngine.Object
    {
        AssetBundle assetbundle;

        if (mAssetBundles.TryGetValue(assetname, out assetbundle) == false)
        {
            //Log.AddLog( string.Format("error: {0} is null",assetname));
            return null;
        }

        UnityEngine.Object[] objs = new UnityEngine.Object[10]; //assetbundle.LoadAll();

        List<T> list = new List<T>();
        for (int i = 0; i < objs.Length; ++i)
        {
            if (objs[i].GetType() != typeof(T)) continue;

            list.Add((T)objs[i]);
        }

        if (list.Count <= 0)
        {
            //Log.AddLog( string.Format("error: {0} is not in {1}", typeof(T).ToString(), assetname ) );
            return null;
        }

        return list.ToArray();
    }

    public int AssetCount(string assetname)
    {
        AssetBundle assetbundle;

        if (mAssetBundles.TryGetValue(assetname, out assetbundle) == false)
        {
            //Log.AddLog( string.Format("error: {0} is null",assetname));
            return 0;
        }

        UnityEngine.Object[] objs = new UnityEngine.Object[10];// assetbundle.LoadAll();
        return objs.Length;
    }

    public bool IsSoundPlaying(string _objectName)
    {
        GameObject target = m_GameSound.gameObject;

        if (target != null)
        {
            AudioSource targetSource = null;

            List<AudioSource> listAudioSource = target.GetComponentsInChildren<AudioSource>().ToList();

            if (listAudioSource != null && listAudioSource.Count > 0)
            {
                List<AudioSource> listTargetAS = listAudioSource.FindAll(ASource => ASource.clip.name == _objectName);

                for (int i = 0; i < listTargetAS.Count; i++)
                {
                    targetSource = listTargetAS[i];

                    return targetSource.isPlaying;
                }
            }
            else
            {
                return false;
            }
        }

        return false;
    }

    //사운드 플레이
    public void PlayOneSound(string _objectName)
    {
        GameObject target = m_GameSound.gameObject;

        if (target != null)
        {
            AudioSource targetSource = null;
            targetSource = target.AddComponent<AudioSource>();

            if (D_AudioClips.ContainsKey(_objectName))
            {
                targetSource.clip = D_AudioClips[_objectName];
            }
            else
            {
                string path = GameInfo.PlatFormName + "/" + GameInfo.GameKindDirectory + "/" + "Sound/";

                UnityEngine.Object obj = LoadAsset_Mng._Inst.LoadAsset(eASSETBUNDLE_TYPE.sound, path, _objectName, eASSET_EXT_TYPE.Sound);

                if (obj == null)
                {
                    Debug.LogWarning("Not Exist Sound Clip !!! " + _objectName);
                    return;
                }

                targetSource.clip = (AudioClip)obj;
                D_AudioClips.Add(_objectName, (AudioClip)obj);
            }

            targetSource.Play();
            StartCoroutine(DestroyAudio(targetSource));
        }
    }

    private IEnumerator DestroyAudio(AudioSource _source)
    {
        if (_source != null)
        {
            while (_source.isPlaying)
            {
                yield return null;
            }

            Destroy((AudioSource)_source);
        }
        else
            yield return null;
    }

    //

    public void StopPlaySound(string _objectName)
    {
        GameObject target = m_GameSound.gameObject;

        if (target != null && D_AudioClips.ContainsKey(_objectName))
        {
            AudioSource targetSource = null;

            List<AudioSource> listAudioSource = target.GetComponentsInChildren<AudioSource>().ToList();

            if(listAudioSource != null && listAudioSource.Count > 0)
            {
                List<AudioSource> listTargetAS = listAudioSource.FindAll(ASource => ASource.clip.name == _objectName);
                
                for(int i =0 ; i < listTargetAS.Count ; i++ )
                {
                    targetSource = listTargetAS[i];

                    if (targetSource.isPlaying)
                    {
                        targetSource.Stop();
                        Destroy((AudioSource)targetSource);
                        StopCoroutine(DestroyAudio(targetSource));
                    }
                }
            }
        }
    }

    //반복 사운드 플레이-------------
    public void PlayLoopSound(string _objectName, bool _boolean = true)
    {
        GameObject target = m_GameSound.gameObject;

        if (target != null)
        {
            AudioSource targetSource = null;

            List<AudioSource> listAudioSource = target.GetComponentsInChildren<AudioSource>().ToList();

            if (D_AudioClips.ContainsKey(_objectName) && listAudioSource != null && listAudioSource.Count > 0)
            {
                List<AudioSource> listTargetAS = listAudioSource.FindAll(ASource => ASource.clip.name == _objectName);

                for (int i = 0; i < listTargetAS.Count; i++)
                {
                    targetSource = listTargetAS[i];

                    if (targetSource.isPlaying && !_boolean)
                    {
                        targetSource.loop = _boolean;

                    }
                }
            }
            else
            {
                targetSource = target.AddComponent<AudioSource>();

                string path = GameInfo.PlatFormName + "/" + GameInfo.GameKindDirectory + "/" + "Sound/";

                UnityEngine.Object obj = LoadAsset_Mng._Inst.LoadAsset(eASSETBUNDLE_TYPE.sound, path, _objectName, eASSET_EXT_TYPE.Sound);

                if (obj == null)
                {
                    Debug.LogWarning("Not Exist Sound Clip !!! " + _objectName);
                    return;
                }

                targetSource.clip = (AudioClip)obj;
                D_AudioClips.Add(_objectName, (AudioClip)obj);

                targetSource.Play();
                targetSource.loop = _boolean;
            }
        }
    }

    //

    private void UnLoadSounds()
    {
        int cout = this.transform.childCount;

        for (int i = 0; i < cout; i++)
        {
            AudioSource[] audios = this.transform.GetChild(i).GetComponents<AudioSource>();

            for (int j = 0; j < audios.Length; j++)
            {
                for (int k = 0; k < NoUnLoadSound.Length; k++)
                {
                    if (audios[j].name == NoUnLoadSound[k]) return;

                    Destroy(audios[j]);
                }
            }
        }
    }

    private void OnLevelWasLoaded()
    {
        UnLoadSounds();
    }
}