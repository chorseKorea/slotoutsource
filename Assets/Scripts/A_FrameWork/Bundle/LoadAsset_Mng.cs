﻿using System.Collections.Generic;
using UnityEngine;

// 어셋 번들 유형
public enum eASSETBUNDLE_TYPE : byte
{
    xml = 0 ,
    game ,
    ui ,
    sound ,

    //editor,		// 실제 배포버전에는 포함되지 않는 번들
    END
}

// 어셋 확장자
public enum eASSET_EXT_TYPE : byte
{
    Prefab = 0 ,
    Png ,
    Xml ,
    Material ,
    Sound
}

public class LoadAsset_Mng : MonoBehaviour
{
    static private LoadAsset_Mng m_Instance;

    static public LoadAsset_Mng _Inst
    {
        get
        {
            return m_Instance;
        }
    }

    public bool m_bUseAssetBundle = false;	// true:에셋번들 사용

    public Dictionary<eASSETBUNDLE_TYPE , AssetBundle> m_mapBundle = new Dictionary<eASSETBUNDLE_TYPE , AssetBundle> ();

#if UNITY_EDITOR
    public AssetBundle[] m_arBundle = new AssetBundle[(int)eASSETBUNDLE_TYPE.END];	// 테스트 용
#endif

    private string[] m_strExt =
	{
		".prefab",
		".png",
		".xml",
		".mat",
		".wav"
	};

    private void Awake ()
    {
        m_Instance = this;
#if !UNITY_EDITOR
        m_bUseAssetBundle = true;
#endif
    }

    public void SetAssetBundle ( AssetBundle bundle , eASSETBUNDLE_TYPE eType )
    {
        if (!m_mapBundle.ContainsKey (eType))
            m_mapBundle.Add (eType , bundle);
        else
            Debug.LogError ("Already Exist AssetBundle. " + eType);

#if UNITY_EDITOR
        m_arBundle[(int)eType] = bundle;
#endif
    }

    // 기타 참조 - Resources.Load(string.Format("sample_build/{0}_pre", info._InfoItem.m_strResource));

    // 에섯 로드(기본 폴더 : "Assets/ResourcesBundle/")
    // 샘플
    // Object objTile = LoadAsset_Mng._Inst.LoadAsset(eASSETBUNDLE_TYPE.game, "ObjGame/Background/", "FloorObject_pre", eASSET_EXT_TYPE.Prefab);
    // GameObject goTile = (GameObject)GameObject.Instantiate(objTile);
    public Object LoadAsset ( eASSETBUNDLE_TYPE eType , string path , string asset , eASSET_EXT_TYPE eExt )
    {
        if (m_bUseAssetBundle)
        {
            //Debug.Log(eType + "  " + asset);
            //  Debug.Log ("번들에서 읽기 loadAsset : " + eType + " " + path + "  "  + asset + " " + eExt);

            Object objAsset;
            if (eExt == eASSET_EXT_TYPE.Prefab)
                objAsset = m_mapBundle[eType].LoadAsset (asset , typeof (GameObject));
            else if (eExt == eASSET_EXT_TYPE.Png)
                objAsset = m_mapBundle[eType].LoadAsset (asset , typeof (Texture2D));
            else if (eExt == eASSET_EXT_TYPE.Material)
                objAsset = m_mapBundle[eType].LoadAsset (asset , typeof (Material));
            //else if (eExt == eASSET_EXT_TYPE.Xml)
            //	objAsset = m_mapBundle[eType].Load(asset, typeof(TextAsset));
            else if (eExt == eASSET_EXT_TYPE.Sound)
                objAsset = m_mapBundle[eType].LoadAsset (asset , typeof (AudioClip));
            else
                objAsset = m_mapBundle[eType].LoadAsset (asset);

            if (objAsset == null)
            {
                Debug.Log (string.Format ("Check path : {0}, {1}" , asset , eType));
                return null;
            }

            return objAsset;
        }
        else
        {
            string strPath = "";

            string strExt = m_strExt[(int)eExt];

            strPath = string.Format ("{0}{1}{2}{3}" , "Assets/ResourcesBundle/" , path , asset , strExt);

            //Debug.Log("strPath " + strPath);

            Object objAsset = null;

#if UNITY_EDITOR
            if (eExt == eASSET_EXT_TYPE.Png)
                objAsset = Resources.LoadAssetAtPath (strPath , typeof (Texture2D));
            else if (eExt == eASSET_EXT_TYPE.Material)
                objAsset = Resources.LoadAssetAtPath (strPath , typeof (Material));
            else if (eExt == eASSET_EXT_TYPE.Sound)
                objAsset = Resources.LoadAssetAtPath (strPath , typeof (AudioClip));
            else
                objAsset = Resources.LoadAssetAtPath (strPath , typeof (GameObject));
#else

            if (eExt == eASSET_EXT_TYPE.Png)
                objAsset = Resources.Load(strPath, typeof(Texture2D));
            else if (eExt == eASSET_EXT_TYPE.Material)
                objAsset = Resources.Load(strPath, typeof(Material));
            else if (eExt == eASSET_EXT_TYPE.Sound)
                objAsset = Resources.Load(strPath, typeof(AudioClip));
            else
                objAsset = Resources.Load(strPath, typeof(GameObject));
#endif

            if (objAsset == null)
            {
                Debug.Log (string.Format ("Check path : {0}, {1}, {2}" , strPath , eType , eExt));
                return null;
            }

            return objAsset;
            //Debug.Log(string.Format("Check Type : {0}, {1}, {2}", asset, strPath, eType));
        }

        //return null;
    }

    public void UnLoad ()
    {
        //#if UNITY_EDITOR
        //        m_arBundle = null;
        //#endif

        if (m_mapBundle != null)
        {
            foreach (AssetBundle bundle in m_mapBundle.Values)
            {
                bundle.Unload (true);
            }
            m_mapBundle.Clear ();
            m_mapBundle = null;
        }
    }
}