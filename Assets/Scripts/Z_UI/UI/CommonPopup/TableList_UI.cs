﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableList_UI : UI_Script
{
    public TableListItem m_goScrollItem;

    private List<TableListItem> m_ScrollitemList = new List<TableListItem> ();
    private bool m_bCompleteInitScrollView = false;

    public UIGrid m_Grid;

    public UILabel CreateLabel;

    private void Awake ()
    {
        childPanels = gameObject.GetComponentsInChildren<UIPanel> ();
        UIType = eUI_TYPE.TableList_UI;
    }

    public override void DepthProcess ()
    {
        // 현재 루트 판넬의 depth값을 얻어와서 자식 판넬들의 값을 다 변경한다.
        //Debug.Log(childPanels.Length);

        m_currDepth = childPanels[0].depth;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.depth = ++m_currDepth;
        }
    }

    public override void DirectionStart ()
    {
        //Debug.Log("열기 연출 시작!");
    }

    public override void DirectionEnd ( eUI_TYPE eType )
    {
        //Debug.Log("닫기 연출 시작!");
        _UIClose ();
    }

    public override void _UIClose ()
    {
        gameObject.SetActive (false);
    }

    public override void UIOutClick ()
    {
        //Debug.Log("바깥클릭!");
    }

    //-------------------------------------------------------------

    public void ClickClose ()
    {
        UI_Script_Mng._Inst.HideUI (this);
    }

    private void Start ()
    {
        StartCoroutine (InitScrollView ());
    }

    private void OnEnable ()
    {
        StartCoroutine (CoRefreshScrollViewData ());
    }

    private IEnumerator InitScrollView ()
    {
        yield return null;

        m_bCompleteInitScrollView = false;

        //Debug.Log("리스트 갯수 : " + Data_Mng._Inst.GetTableListCount());

        int listCount = 3; // 임시 - 일단 10개 세팅
        for (int i = 0; i < listCount; i++)
        {
            TableListItem item = Instantiate (m_goScrollItem) as TableListItem;

            item.gameObject.transform.parent = m_Grid.gameObject.transform;
            item.gameObject.transform.localPosition = Vector3.zero;
            item.gameObject.transform.localScale = Vector3.one;

            item.SetParent (gameObject , i);

            item.SetData (i);
            item.gameObject.SetActive (false);

            m_ScrollitemList.Add (item);
        }

        // PC상에서는 이코드가 빠름
        yield return null;
        m_Grid.Reposition ();

        m_bCompleteInitScrollView = true;

        SetScrollViewData ();
    }

    private IEnumerator CoRefreshScrollViewData ()
    {
        while (true)
        {
            if (m_bCompleteInitScrollView == false)
            {
                // 최초 데이터 세팅은 스크롤뷰 세팅을 기다려야 함
                yield return new WaitForFixedUpdate ();
            }
            else
            {
                ResetScrollViewData ();
                SetScrollViewData ();
                break;
            }
        }
    }

    private void ResetScrollViewData ()
    {
        for (int i = 0; i < m_ScrollitemList.Count; i++)
        {
            TableListItem item = m_ScrollitemList[i];
            item.SetData (i);
            item.gameObject.SetActive (false);
        }
    }

    private void SetScrollViewData ()
    {
        int listCount = 3;// Data_Mng._Inst.GetTableListCount();
        //Debug.Log("JIS SetScrollViewData() listcount : " + listCount);
        if (listCount == 0)
            return;

        for (int i = 0; i < listCount; i++)
        {
            m_ScrollitemList[i].gameObject.SetActive (true);
            m_ScrollitemList[i].SetData (i);
        }

        m_Grid.Reposition ();
    }

    public void ClickTableCreateAndJoin ()
    {
        //PacketMng_Socket._Inst.SEND_JOINTABLE_REQ(-1);
    }

    public void Refresh ()
    {
        ResetScrollViewData ();
        SetScrollViewData ();
    }

    public void SetTitleChangeTest ( string msg )
    {
        CreateLabel.text = msg;
    }
}