﻿using UnityEngine;

public class Lobby_UI : UI_Script
{
    private void Awake ()
    {
        childPanels = gameObject.GetComponentsInChildren<UIPanel> ();
        UIType = eUI_TYPE.Lobby_UI;
    }

    public override void DepthProcess ()
    {
        // 현재 루트 판넬의 depth값을 얻어와서 자식 판넬들의 값을 다 변경한다.
        //Debug.Log(childPanels.Length);

        m_currDepth = childPanels[0].depth;

        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.depth = ++m_currDepth;
        }
    }

    public override void DirectionStart ()
    {
        //Debug.Log("열기 연출 시작!");
    }

    public override void DirectionEnd ( eUI_TYPE eType )
    {
        //Debug.Log("닫기 연출 시작!");
        _UIClose ();
    }

    public override void _UIClose ()
    {
        gameObject.SetActive (false);
    }

    public override void UIOutClick ()
    {
        //Debug.Log("바깥클릭!");
    }

    //-------------------------------------------------------------

    public void ClickClose ()
    {
        UI_Script_Mng._Inst.HideUI (this);
    }

    public void ClickHoldem ()
    {
        print ("holdem");
        UI_Script_Mng._Inst.GetUI (eUI_TYPE.TableList_UI);

        //PacketMng_Socket._Inst.SEND_TABLELIST_REQ("texas", Data_Mng._Inst.m_nListPerPage, 0, 0);
    }

    public void ClickSlot ()
    {
        Debug.Log ("slot Clicked");

        Main._Inst._CUR_SCENE = eSCENE.SLOT_LOBBY;
    }
}