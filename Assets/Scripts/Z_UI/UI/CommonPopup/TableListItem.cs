﻿using UnityEngine;
using System.Collections;

public class TableListItem : MonoBehaviour {

    public UILabel roomIdx;
    public UILabel blindmoney;
    public UILabel byinmoney;
    public UILabel playernum;
    public UILabel speed;
    
    private GameObject m_goParent;
    int m_nIndex;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetParent(GameObject parent, int temp)
    {
        m_goParent = parent;
    }

    public bool SetData(int idx)
    {
        m_nIndex = idx;
        

        //TableInfo info = Data_Mng._Inst.GetTableInfo(m_nIndex);
        //if (info == null) return false;

        roomIdx.text = string.Format("{0}", m_nIndex);
        blindmoney.text = string.Format("{0}/{1}", 50, 100);
        byinmoney.text = string.Format("{0}", "1000");
        playernum.text = string.Format("{0}/{1}", 5, 10);
        speed.text = string.Format("{0}", "Normal");
        return true;
    }

    public void ClickTableJoin()
    {
        
       // string title = string.Format("{0}번방 id:{1}", m_nIndex, .strTableID);
        Debug.Log("방번호 : " + m_nIndex);
        //PacketMng_Socket._Inst.SEND_JOINTABLE_REQ(m_nIndex);
    }
}
