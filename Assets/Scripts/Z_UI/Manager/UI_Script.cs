﻿using System.Collections;
using UnityEngine;

public enum eUI_TYPE	// 스크립트 매니저에 등록될 항목만 정의
{
    NONE = 0,			// 기본

    Common_Popup,
    Lobby_UI,
    CommonPanel_UI,
    TableList_UI,
    MainMenu,
    TableSetting_Popup,
    BuyIntoGame_Popup,
    TexasHoldem_Popup,
    HandBet_Popup,
    BottomPanel,
    BottomPanel_Web,
    Raise_Popup,
    SitngoTableSetting,
    MainHUD,
    Option_Popup,
    Friend_Popup,
    MyProfile_Popup,
    OtherProfile_Popup,
    InBox_Popup,
    Shop_Popup,
	HoldemOption_Popup,

    InGame_Holdem,
    InGame_Holdem_AllPanel,

    MissionList_Popup,

    Result1_Direction_UI,       // 쇼다운 글씨 연출 - 단독작동
    //Result2_Direction_UI,       // 포켓카드 연출 - 메인프리팹 조작
    //Result3_Direction_UI,       // 커뮤니티 카드 연출 - 메인프리팹 조작

    //for slot

    SlotMachine_Base ,
    Slot_Lobby,
    Popup_FreeSpin,
    Popup_Jackpot,
    Popup_BigWin,
    Popup_Win,
    Popup_Paytable_C7,

    END
}

public enum eMSG_LIST
{
    NONE = 0,
    DISCONNECT_NETWORK_50001 = 50001,			// 서버와의 접속 종료
    CANT_CONNECT_SERVER_50002 = 50002,			// 서버 접속 불가
    ALREADY_CONNECT_ANOTHER_50003 = 50003,		// 다른곳에서 접속중일때(다른 유저의 접속종료)
    CONNECTED_ANOTHER_50004 = 50004,			// 다른곳에서 접속했을때(현재 유저의 접속종료)

    LACK_GOLD = 40001,							// 골드부족

    END
}

public class UI_Script : MonoBehaviour
{
    public int m_currDepth = 0;
    public eUI_TYPE m_eType = eUI_TYPE.NONE;
    public eMSG_LIST m_eMsgType = eMSG_LIST.NONE;
    public TweenAlpha m_twaAlpha;

    public TweenScale m_twsScale;
    protected UIPanel[] childPanels;

    public bool IsNoColl = false;//콜라이더를 불러오지 않음.

    virtual public void SetData(ArrayList arr) { }
    virtual public int GetMaxDepth() { return m_currDepth; }
    virtual public bool CheckChildAllClose() { return false; }
    virtual public void ChildFirstClose() { }						// 자식들이 먼저 닫혀야 할때(빌딩UI(HudUI_Building) 참고)
    virtual public void UIOutClick() { }
    virtual public void OpenNextUI(eUI_TYPE eType) { }
    virtual public void OpenNextUI(ArrayList arr) { }
    

    // 필수적으로 모든 자식들이 구현해야 함
    virtual public void DepthProcess() { }
    virtual public void DirectionStart() { }
    virtual public void DirectionEnd(eUI_TYPE eType) { }
    virtual public void _UIClose() { } // 자신과 매니저에서만 사용하는것임
    public void TweenCreate()
    {
        m_twaAlpha = gameObject.GetComponent<TweenAlpha>();
        m_twsScale = gameObject.GetComponent<TweenScale>();
        if (m_twaAlpha != null)
        {
            m_twaAlpha.enabled = false;
        }
        if (m_twsScale != null)
        {
            m_twsScale.enabled = false;
        }
    }
    public void TweenEnable()
    {
        if (m_twaAlpha != null)
        {
            m_twaAlpha.enabled = true;
            m_twaAlpha.PlayForward();
        }
        if (m_twsScale != null)
        {
            m_twsScale.enabled = true;
            m_twsScale.PlayForward();
        }
        //m_twaAlpha.ResetToBeginning();
        //m_twsScale.ResetToBeginning();
    }
    public void TweenClose()
    {
        if (m_twaAlpha != null)
        {
            m_twaAlpha.PlayReverse();
        }
        if (m_twsScale != null)
        {
            m_twsScale.PlayReverse();
        }
        StartCoroutine("CloseDirect");
    }


    IEnumerator CloseDirect()
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);

    }
    virtual public void SetRenderQ()
    {
        int RQ = childPanels[0].startingRenderQueue;
        
        for (int i = 0; i < childPanels.Length; i++)
        {
            UIPanel item = childPanels[i];
            item.renderQueue = UIPanel.RenderQueue.Explicit;
            item.startingRenderQueue = RQ++;
        }
    }



    public eUI_TYPE UIType
    {
        get { return m_eType; }
        set { m_eType = value; }
    }

    public eMSG_LIST MSGType
    {
        get { return m_eMsgType; }
        set { m_eMsgType = value; }
    }
}