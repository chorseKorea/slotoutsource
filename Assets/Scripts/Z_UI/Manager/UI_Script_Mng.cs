﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct UIDepthInfo
{
    public eUI_TYPE m_eType;
    public eInGame_Type m_eGameType;
    public uint m_uiRenderQ;

    public UIDepthInfo(eUI_TYPE type, eInGame_Type gType, uint renderQ)
    {
        m_eType = type;
        m_eGameType = gType;
        m_uiRenderQ = renderQ;
    }
};

public enum eInGame_Type
{
    None = 0,
    InGame_Holdem_AllPanel,
}

public class UI_Script_Mng : MonoBehaviour
{
    private UI_Script_Mng() { }
    static private readonly UI_Script_Mng m_Instance = new GameObject().AddComponent<UI_Script_Mng>();
    static public UI_Script_Mng _Inst { get { return m_Instance; } }

    // Common Variable
    private GameObject m_goRoot;

    private UIRoot m_uiRoot;				// 루트
    private UIPanel m_panRoot;

    public bool m_bIsDirectioning = false;	// UI 연출중이면 true

    private GameObject m_goCollider;		// 컬라이더가 붙어있는 빈 오브젝트
    UIButton m_OutClickScript;				// UI바깥클릭 처리용

    public UIRoot GetUIRoot()
    {
        return m_uiRoot;
    }

    public GameObject GetRoot()
    {
        return m_goRoot;
    }

    // 추가, 찾기는 내부함수로
    // 외부에서 쓰려면 get- 으로
    /// <summary>
    /// Game UI GameObject가 모여있는 Dictionary
    /// </summary>
    private Dictionary<eUI_TYPE, GameObject> m_mapObj = new Dictionary<eUI_TYPE, GameObject>();
    /// <summary>
    /// Game UI String이 모여있는 Dictionary
    /// </summary>
    private Dictionary<eUI_TYPE, string> m_mapPrefabname = new Dictionary<eUI_TYPE, string>();

    /// <summary>
    /// MainGame UI GameObject가 모여있는 Dictionnary
    /// </summary>
    private Dictionary<eInGame_Type, GameObject> m_MainUiObj = new Dictionary<eInGame_Type, GameObject>();
    /// <summary>
    /// MainGame UI String이 모여있는 Dictionnary
    /// </summary>
    private Dictionary<eInGame_Type, string> m_MainUiPrefabName = new Dictionary<eInGame_Type, string>();
    


    private int m_nMaxDepth = 0;

    // 현재 붙어있는 상황을 알아야 함
    // 닫았을때 리스트에서 삭제
    // 생성, 활성화했을때 추가
    // 마지막 요소값만을 가지고 처리하기 때문에 스택구조로 가도 될듯
    private List<UIDepthInfo> m_lstDepthInfo = new List<UIDepthInfo>();



    // 뒷면 클릭 방지용
    BoxCollider2D m_BoxCollider2D = null;

    //BoxCollider m_BoxCollider = null;

    //private int m_nAdd_LastDrawCallListSize = 0;
    //private int m_nHide_LastUI_LOCALDrawCallListSize = 0;

    // 수동 렌더큐를 세팅하는 루틴이 들어갔다면 
    // 이후 추가되는 팝업은 렌더큐를 강제세팅
    //private bool m_bIsSetManualRenderQ = false;
    //private int m_nLastRenderQ = 3000;
    //private int m_nMaualRenderQScriptCount = 0;

    private GameObject m_goClosing;
    private GameObject m_goMaxGrade;

    //private UIDepthInfo m_eLastInfo;

    //private int m_nSpecialRenderQ = 0;
    //private int m_nNormalRenderQ = 0;

    private eUI_TYPE m_currType;

    // 새로운 렌더큐 관리 방식용 변수들
    private uint m_uiNormalRQ_Start = 3000;

    private uint m_uiNormalRQ_Offset = 100;
    private uint m_uiCurrRQ = 0;

    public static int s_Normal_RQ = 3000;

    public static int s_Normal_Slot_RQ_Offset = 100;
    public static int s_Highlight_Line_RQ_Offset = 200;
    public static int s_Highlight_Slot_RQ_Offset = 300;
    public static int s_Particle_RQ_Offset = 200;
    
    public static int s_iHUD_RQ = 5000;
    public static int s_iLOADING_RQ = 6000;
    public static int s_iPOPUP_RQ = 7000;
    public static int s_iMAINMSG_RQ = 8000;

    private GameObject m_ButtonPanel;

    public GameObject ButtonPalnel
    {
        get { return m_ButtonPanel; }
        set { m_ButtonPanel = value; }
    }

    public void Create(GameObject parent)
    {
        m_goRoot = parent;

        gameObject.name = "UI_Script_Mng";
        gameObject.transform.parent = m_goRoot.transform;
        gameObject.transform.localPosition = Vector3.zero;

        _CreateRoot();

        _LoadPrefabName();

        // 컬라이더 오브젝트 생성(위젯, 컬라이더, 버튼스크립트가 한세트)
        // 컬라이더의 부모설정시 그냥 빈 오브젝트는 설정이 안됨 위젯이 포함되어 있어야 함
        // 관리자가 가지고 있는 컬라이더를 각 자식들이 생성될때마다
        // 최상위 자식에다 옮기고 버튼 스크립트처리는 자식들이 처리
        m_goCollider = new GameObject("CollObj");
        m_goCollider.gameObject.transform.parent = gameObject.transform;

        
        
        UIWidget widget = m_goCollider.AddComponent<UIWidget>();
        widget.depth = 0;
        
          
        
        // 테스트 씬에서 2D컬라이더가 안붙음
        //NGUITools.AddWidgetCollider(m_goCollider);
        m_goCollider.AddComponent<BoxCollider2D>();
        

        
        m_BoxCollider2D = m_goCollider.GetComponent<BoxCollider2D>();
        
        if (m_BoxCollider2D != null)
        {
            //Debug.Log(bb + ", autoResizeBoxCollider : " + bb.GetComponent<UIWidget>().autoResizeBoxCollider);
            m_BoxCollider2D.GetComponent<UIWidget>().autoResizeBoxCollider = true;
            m_BoxCollider2D.offset = new Vector2(0f, 0f);
            m_BoxCollider2D.size = new Vector2(0f, 0f);
        }
        else
            Debug.Log("Collider error!! ");
        


        // 자식 버튼 처리와 연결
        m_OutClickScript = m_goCollider.AddComponent<UIButton>();

        m_goCollider.SetActive(false);
        
    }

    // 이 함수는 연출없이 떠있는 모든 UI를 닫는다.
    public void CloseAllUI()
    {
        // 현재 열린 모든 UI닫기
        //Debug.Log("CloseAllUI() ");

        // 현재 팝업 UI가 떠있으면 추가 작업이 안되게 막음.
        //if (PopupUI_Script_Mng._Inst.IsAnyPopUPUIOpen())
        //{
        //    //Debug.Log("팝업UI 중에 열린게 있음");

        //    // 팝업은 자동으로 닫으면 안됨 - 경고, 에러메세지가 있을수 있으므로
        //    // 꼭 사용자 입력을 받아야함 
        //    //PopupUI_Script_Mng._Inst.AllClosePopUPUI();

        //    return;
        //}

        if (IsAnyUIOpen())
        {
            //Debug.Log("일반 UI 중에 열린게 있음 " + m_lstDepthInfo.Count);
            for (int i = 0; i < m_lstDepthInfo.Count; i++ )
            {
                UIDepthInfo item = m_lstDepthInfo[i];
                //Debug.Log("UI " + item.m_eType);
                GameObject gameObj = _FindUI(item.m_eType);
                gameObj.GetComponent<UI_Script>()._UIClose();
            }

            m_lstDepthInfo.Clear();
        }
    }

    // 모든 UI닫고 다음 UI열기 - 예) 씬 전환할때 사용
    public UI_Script AllClose_And_GetUI(eUI_TYPE type, ArrayList array)
    {
        //Debug.Log("AllClose_And_GetUI() " + type);
        CloseAllUI();

        return GetUI(type, array);
    }

    public void OpenGameUi(eInGame_Type eType)
    {
        GameObject obj = _FindMainGameUi(eType);

        if (obj == null)
        {
            string path = "UI/Game/Pack/Prefabs/";

            _AddGameUi(path, eType);
        }

    }

    public void CloseGameUi(eInGame_Type eType)
    {
        
    }

    public UI_Script GetUI(eUI_TYPE type) {
        return GetUI(type, null);
    }
    public UI_Script GetUI(eUI_TYPE type, ArrayList array)
    {
        GameObject obj = _FindUI(type);

        UI_Script workScript;

        if (obj == null)
        {
            //if (Constants.JIS_MANAGERDEPTH_DEBUG) Debug.Log("---------- UI 새로 생성!! -------- " + type);

            //// 현재 팝업 UI가 떠있으면 추가 작업 막아야 함
            //if (PopupUI_Script_Mng._Inst.IsAnyPopUPUIOpen())
            //{
            //    Debug.Log("팝업UI 중에 열린게 있음");
            //    return null;
            //}

            //Debug.Log("GetUI() UI가 없음 새로 생성 : " + type);

            /*
            string path;

            if (type == eUI_TYPE.CommonPopup ||
                type == eUI_TYPE.CommonPopupTest ||
                type == eUI_TYPE.TableSetting_Popup)
            {
                path = "UI/Game/Pack/Prefabs/";
            }
            //else if (
            //        )
            //    path = "UI/Test/NewWork/";
            else
                path = "UI/Test/";
            */ //minjunji

            

            // 임시 PlatFormName 과 GameKindDirectory는 GameInfo쪽으로 뽑아야 할듯
            //string path = Main._Inst.PlatFormName + "/" + Main._Inst.GameKindDirectory + "/" + "UI/Game/Pack/Prefabs/";
            string path = "Web/SlotGame/" + "UI/Game/Pack/Prefabs/";


            //Debug.Log(path);




            workScript = _AddUI(path, type);
            if (workScript == null)
                return null;

            if (array != null) workScript.SetData(array);
        }
        else
        {
            workScript = _ReActiveUI(obj, array);
            if (workScript == null)
                return null;
        }

        m_currType = type;
        //Debug.Log("m_currType : " + m_currType);

        workScript.DirectionStart();
        return workScript;
    }

    //
    // 이 함수가 작동하려면 닫히는 UI에 DirectionEnd, OpenNextUI 오버라이드 세팅이 되어 있어야 함
    // 자식UI가 닫히는 연출이 들어가 있다면 CheckChildAllClose, ChildFirstClose 함수도 오버라이드 세팅이 되어 있어야 함
    public void HideThisUI_OpenNextUI(eUI_TYPE currType, eUI_TYPE nextType){ HideThisUI_OpenNextUI(currType, nextType, null); }
    public void HideThisUI_OpenNextUI(eUI_TYPE currType, eUI_TYPE nextType, ArrayList array)
    {
        GameObject gameObj = _FindUI(currType);
        if (gameObj == null)
        {
            Debug.Log("Not Found UI Type!");
            return;
        }

        UI_Script ui = gameObj.GetComponent<UI_Script>();
        if (array != null)
        {
            // 받아온 리스트에 타입을 추가해서
            // 리스트로 넘기기(주로 빌딩UI용)
            ArrayList aList = new ArrayList();
            aList.Add(nextType);
            for (int i = 0; i < array.Count; i++ )
            {
                aList.Add(array[i]);
            }

            ui.OpenNextUI(aList);
        }
        else
        {
            ui.OpenNextUI(nextType);
        }

        HideUI(currType); // 빌딩UI는 예외적으로 CloseFirstChild가 우선작동
    }

    public void HideUI(UI_Script ui) {
        HideUI(ui.m_eType);
    }

    public void HideUI(eUI_TYPE eType)
    {
        //Debug.Log("HideUI Start ============================================ " + eType);
        GameObject gameObj = _FindUI(eType);

        if (gameObj == null)
        {
            Debug.Log("HideUI() Not Found UI!! : " + eType);
        }
        else
        {
            if (gameObj.activeSelf == false)
            {
                Debug.Log("이미 비활성화 되어 있음! " + eType);
                return;
            }

            //-----------------------------------------------
            // 빌딩 UI는 연출이 있는 자식 UI가 먼저 닫혀야 함
            //if (eType == eUI_TYPE.HUD_Building)
            //{
            //    UI_Script ui = gameObj.GetComponent<UI_Script>();
            //    if (ui.CheckChildAllClose() == false)
            //    {
            //        ui.ChildFirstClose();
            //        return;
            //    }
            //    else
            //    {
            //        // 자식들이 모두 닫혔음...
            //        // 다음 루틴 진행
            //    }
            //}
            //-----------------------------------------------

            // 마지막 UI 삭제시 - 컬라이더 추가되면 소스변경
            int lastindex = m_lstDepthInfo.Count - 1;
            if (m_lstDepthInfo.Count >= 1 && lastindex >= 0)
            {
                // 리스트의 맨 마지막 요소를 얻어서
                UIDepthInfo lastinfo = m_lstDepthInfo[lastindex];
                if (lastinfo.m_eType == eType)
                {
                    UI_Script ui = gameObj.GetComponent<UI_Script>();

                    // 닫기 연출
                    ui.DirectionEnd(eType);

                    // 리스트에서 삭제
                    m_lstDepthInfo.RemoveAt(lastindex);

                    // 삭제후 그 다음 요소값을 얻어서 처리
                    if (m_lstDepthInfo.Count > 0)
                    {
                        lastindex = m_lstDepthInfo.Count - 1;
                        lastinfo = m_lstDepthInfo[lastindex];
                        gameObj = _FindUI(lastinfo.m_eType);
                        m_currType = lastinfo.m_eType;
                    }
                    else
                    {
                        gameObj = null;
                        m_currType = eUI_TYPE.NONE;
                    }

                    // 현재 사라진 오브젝트 바로 뒤의 오브젝트에 컬라이더 붙이기
                    _ColliderProcess(gameObj);
                }
                else
                {
                    Debug.Log("삭제하려는 타입과 맨마지막 타입이 같지 않습니다. 삭제할 타입:" + eType + ", 마지막 타입:" + lastinfo.m_eType);
                    return;
                }
            }
            else
            {
                Debug.Log("삭제하려는 카운트 값이 맞지 않습니다.");
                return;
            }
        }
    }

    // 연출중인지 세팅
    public void SetDirection(bool bIsDirection)
    {
        m_bIsDirectioning = bIsDirection;
    }

    // 현재 연출중인지 값 얻어오기
    public bool GetDirection()
    {
        return m_bIsDirectioning;
    }

    // UI가 하나라도 열렸다면
    public bool IsAnyUIOpen()
    {
        //Debug.Log("count : " + m_lstDepthInfo.Count);        
        if (m_lstDepthInfo.Count > 0)
            return true;

        if (GetDirection() == true)
            return true;

        return false;
    }

    public GameObject GetMapObj(eUI_TYPE _type)
    {
        Debug.Log(m_mapObj[_type].gameObject);
        return m_mapObj[_type].gameObject;
    }

    // 현재 활성화 되어 있는지 체크하는 함수
    public bool IsActive(eUI_TYPE type)
    {
        GameObject obj = _FindUI(type);

        if (obj == null)
            return false;
        else
            return obj.activeSelf;
    }

    // GetUI와 기능 중복
    // 새로 생성할때도 GetUI를 쓰지만 이미 생성된 UI를 얻을수도 있음
    public GameObject FindUI(eUI_TYPE type)
    {
        return _FindUI(type);
    }

    public GameObject FindUI(eInGame_Type type)
    {
        return _FindMainGameUi(type);
    }

    ///////////////////////////////////////////////////////////////////////////// 
    //
    // 이하 내부함수 - 될수 있으면 건드리지 말고 필요한 함수있으면 새로 만들어서...
    //
    /////////////////////////////////////////////////////////////////////////////
    private void _CreateRoot()
    {
        m_uiRoot = new GameObject("UIRoot").AddComponent<UIRoot>();

        m_uiRoot.scalingStyle = UIRoot.Scaling.Flexible;//.ConstrainedOnMobiles;//.FixedSizeOnMobiles;

        // 임시 - 웹버전용과 모바일 버전용으로 구분되는 코드가 있어야 함

        m_uiRoot.manualWidth = 1600;
        m_uiRoot.manualHeight = 870;// 720;		// 기준값

        m_uiRoot.maximumHeight = 1600;// 1536;
        m_uiRoot.minimumHeight = 870;

        m_uiRoot.gameObject.layer = 5; // "UI";

        //"JIS
        m_uiRoot.transform.parent = m_goRoot.transform;
        m_uiRoot.transform.localPosition = new Vector3(-1000f, 0f, 0f);
        //m_uiRoot.transform.localPosition = new Vector3(0f, -3000f, 0f);
        //m_uiRoot.transform.localPosition = new Vector3(-10000f, 0f, 0f);
    }

    	// 추후 외부에서 갖고 들어오는 방식으로 변경해야 함 (타입명, 프리팹)
    private void _LoadPrefabName()
    {
        eUI_TYPE[] values = (eUI_TYPE[])System.Enum.GetValues(typeof(eUI_TYPE));

        for (int i = 0; i < values.Length; i++)
        {
            if (values[i] == eUI_TYPE.NONE || values[i] == eUI_TYPE.END || values[i] == eUI_TYPE.MainHUD) continue;            
            m_mapPrefabname.Add(values[i], values[i].ToString());
        }

        eInGame_Type[] gameValues = (eInGame_Type[])System.Enum.GetValues(typeof(eInGame_Type));
        for (int i = 0; i < gameValues.Length; i++)
        {
            if (gameValues[i] == eInGame_Type.None) continue;
            m_MainUiPrefabName.Add(gameValues[i], gameValues[i].ToString());
        }
    }

    private GameObject _FindUI(eUI_TYPE type)
    {
        if (m_mapObj.ContainsKey(type)) return m_mapObj[type];
        return null;
    }

    private GameObject _FindMainGameUi(eInGame_Type type)
    {
        if (m_MainUiObj.ContainsKey(type)) return m_MainUiObj[type];
        return null;
    }

    private string _Find_PrefabName(eUI_TYPE type)
    {
        if (m_mapPrefabname.ContainsKey(type)) return m_mapPrefabname[type];
        return null;
    }

    /// <summary>
    /// 메인 게임UI Name을 검색.
    /// </summary>
    /// <param name="type">eInGameType</param>
    /// <returns></returns>
    private string _Find_MainGameName(eInGame_Type type)
    {
        if (m_MainUiPrefabName.ContainsKey(type)) return m_MainUiPrefabName[type];
        return null;
    }

    // 활성화 되어 있는 UI가 있다면 인자값 다시 세팅
    private UI_Script _ReActiveUI(GameObject obj, ArrayList array)
    {
        UI_Script workScript;

        // 활성화되어 있는 UI는 데이터값만 바꾸는걸로
        if (obj.activeSelf)
        {
            //if (Constants.JIS_MANAGERDEPTH_DEBUG) Debug.Log("---------- UI 이미생성, 이미활성화 --------");

            // 될수 있으면 이 루틴은 호출을 안해야 함
            // 컬라이더나 뎁스값등이 꼬일수 있음
            workScript = obj.GetComponent<UI_Script>();
            if (array != null) workScript.SetData(array);
        }
        // 새로 로드되는 UI라면
        else
        {
            //if (Constants.JIS_MANAGERDEPTH_DEBUG) Debug.Log("---------- UI 이미생성, 활성화!! --------");

            // 꼭 아래 순서로 진행!!

            // 실제 데이터를 담기 위한 맵 - 추가할때만 처리
            //m_mapObj.Add(type, gameObj);
            obj.SetActive(true);

            workScript = obj.GetComponent<UI_Script>();
            if (array != null) workScript.SetData(array);

            // 렌더큐 지정
            m_uiCurrRQ = _RenderQProcess(obj);

            // 자식들 뎁스값 처리 - 내부판넬순서, 컬라이더 처리와 관계있음
            _PanelDepthProcess(obj);

            // 리스트 추가
            UIDepthInfo info = new UIDepthInfo(workScript.m_eType, eInGame_Type.None, m_uiCurrRQ);
            m_lstDepthInfo.Add(info);

            // 컬라이더 처리
            _ColliderProcess(obj);
        }

        return workScript;
        //Debug.Log("m_lstDepthInfo.Count : " + m_lstDepthInfo.Count);
    }
    
    private UI_Script _AddUI(string strPath, eUI_TYPE type)
    {
        //Debug.Log("_AddUI_New:" + type);

        string filename = _Find_PrefabName(type);
        if (filename == null)
        {
            Debug.Log("File not found!");
            return null;
        }

        string fullpath = string.Format("{0}{1}.prefab", strPath, filename);
        //Debug.Log("JIS fullpath:" + fullpath);

        Object obj = LoadAsset_Mng._Inst.LoadAsset (eASSETBUNDLE_TYPE.ui , strPath , filename , eASSET_EXT_TYPE.Prefab);

        //Object obj = null;// LoadAsset_Mng._Inst.LoadAsset(eASSETBUNDLE_TYPE.ui, strPath, filename, eASSET_EXT_TYPE.Prefab);

		GameObject gameObj = (GameObject)Instantiate(obj)as GameObject;

        if (gameObj == null)
        {
            Debug.Log("리소스를 로드할수가 없습니다 : " + fullpath);
            return null;
        }

        //Debug.Log(fullpath + "     " + type);

        gameObj.transform.parent = m_uiRoot.gameObject.transform;
        gameObj.transform.localPosition = Vector3.zero;
        gameObj.transform.localScale = Vector3.one;

        UI_Script result = gameObj.GetComponent<UI_Script>();

        //Debug.Log("UI : " + gameObj);

        // 꼭 아래 순서로 진행!!

        // 실제 데이터를 담기 위한 맵 - 추가할때만 처리

        //Debug.Log(gameObj);
        m_mapObj.Add(type, gameObj);

        // 렌더큐 지정
        m_uiCurrRQ = _RenderQProcess(gameObj);

        // 자식들 뎁스값 처리 - 내부판넬순서, 컬라이더 처리와 관계있음
        _PanelDepthProcess(gameObj);

        // 리스트 추가
        UIDepthInfo info = new UIDepthInfo(type, eInGame_Type.None, m_uiCurrRQ);
        m_lstDepthInfo.Add(info);

        // 컬라이더 처리
        _ColliderProcess(gameObj);

        return result;
    }

    /// <summary>
    /// m_MainUiObj에 지정된 strPath 및 type의 게임 UI를 추가.
    /// </summary>
    /// <param name="strPath">경로</param>
    /// <param name="type">MainGameUi type</param>
    private void _AddGameUi(string strPath, eInGame_Type type)
    {
        //string filename = _Find_PrefabName(type);
        string fileName = _Find_MainGameName(type);
        if (fileName == null)
        {
            Debug.Log("File not found!");
            return;
        }

        string fullpath = string.Format("{0}{1}.prefab", strPath, fileName);
        //Debug.Log(fullpath + " " + type);

        Object obj = null;// LoadAsset_Mng._Inst.LoadAsset(eASSETBUNDLE_TYPE.ui, strPath, fileName, eASSET_EXT_TYPE.Prefab);

        GameObject gameObj = (GameObject)Instantiate(obj) as GameObject;

        if (gameObj == null)
        {
            Debug.Log("리소스를 로드할수가 없습니다 : " + fullpath);
            return;
        }

        //Debug.Log(fullpath + "     " + type);

        gameObj.transform.parent = m_uiRoot.gameObject.transform;
        gameObj.transform.localPosition = Vector3.zero;
        gameObj.transform.localScale = Vector3.one;

        //UI_Script result = gameObj.GetComponent<UI_Script>();
        
        // 꼭 아래 순서로 진행!!

        // 실제 데이터를 담기 위한 맵 - 추가할때만 처리
        m_MainUiObj.Add(type, gameObj);

        // 렌더큐 지정
        m_uiCurrRQ = _RenderQProcess(gameObj);

        // 자식들 뎁스값 처리 - 내부판넬순서, 컬라이더 처리와 관계있음
        _PanelDepthProcess(gameObj);

        // 리스트 추가        
        UIDepthInfo info = new UIDepthInfo(eUI_TYPE.NONE, type, m_uiCurrRQ);

        m_lstDepthInfo.Add(info);

        // 컬라이더 처리
        _ColliderProcess(gameObj);
    }

    // 렌더큐값 얻기 함수 - 각 프리팹의 부모는 판넬이 기본적으로 하나씩 있어야 함.
    private uint _RenderQProcess(GameObject obj)
    {
        // 리스트의 갯수 * 오프셋 = 현재 적용할 UI의 렌더큐
        uint currRQ = (uint)((m_lstDepthInfo.Count * m_uiNormalRQ_Offset) + m_uiNormalRQ_Start);

        //Debug.Log(currRQ);

        // 판넬에 실제 렌더큐값 적용하기
        UIPanel pan = obj.GetComponent<UIPanel>();
        pan.renderQueue = UIPanel.RenderQueue.Explicit;
        pan.startingRenderQueue = (int)currRQ;

        //Debug.Log(currRQ);
        return currRQ;
    }

    // 판넬의 뎁스값 처리 함수 - 자식들의 뎁스값들도 처리
    private void _PanelDepthProcess(GameObject obj)
    {
        //Debug.Log(obj.name);

        if (obj == null)
        {
            return;
        }

        UIPanel parentPanel = obj.GetComponent<UIPanel>();
        if (m_lstDepthInfo.Count == 0)
        {
            m_nMaxDepth = 1;
        }
        else
        {
            //UIDepthInfo lastinfo = m_lstDepthInfo[m_lstDepthInfo.Count - 1];
        }

        parentPanel.depth = m_nMaxDepth;

        // 자식 판넬들의 depth값들도 일괄적으로 적용해야 함
        UI_Script workScript = obj.GetComponent<UI_Script>();
        if (workScript != null)
        {
            workScript.DepthProcess();
            workScript.SetRenderQ();
        }
        else
        {
            Debug.Log("UI_Script error!!");
        }

        m_nMaxDepth = workScript.GetMaxDepth();
    }

    private void _ColliderProcess(GameObject obj)
    {
        if (obj == null) return;
        
        UI_Script script = obj.GetComponent<UI_Script>();
        
        if (script.IsNoColl) return;

        m_goCollider.SetActive(true);
        m_goCollider.transform.parent = obj.transform;

        m_goCollider.transform.localPosition = Vector3.zero;
        m_goCollider.transform.localScale = Vector3.one;

        script = obj.GetComponent<UI_Script>();
        //m_BoxCollider.gameObject.transform.parent = obj.transform.parent.transform;
        m_BoxCollider2D.gameObject.transform.localPosition = Vector3.zero;

        // 자식 아웃클릭처리와 연결(UIOutClick에 인수전달가능)
        EventDelegate.Set(m_OutClickScript.onClick, script.UIOutClick);


        //UI_Script script = obj.GetComponent<UI_Script>();
        if (script.m_eType == eUI_TYPE.MainHUD || 
            script.m_eType == eUI_TYPE.BottomPanel_Web ||
            script.m_eType == eUI_TYPE.CommonPanel_UI)
        {
            //Debug.Log("box collider size reset");
            m_BoxCollider2D.offset = new Vector3(0f, 0f, 0f);
            m_BoxCollider2D.size = Vector3.zero;
        }
        else
        {
            m_BoxCollider2D.offset = new Vector3(0f, 0f, 0f);
            m_BoxCollider2D.size = new Vector3(Screen.width + 700, Screen.height + 500, 0);
        }

        //Debug.Log("" + m_BoxCollider.size);

        // 이렇게 이상하게 처리가 되어야 컬라이더 오브젝트 작동
        UIWidget widget = m_goCollider.GetComponent<UIWidget>();
        widget.enabled = false;
        widget.enabled = true;

        //Debug.Log("UI_Script_Mng() _ColliderProcess : " + obj.transform);

        /*
        // EventDelegate 오브젝트 생성.
        EventDelegate eventClick = new EventDelegate(script, "UIOutClick");
		
         * // Parameter 오브젝트 생성. 콜백에 변수를 전달할 수 있음.
            EventDelegate.Parameter param = new EventDelegate.Parameter();
            // 전달한 변수. 여기선 GameObject.
            param.obj = objButton;
            // 전달한 변수의 타입.
            param.expectedType = typeof(GameObject);
            // 배열로 돼있기 때문에 인수를 여러개 전달할 수 있음.
            eventClick.parameters[0] = param;

         * 
		
        // 이벤트를 버튼에 걸어줌. - 계속 증가됨
        EventDelegate.Add(m_OutClickScript.onClick, eventClick);
        */
    }

    public void RunOnComplete(eUI_TYPE eType)
    {
        //Debug.Log("RunOnComplete : " + eType);

        // 같은 계열의 UI를 닫고 열때 전의 UI가 닫히는 지연시간필요
        StartCoroutine(WaitCloseUI(eType));
    }

    public void RunOnComplete(ArrayList arr)
    {
        //ArrayList range = arr.GetRange(1, arr.Count - 1);

        //UI_Script script = GetUI((eUI_TYPE)arr[0], range);

        // 아래는 배열 받아서 처리하는 예제
        //if ((eUI_TYPE)arr[0] == eUI_TYPE.HUD_Building)
        //{
        //    빌딩타입 = arr[1];
        //    빌딩상태 = arr[2];
        //}
    }

    IEnumerator WaitCloseUI(eUI_TYPE eType)
    {
        yield return new WaitForEndOfFrame();

        //UI_Script script = GetUI(eType, null);

        //if (eType == eUI_TYPE.HUD_Building)
        //{
        //    ObjBuild objBuild = (ObjGame_Mng._Inst._SelectObj as ObjBuild);
        //    (script as HudUI_Building).SetType(objBuild);
        //}
    }

    public void CloseCurrUI()
    {
        // 디버그용 루틴
        /*
        for (int i = 0; i < m_lstDepthInfo.Count; i++)
        {
            UIDepthInfo depthinfo = m_lstDepthInfo[i];
            Debug.Log("" + depthinfo.m_eType);
        }
        //Debug.Log("맨마지막 UI:" + m_lstDepthInfo[m_lstDepthInfo.Count-1].m_eType);
        */

        if (m_currType == eUI_TYPE.NONE)
        {
            Debug.Log("닫을 UI가 없음!");
        }
        else
        {
            HideUI(m_currType);
        }
    }
}