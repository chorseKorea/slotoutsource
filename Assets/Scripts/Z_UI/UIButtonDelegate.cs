﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIButtonDelegate : MonoBehaviour {

    public enum Trigger
    {
        OnClick,
        OnMouseOver,
        OnMouseOut,
        OnPress,
        OnRelease,
        OnSelectIn,
        OnSelectOut,
        OnDrag,
    }

    public GameObject target;
    public string functionName;
    public Trigger trigger = Trigger.OnClick;
    public bool includeChildren = false;


    public object Argv;

    // Sound
    public bool bBtnSound = true;
    //public eSOUND eSnd = eSOUND.EButton_click;


    void OnHover(bool isOver)
    {
        if (((isOver && trigger == Trigger.OnMouseOver) ||
            (!isOver && trigger == Trigger.OnMouseOut))) Send();
    }

    void OnPress(bool isPressed)
    {
        if (((isPressed && trigger == Trigger.OnPress) ||
            (!isPressed && trigger == Trigger.OnRelease)))
        {
            Send();
        }
    }

    void OnSelect(bool isSelect)
    {
        if (((isSelect && trigger == Trigger.OnSelectIn) ||
            (!isSelect && trigger == Trigger.OnSelectOut)))
        {
            if (isSelect) UICamera.selectedObject = null;

            //if (SCREEN_HANDLER.s_nCurScreen == (int)eSCR_HANDLE.RESTAURANT)
            //{
            //	if (GameInfo.GAMERESTAURANTSTATE == eRESTAURNTSTAGE_STATE.eSELECT_BUFFETTABLE)
            //	{
            //		PopUpMgr.Inst().Show(eMSG_LIST.SELECT_BUFFET_TABLE);
            //		return;
            //	}
            //}

            Send();
        }
    }

    // 현재 버튼이 클릭됐을때(컬라이더 범위가 벗어나면 발생하지 않는다.)
    void OnClick()
    {
        if (trigger == Trigger.OnClick) Send();
    }

    void OnDrag(Vector2 delta)
    {
        if (trigger == Trigger.OnDrag) Send();
    }

    void Send()
    {
        //if (bBtnSound) Sound_Mng._Inst.Play(eSnd);

        if (!enabled || !gameObject.active || string.IsNullOrEmpty(functionName)) return;
        if (target == null) target = gameObject;

        if (includeChildren)
        {
            Transform[] transforms = target.GetComponentsInChildren<Transform>();

            for (int i = 0, imax = transforms.Length; i < imax; ++i)
            {
                Transform t = transforms[i];
                t.gameObject.SendMessage(functionName, Argv, SendMessageOptions.DontRequireReceiver);
                //t.gameObject.SendMessage(functionName, m_arlArgv, SendMessageOptions.DontRequireReceiver);
            }
        }
        else
        {
            target.SendMessage(functionName, Argv, SendMessageOptions.DontRequireReceiver);
        }
    }
}
